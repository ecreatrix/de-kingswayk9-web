/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "../../../../Plugins/external-apis/development/source/scripts/main.jsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../../../Extras/wa-custom/scripts.2/misc/slick.jsx":
/*!*******************************************************************************!*\
  !*** /Volumes/Files/_Business/Code/Extras/wa-custom/scripts.2/misc/slick.jsx ***!
  \*******************************************************************************/
/*! exports provided: createSlick, resetSlick */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createSlick\", function() { return createSlick; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"resetSlick\", function() { return resetSlick; });\n/* jshint -W079 */\nvar $ = window.jQuery;\nfunction createSlick(slickClass) {\n  $(slickClass + ':not(.slick-initialized)').each(function () {\n    var attributes = $(this).attr('data-slick-atts');\n\n    if (undefined !== attributes) {\n      attributes = JSON.parse(attributes);\n    }\n\n    if (attributes !== undefined && attributes !== null && attributes.slidesToShow >= 1) {\n      console.log(attributes);\n      var parameters = {\n        fade: attributes.fade || false,\n        arrows: attributes.arrows || false,\n        dots: attributes.dots || false,\n        infinite: attributes.infinite || false,\n        autoplay: attributes.autoplay || false,\n        autoplaySpeed: attributes.autoplaySpeed * 1000 || 3000,\n        slidesToShow: attributes.slidesToShow || 1,\n        slidesToScroll: attributes.slidesToScroll || 1,\n        swipeToSlide: true,\n        lazyLoad: 'ondemand',\n        responsive: [{\n          breakpoint: 992,\n          settings: {\n            slidesToShow: attributes.slidesToShow ? attributes.slidesToShow : attributes.slidesToShow,\n            slidesToScroll: 1\n          }\n        }, {\n          breakpoint: 768,\n          settings: {\n            slidesToShow: attributes.slidesToShowTablet ? attributes.slidesToShowTablet : attributes.slidesToShowTablet,\n            slidesToScroll: 1\n          }\n        }, {\n          breakpoint: 544,\n          settings: {\n            slidesToShow: attributes.slidesToShowMobile ? attributes.slidesToShowMobile : attributes.slidesToShowMobile,\n            slidesToScroll: 1\n          }\n        }]\n      };\n      console.log(parameters);\n      $(this).slick(parameters);\n    } else {\n      $(this).slick({\n        lazyLoad: 'ondemand'\n      });\n    }\n  });\n}\n;\nfunction resetSlick(slickClass) {\n  var slick = $(slickClass + '.slick-initialized');\n\n  if (undefined === slick) {\n    return;\n  }\n\n  slick.slick('setPosition');\n}\n;\n\n//# sourceURL=webpack:////Volumes/Files/_Business/Code/Extras/wa-custom/scripts.2/misc/slick.jsx?");

/***/ }),

/***/ "../../../../Plugins/external-apis/development/source/scripts/main.jsx":
/*!***********************************************************************************************!*\
  !*** /Volumes/Files/_Business/Code/Plugins/external-apis/development/source/scripts/main.jsx ***!
  \***********************************************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _main_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main/slick */ \"../../../../Plugins/external-apis/development/source/scripts/main/slick.jsx\");\n\n\n//# sourceURL=webpack:////Volumes/Files/_Business/Code/Plugins/external-apis/development/source/scripts/main.jsx?");

/***/ }),

/***/ "../../../../Plugins/external-apis/development/source/scripts/main/slick.jsx":
/*!*****************************************************************************************************!*\
  !*** /Volumes/Files/_Business/Code/Plugins/external-apis/development/source/scripts/main/slick.jsx ***!
  \*****************************************************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var wa_scripts_2_misc_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! wa-scripts.2/misc/slick */ \"../../../../Extras/wa-custom/scripts.2/misc/slick.jsx\");\n$ = jQuery.noConflict();\n\nvar slickClass = '.slick-add-waapis'; //console.log(slickClass)\n\n$(window).on('orientationchange resize', function () {\n  if ($(slickClass).length) {\n    Object(wa_scripts_2_misc_slick__WEBPACK_IMPORTED_MODULE_0__[\"resetSlick\"])(slickClass);\n  }\n});\n$(window).on('ready', function () {\n  // Make sure slick shows by resizing on opening of modal\n  $('.modal-carousel').on('shown.bs.modal', function () {\n    if ($(slickClass).length) {\n      Object(wa_scripts_2_misc_slick__WEBPACK_IMPORTED_MODULE_0__[\"resetSlick\"])(slickClass);\n    }\n  });\n});\n$(window).on('load', function () {\n  //console.log($( slickClass ).length)\n  if ($(slickClass).length) {\n    Object(wa_scripts_2_misc_slick__WEBPACK_IMPORTED_MODULE_0__[\"createSlick\"])(slickClass);\n  }\n});\n\n//# sourceURL=webpack:////Volumes/Files/_Business/Code/Plugins/external-apis/development/source/scripts/main/slick.jsx?");

/***/ })

/******/ });