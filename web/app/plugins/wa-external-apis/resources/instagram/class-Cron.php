<?php
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

if (  ! defined( 'ABSPATH' ) ) {
	exit;
}

if (  ! class_exists( Cron::class ) ) {
	class Cron {
		public static $sections = ['access', 'profile', 'media'];

		public function __construct() {
			$this->activate();

			foreach ( self::$sections as $section ) {
				$hook = self::get_key() . '_' . $section . '_cron_job';
				add_action( $hook, [$this, 'refresh_' . $section] );
			}
			//print_r($GLOBALS['wp_filter']);

		}

		//Setup cron jobs
		public function activate() {
			add_filter( 'cron_schedules', [$this, 'schedules'] );

			foreach ( self::$sections as $section ) {
				$key = self::get_key() . '_' . $section . '_';

				//var_dump( $key . 'cron_job' );
				if (  ! wp_next_scheduled( $key . 'cron_job' ) ) {
					wp_schedule_event( time(), $key . 'schedule', $key . 'cron_job' );
				}
			}
		}

		public static function deactivate() {
			//wp_clear_scheduled_hook( 'waapis_instagram_accesscron_job' );
			//wp_clear_scheduled_hook( 'waapis_instagram_profilecron_job' ); wp_clear_scheduled_hook( 'waapis_instagram_mediacron_job' );

			foreach ( self::$sections as $section ) {
				$key = self::get_key() . '_' . $section . '_cron_job';
				wp_clear_scheduled_hook( $key );
			}
		}

		public static function get_key() {
			return Plugin\CODENAME  . '_' . Index::get_key();
		}

		public function refresh_access() {
			$key      = Index::get_key();
			$settings = Plugin\Settings\Options::get_settings( Index::get_key() );

			if ( array_key_exists( 'access', $settings ) ) {
				$access = new Connect\Access( false, $settings['access'] );
				$access->connect();
				$settings['access'] = $access;

				Plugin\Settings\Options::update_settings( $settings, $key );
			}

			$this->activate();
		}

		public function refresh_media() {
			$key      = Index::get_key();
			$settings = Plugin\Settings\Options::get_settings( $key );

			if ( array_key_exists( 'access', $settings ) ) {
				$media = new Connect\Media( false, $settings['access'] );
				$media->connect();
				$connection['media'] = $media;

				Plugin\Settings\Options::update_settings( $settings, $key );
			}

			$this->activate();
		}

		public function refresh_profile() {
			$key      = Index::get_key();
			$settings = Plugin\Settings\Options::get_settings( $key );

			if ( array_key_exists( 'access', $settings ) ) {
				$profile = new Connect\Profile( false, $settings['access'] );
				$profile->connect();
				$connection['profile'] = $profile;

				Plugin\Settings\Options::update_settings( $settings, $key );
			}

			$this->activate();
		}

		public function schedules() {
			$settings = Plugin\Settings\Options::get_settings( Index::get_key() );
			$key      = self::get_key();

			if ( array_key_exists( 'access', $settings ) && $settings['access'] && class_exists( Plugin\Cron\Index::class ) && ! wp_next_scheduled( $key ) ) {
				// Cron job not setup and access set. Create cron job

				$schedules[$key . '_access_schedule'] = [
					'interval' => MONTH_IN_SECONDS * 2 - WEEK_IN_SECONDS,
					//'interval' => 10,
					'display'  => __( 'Every 7 weeks' ),
				];
				$schedules[$key . '_profile_schedule'] = [
					'interval' => WEEK_IN_SECONDS,
					//'interval' => 10,
					'display'  => __( 'Weekly' ),
				];
				$schedules[$key . '_media_schedule'] = [
					'interval' => DAY_IN_SECONDS * 2,
					//'interval' => 10,
					'display'  => __( 'bidaily' ),
				];

				$settings['cron'] = $schedules;

				Plugin\Settings\Options::update_settings( $settings, $key );
			} else {
			}
			return [];
			return $schedules;
		}
	}
}
