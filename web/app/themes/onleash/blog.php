<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the Visual Composer to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

// Get template page's content
$onleash_content = '';
$onleash_blog_archive_mask = '%%CONTENT%%';
$onleash_blog_archive_subst = sprintf('<div class="blog_archive">%s</div>', $onleash_blog_archive_mask);
if ( have_posts() ) {
	the_post(); 
	if (($onleash_content = apply_filters('the_content', get_the_content())) != '') {
		if (($onleash_pos = strpos($onleash_content, $onleash_blog_archive_mask)) !== false) {
			$onleash_content = preg_replace('/(\<p\>\s*)?'.$onleash_blog_archive_mask.'(\s*\<\/p\>)/i', $onleash_blog_archive_subst, $onleash_content);
		} else
			$onleash_content .= $onleash_blog_archive_subst;
		$onleash_content = explode($onleash_blog_archive_mask, $onleash_content);
		// Add VC custom styles to the inline CSS
		$vc_custom_css = get_post_meta( get_the_ID(), '_wpb_shortcodes_custom_css', true );
		if ( !empty( $vc_custom_css ) ) onleash_add_inline_css(strip_tags($vc_custom_css));
	}
}

// Prepare args for a new query
$onleash_args = array(
	'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish'
);
$onleash_args = onleash_query_add_posts_and_cats($onleash_args, '', onleash_get_theme_option('post_type'), onleash_get_theme_option('parent_cat'));
$onleash_page_number = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
if ($onleash_page_number > 1) {
	$onleash_args['paged'] = $onleash_page_number;
	$onleash_args['ignore_sticky_posts'] = true;
}
$onleash_ppp = onleash_get_theme_option('posts_per_page');
if ((int) $onleash_ppp != 0)
	$onleash_args['posts_per_page'] = (int) $onleash_ppp;
// Make a new query
query_posts( $onleash_args );
// Set a new query as main WP Query
$GLOBALS['wp_the_query'] = $GLOBALS['wp_query'];

// Set query vars in the new query!
if (is_array($onleash_content) && count($onleash_content) == 2) {
	set_query_var('blog_archive_start', $onleash_content[0]);
	set_query_var('blog_archive_end', $onleash_content[1]);
}

get_template_part('index');
?>