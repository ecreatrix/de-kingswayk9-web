<?php
/**
 * The Sticky template to display the sticky posts
 *
 * Used for index/archive
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_columns = max(1, min(3, count(get_option( 'sticky_posts' ))));
$onleash_post_format = get_post_format();
$onleash_post_format = empty($onleash_post_format) ? 'standard' : str_replace('post-format-', '', $onleash_post_format);
$onleash_animation = onleash_get_theme_option('blog_animation');

?><div class="column-1_<?php echo esc_attr($onleash_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_sticky post_format_'.esc_attr($onleash_post_format) ); ?>
	<?php echo (!onleash_is_off($onleash_animation) ? ' data-animation="'.esc_attr(onleash_get_animation_classes($onleash_animation)).'"' : ''); ?>
	>

	<?php
	if ( is_sticky() && is_home() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	onleash_show_post_featured(array(
		'thumb_size' => onleash_get_thumb_size($onleash_columns==1 ? 'big' : ($onleash_columns==2 ? 'med' : 'avatar'))
	));

	if ( !in_array($onleash_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php
			// Post title
			the_title( sprintf( '<h6 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h6>' );
			// Post meta
			onleash_show_post_meta();
			?>
		</div><!-- .entry-header -->
		<?php
	}
	?>
</article></div>