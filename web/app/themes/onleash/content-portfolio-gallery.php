<?php
/**
 * The Gallery template to display posts
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_blog_style = explode('_', onleash_get_theme_option('blog_style'));
$onleash_columns = empty($onleash_blog_style[1]) ? 2 : max(2, $onleash_blog_style[1]);
$onleash_post_format = get_post_format();
$onleash_post_format = empty($onleash_post_format) ? 'standard' : str_replace('post-format-', '', $onleash_post_format);
$onleash_animation = onleash_get_theme_option('blog_animation');
$onleash_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' );

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_gallery post_layout_gallery_'.esc_attr($onleash_columns).' post_format_'.esc_attr($onleash_post_format) ); ?>
	<?php echo (!onleash_is_off($onleash_animation) ? ' data-animation="'.esc_attr(onleash_get_animation_classes($onleash_animation)).'"' : ''); ?>
	data-size="<?php if (!empty($onleash_image[1]) && !empty($onleash_image[2])) echo intval($onleash_image[1]) .'x' . intval($onleash_image[2]); ?>"
	data-src="<?php if (!empty($onleash_image[0])) echo esc_url($onleash_image[0]); ?>"
	>

	<?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	$onleash_image_hover = 'icon';	//onleash_get_theme_option('image_hover');
	if (in_array($onleash_image_hover, array('icons', 'zoom'))) $onleash_image_hover = 'dots';
	onleash_show_post_featured(array(
		'hover' => $onleash_image_hover,
		'thumb_size' => onleash_get_thumb_size( strpos(onleash_get_theme_option('body_style'), 'full')!==false || $onleash_columns < 3 ? 'masonry-big' : 'masonry' ),
		'thumb_only' => true,
		'show_no_image' => true,
		'post_info' => '<div class="post_details">'
							. '<h2 class="post_title"><a href="'.esc_url(get_permalink()).'">'. esc_html(get_the_title()) . '</a></h2>'
							. '<div class="post_description">'
								. onleash_show_post_meta(array(
									'categories' => true,
									'date' => true,
									'edit' => false,
									'seo' => false,
									'share' => true,
									'counters' => 'comments',
									'echo' => false
									))
								. '<div class="post_description_content">'
									. apply_filters('the_excerpt', get_the_excerpt())
								. '</div>'
								. '<a href="'.esc_url(get_permalink()).'" class="theme_button post_readmore"><span class="post_readmore_label">' . esc_html__('Learn more', 'onleash') . '</span></a>'
							. '</div>'
						. '</div>'
	));
	?>
</article>