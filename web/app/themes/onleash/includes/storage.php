<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('onleash_storage_get')) {
	function onleash_storage_get($var_name, $default='') {
		global $ONLEASH_STORAGE;
		return isset($ONLEASH_STORAGE[$var_name]) ? $ONLEASH_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('onleash_storage_set')) {
	function onleash_storage_set($var_name, $value) {
		global $ONLEASH_STORAGE;
		$ONLEASH_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('onleash_storage_empty')) {
	function onleash_storage_empty($var_name, $key='', $key2='') {
		global $ONLEASH_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($ONLEASH_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($ONLEASH_STORAGE[$var_name][$key]);
		else
			return empty($ONLEASH_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('onleash_storage_isset')) {
	function onleash_storage_isset($var_name, $key='', $key2='') {
		global $ONLEASH_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($ONLEASH_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($ONLEASH_STORAGE[$var_name][$key]);
		else
			return isset($ONLEASH_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('onleash_storage_inc')) {
	function onleash_storage_inc($var_name, $value=1) {
		global $ONLEASH_STORAGE;
		if (empty($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = 0;
		$ONLEASH_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('onleash_storage_concat')) {
	function onleash_storage_concat($var_name, $value) {
		global $ONLEASH_STORAGE;
		if (empty($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = '';
		$ONLEASH_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('onleash_storage_get_array')) {
	function onleash_storage_get_array($var_name, $key, $key2='', $default='') {
		global $ONLEASH_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($ONLEASH_STORAGE[$var_name][$key]) ? $ONLEASH_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($ONLEASH_STORAGE[$var_name][$key][$key2]) ? $ONLEASH_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('onleash_storage_set_array')) {
	function onleash_storage_set_array($var_name, $key, $value) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if ($key==='')
			$ONLEASH_STORAGE[$var_name][] = $value;
		else
			$ONLEASH_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('onleash_storage_set_array2')) {
	function onleash_storage_set_array2($var_name, $key, $key2, $value) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if (!isset($ONLEASH_STORAGE[$var_name][$key])) $ONLEASH_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$ONLEASH_STORAGE[$var_name][$key][] = $value;
		else
			$ONLEASH_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Merge array elements
if (!function_exists('onleash_storage_merge_array')) {
	function onleash_storage_merge_array($var_name, $key, $value) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if ($key==='')
			$ONLEASH_STORAGE[$var_name] = array_merge($ONLEASH_STORAGE[$var_name], $value);
		else
			$ONLEASH_STORAGE[$var_name][$key] = array_merge($ONLEASH_STORAGE[$var_name][$key], $value);
	}
}

// Add array element after the key
if (!function_exists('onleash_storage_set_array_after')) {
	function onleash_storage_set_array_after($var_name, $after, $key, $value='') {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if (is_array($key))
			onleash_array_insert_after($ONLEASH_STORAGE[$var_name], $after, $key);
		else
			onleash_array_insert_after($ONLEASH_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('onleash_storage_set_array_before')) {
	function onleash_storage_set_array_before($var_name, $before, $key, $value='') {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if (is_array($key))
			onleash_array_insert_before($ONLEASH_STORAGE[$var_name], $before, $key);
		else
			onleash_array_insert_before($ONLEASH_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('onleash_storage_push_array')) {
	function onleash_storage_push_array($var_name, $key, $value) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($ONLEASH_STORAGE[$var_name], $value);
		else {
			if (!isset($ONLEASH_STORAGE[$var_name][$key])) $ONLEASH_STORAGE[$var_name][$key] = array();
			array_push($ONLEASH_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('onleash_storage_pop_array')) {
	function onleash_storage_pop_array($var_name, $key='', $defa='') {
		global $ONLEASH_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($ONLEASH_STORAGE[$var_name]) && is_array($ONLEASH_STORAGE[$var_name]) && count($ONLEASH_STORAGE[$var_name]) > 0) 
				$rez = array_pop($ONLEASH_STORAGE[$var_name]);
		} else {
			if (isset($ONLEASH_STORAGE[$var_name][$key]) && is_array($ONLEASH_STORAGE[$var_name][$key]) && count($ONLEASH_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($ONLEASH_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('onleash_storage_inc_array')) {
	function onleash_storage_inc_array($var_name, $key, $value=1) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if (empty($ONLEASH_STORAGE[$var_name][$key])) $ONLEASH_STORAGE[$var_name][$key] = 0;
		$ONLEASH_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('onleash_storage_concat_array')) {
	function onleash_storage_concat_array($var_name, $key, $value) {
		global $ONLEASH_STORAGE;
		if (!isset($ONLEASH_STORAGE[$var_name])) $ONLEASH_STORAGE[$var_name] = array();
		if (empty($ONLEASH_STORAGE[$var_name][$key])) $ONLEASH_STORAGE[$var_name][$key] = '';
		$ONLEASH_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('onleash_storage_call_obj_method')) {
	function onleash_storage_call_obj_method($var_name, $method, $param=null) {
		global $ONLEASH_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($ONLEASH_STORAGE[$var_name]) ? $ONLEASH_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($ONLEASH_STORAGE[$var_name]) ? $ONLEASH_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('onleash_storage_get_obj_property')) {
	function onleash_storage_get_obj_property($var_name, $prop, $default='') {
		global $ONLEASH_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($ONLEASH_STORAGE[$var_name]->$prop) ? $ONLEASH_STORAGE[$var_name]->$prop : $default;
	}
}
?>