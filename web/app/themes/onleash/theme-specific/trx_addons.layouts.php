<?php
//Custom Layouts
$layouts = array(
		'footer_12' => array(
				'name' => 'Dog Footer Standard',
				'template' => '[vc_row full_width=\"stretch_row\" scheme=\"dark\" row_type=\"normal\" row_delimiter=\"1\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492779681452{padding-top: 6.8rem !important;padding-bottom: 6.8rem !important;background-color: #303030 !important;}\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][trx_widget_contacts columns=\"\" googlemap=\"\" socials=\"1\" logo=\"188\" description=\"You’ll enjoy real peace of mind knowing our dedicated professionals will do whatever is needed to keep your pets happy, healthy and safe.\"][/trx_widget_contacts][/vc_column_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][vc_wp_custommenu title=\"Information\" nav_menu=\"26\"][/vc_column_inner][vc_column_inner width=\"1/3\" icons_position=\"left\"][trx_widget_contacts columns=\"\" googlemap=\"\" socials=\"\" title=\"Contact Us:\" address=\"8768 West Creek, New York, NY 11375\" phone=\"0 800 555 11 22, 0 800 999 88 77\" email=\"youremail@info.com\"][/trx_widget_contacts][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row full_width=\"stretch_row\" scheme=\"dark\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492777918247{padding-top: 0px !important;padding-bottom: 0px !important;background-color: #2b2b2b !important;}\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" float=\"center\" number_position=\"br\" title_style=\"default\"][vc_wp_text]<a href=\"https://themeforest.net/user/ancorathemes/portfolio\" target=\"_blank\">AncoraThemes</a> &copy; 2017. All Rights Reserved. <a href=\"http://ancorathemes.com/about/\" target=\"_blank\">Terms of Use</a> and <a href=\"http://ancorathemes.com/about/\" target=\"_blank\">Privacy Policy</a>[/vc_wp_text][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'footer'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492779681452{padding-top: 6.8rem !important;padding-bottom: 6.8rem !important;background-color: #303030 !important;}.vc_custom_1492777918247{padding-top: 0px !important;padding-bottom: 0px !important;background-color: #2b2b2b !important;}'
						)
				),
		'header_490' => array(
				'name' => 'Dog Header Short',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"1\" hide_on_frontpage=\"\" css=\".vc_custom_1494245101279{padding-top: 0.2rem !important;padding-bottom: 0.1rem !important;background-color: #f5f4f0 !important;}\"][vc_column column_align=\"center\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_iconed_text icon=\"icon-mail-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-mail-empty\" text2=\"onleash@info.com\" link=\"mailto:onleash@info.com\"][trx_sc_layouts_iconed_text icon=\"icon-phone-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-phone-2\" text2=\"0 (800) 123-456\"][trx_sc_layouts_login text_login=\"Login or Register\" text_logout=\"Logout\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1494245342471{padding-top: 1.7rem !important;padding-bottom: 1.45rem !important;}\"][vc_column icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner column_align=\"left\" icons_position=\"left\" offset=\"vc_col-md-4\"][trx_sc_layouts_logo][/vc_column_inner][vc_column_inner column_align=\"right\" icons_position=\"left\" offset=\"vc_col-md-8\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494245101279{padding-top: 0.2rem !important;padding-bottom: 0.1rem !important;background-color: #f5f4f0 !important;}.vc_custom_1494245342471{padding-top: 1.7rem !important;padding-bottom: 1.45rem !important;}'
						)
				),
		'header_505' => array(
				'name' => 'Dog Header Short /1 row/',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column column_align=\"center\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/3\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_logo][/vc_column_inner][vc_column_inner width=\"2/3\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-sm vc_hidden-xs\"][trx_sc_layouts_iconed_text icon=\"icon-mail-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-mail-empty\" text2=\"onleash@info.com\" link=\"mailto:onleash@info.com\"][trx_sc_layouts_iconed_text icon=\"icon-phone-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-phone-2\" text2=\"0 (800) 123-456\"][trx_sc_layouts_login text_login=\"Login or Register\" text_logout=\"Logout\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492773239489{background-color: #f5f4f0 !important;}\"][vc_column icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner column_align=\"left\" icons_position=\"left\" offset=\"vc_col-md-8\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-sm vc_hidden-xs\"][trx_sc_layouts_search style=\"normal\" ajax=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492773239489{background-color: #f5f4f0 !important;}'
						)
				),
		'header_24' => array(
				'name' => 'Dog Header Short /2 rows/',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\" row_type=\"compact\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column column_align=\"center\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner column_align=\"left\" icons_position=\"left\" offset=\"vc_col-md-4\"][trx_sc_layouts_logo][/vc_column_inner][vc_column_inner width=\"2/3\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-sm vc_hidden-xs\"][trx_sc_layouts_iconed_text icon=\"icon-mail-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-mail-empty\" text2=\"onleash@info.com\" link=\"mailto:onleash@info.com\"][trx_sc_layouts_iconed_text icon=\"icon-phone-1\" icon_type=\"fontawesome\" icon_fontawesome=\"icon-phone-2\" text2=\"0 (800) 123-456\"][trx_sc_layouts_login text_login=\"Login or Register\" text_logout=\"Logout\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1492773239489{background-color: #f5f4f0 !important;}\"][vc_column icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" padding=\"none\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner column_align=\"left\" icons_position=\"left\" offset=\"vc_col-md-8\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"right\" icons_position=\"left\" offset=\"vc_hidden-sm vc_hidden-xs\"][trx_sc_layouts_search style=\"normal\" ajax=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row row_type=\"normal\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"1\" css=\".vc_custom_1492773526491{background: #f7f7f7 url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-title.jpg?id=395) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column column_align=\"left\" icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" padding=\"none\"][trx_sc_layouts_title title=\"1\" meta=\"\" breadcrumbs=\"1\" icon_type=\"fontawesome\" icon_fontawesome=\"\"][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492773239489{background-color: #f5f4f0 !important;}.vc_custom_1492773526491{background: #f7f7f7 url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-title.jpg?id=395) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}'
						)
				),
		'custom_482' => array(
				'name' => 'Dog Quality & Reliable',
				'template' => '[vc_row full_width=\"stretch_row\" css=\".vc_custom_1492422878750{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-quality.jpg?id=337) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"1/2\" icons_position=\"left\"][/vc_column][vc_column width=\"1/2\" icons_position=\"left\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"1\" css=\".vc_custom_1494402272512{margin-top: 2.4rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h1\" title_align=\"left\" title=\"Quality &amp; Reliable\" subtitle=\"Pet Care Services\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\" css=\".vc_custom_1494224205097{margin-top: 0.75rem !important;}\"][vc_column_text]
<ul class=\"trx_addons_list_success_circled\">
	<li>Your Premiere Pet Services</li>
	<li>15+ Years In Business</li>
	<li>100% Private and Engaging Walks</li>
</ul>
[/vc_column_text][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"1\" css=\".vc_custom_1494402278336{margin-top: -0.7rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492422878750{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-quality.jpg?id=337) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1494402272512{margin-top: 2.4rem !important;}.vc_custom_1494224205097{margin-top: 0.75rem !important;}.vc_custom_1494402278336{margin-top: -0.7rem !important;}'
						)
				),
		'custom_487' => array(
				'name' => 'Dogs Blogger',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494230284060{margin-top: 0.8rem !important;}\"][trx_sc_blogger type=\"classic\" hide_excerpt=\"1\" cat=\"0\" orderby=\"post_date\" order=\"desc\" title_style=\"default\" title_tag=\"h2\" title_align=\"center\" columns=\"3\" title=\"News &amp; Articles\" subtitle=\"Latest News\" link=\"/classic-1-column/\" link_text=\"view all news\" ids=\"137, 134, 131\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494230284060{margin-top: 0.8rem !important;}'
						)
				),
		'custom_324' => array(
				'name' => 'Dogs form',
				'template' => '[vc_row full_width=\"stretch_row\" css=\".vc_custom_1492419791789{background-color: #ece9e4 !important;}\"][vc_column icons_position=\"left\"][contact-form-7 id=\"325\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492419791789{background-color: #ece9e4 !important;}'
						)
				),
		'custom_316' => array(
				'name' => 'Dogs How Does It Work',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494224333337{margin-top: 0.5rem !important;}\"][trx_sc_services type=\"default\" featured=\"icon\" featured_position=\"top\" hide_excerpt=\"1\" no_margin=\"\" icons_animation=\"\" cat=\"22\" orderby=\"post_date\" order=\"desc\" slider=\"1\" title_style=\"default\" title_tag=\"h2\" title_align=\"center\" numbered=\"1\" count=\"3\" columns=\"3\" title=\"How Does It Work\" subtitle=\"3 Easy Steps\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493883458046{margin-top: -0.7rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494224333337{margin-top: 0.5rem !important;}.vc_custom_1493883458046{margin-top: -0.7rem !important;}'
						)
				),
		'custom_293' => array(
				'name' => 'Dogs Instagram',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493816908507{margin-top: 0.55rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Follow Our Instagram\" subtitle=\"@OnLeash\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\" css=\".vc_custom_1493816974265{margin-top: 1.5rem !important;}\"][vc_column_text][instagram-feed][/vc_column_text][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493816998507{margin-top: 0.5rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1493816908507{margin-top: 0.55rem !important;}.vc_custom_1493816974265{margin-top: 1.5rem !important;}.vc_custom_1493816998507{margin-top: 0.5rem !important;}'
						)
				),
		'custom_295' => array(
				'name' => 'Dogs Map',
				'template' => '[vc_row full_width=\"stretch_row_content_no_spaces\"][vc_column icons_position=\"left\"][trx_sc_googlemap style=\"dark\" zoom=\"14\" height=\"441\" markers=\"%5B%7B%22address%22%3A%22Central%20Park%2C%20New%20York%22%2C%22icon%22%3A%22468%22%7D%5D\" title_style=\"default\"][/trx_sc_googlemap][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				),
		'custom_484' => array(
				'name' => 'Dogs OnLeash App',
				'template' => '[vc_row full_width=\"stretch_row\" equal_height=\"yes\" content_placement=\"middle\" css=\".vc_custom_1494229507068{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-app.jpg?id=341) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\" css=\".vc_custom_1494229598003{margin-top: -0.7rem !important;}\"][/vc_column][vc_column width=\"5/12\" icons_position=\"left\"][vc_single_image image=\"340\" img_size=\"full\" alignment=\"right\"][/vc_column][vc_column width=\"1/2\" icons_position=\"left\" offset=\"vc_hidden-lg vc_hidden-md vc_hidden-sm\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\"][/vc_column][vc_column width=\"7/12\" icons_position=\"left\" offset=\"vc_col-md-offset-1 vc_col-md-6 vc_col-sm-offset-0\"][trx_sc_title title_style=\"default\" title_tag=\"h1\" title_align=\"left\" title=\"OnLeash App\" subtitle=\"Download For Free\" description=\"{{“ Our App is like having a dog-loving neighbor! ”}}

It makes dog ownership easier by connecting dog owners with dog lovers where they can hire on-demand for dog walking, dog sitting, or dog boarding services 7-days a week. \"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\" css=\".vc_custom_1494226397522{margin-top: 1.8rem !important;}\"][vc_row_inner][vc_column_inner width=\"5/12\" icons_position=\"left\"][trx_sc_button type=\"default\" color_style=\"dark\" align=\"left\" text_align=\"left\" icon=\"http://onleash.ancorathemes.com/wp-content/themes/onleash/trx_addons/css/icons.png/apple.png\" icon_position=\"left\" title=\"Apple Store\" subtitle=\"download on the\" link=\"/contacts/\"][/vc_column_inner][vc_column_inner width=\"1/2\" icons_position=\"left\" offset=\"vc_hidden-lg vc_hidden-md vc_hidden-sm\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\"][/vc_column_inner][vc_column_inner width=\"5/12\" icons_position=\"left\"][trx_sc_button type=\"default\" color_style=\"link2\" align=\"left\" text_align=\"left\" icon=\"http://onleash.ancorathemes.com/wp-content/themes/onleash/trx_addons/css/icons.png/play_market.png\" icon_position=\"left\" title=\"Google Play\" subtitle=\"download on the\" link=\"/contacts/\"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494229647803{margin-top: 0.3rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494229507068{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-app.jpg?id=341) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1494229598003{margin-top: -0.7rem !important;}.vc_custom_1494226397522{margin-top: 1.8rem !important;}.vc_custom_1494229647803{margin-top: 0.3rem !important;}'
						)
				),
		'custom_354' => array(
				'name' => 'Dogs Price home 2',
				'template' => '[vc_row css=\".vc_custom_1494249316504{background-color: #f5f4f0 !important;}\"][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494159310838{margin-top: 0.7rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Welcome to OnLeash\" subtitle=\"Premier Pet Services\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\" css=\".vc_custom_1494159345357{margin-top: 1.85rem !important;}\"][/vc_column][/vc_row][vc_row][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Dog Walking Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}16|walk\" image=\"166\" price_period=\" from\" subtitle=\"Group & private\" description=\"We provide individualized daily dog walking that is tailored to fit your needs.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Daily Visits Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}23|visit\" image=\"164\" price_period=\" from\" subtitle=\"Walks & feeding\" description=\"Our daily pet visits make sure your pet will get the same care as usual.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Pet Sitting Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}16|day\" image=\"162\" price_period=\" from\" subtitle=\"Overnight sitting\" description=\"No stress or worrying about your pet and home while you are traveling.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][/vc_row][vc_row][vc_column width=\"1/3\" icons_position=\"left\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494159749317{margin-top: 0.9rem !important;}\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494249316504{background-color: #f5f4f0 !important;}.vc_custom_1494159310838{margin-top: 0.7rem !important;}.vc_custom_1494159345357{margin-top: 1.85rem !important;}.vc_custom_1494159749317{margin-top: 0.9rem !important;}'
						)
				),
		'custom_278' => array(
				'name' => 'Dogs Prices',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494159310838{margin-top: 0.7rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Welcome to OnLeash\" subtitle=\"Premier Pet Services\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\" css=\".vc_custom_1494159345357{margin-top: 1.85rem !important;}\"][/vc_column][/vc_row][vc_row][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Dog Walking Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}16|walk\" image=\"166\" price_period=\" from\" subtitle=\"Group & private\" description=\"We provide individualized daily dog walking that is tailored to fit your needs.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Daily Visits Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}23|visit\" image=\"164\" price_period=\" from\" subtitle=\"Walks & feeding\" description=\"Our daily pet visits make sure your pet will get the same care as usual.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][trx_sc_price icon_type=\"fontawesome\" popular=\"\" currency=\"$\" title=\"Pet Sitting Service\" price=\"{{span class=from}}from{{/span}} {{span class=currency}}${{/span}}16|day\" image=\"162\" price_period=\" from\" subtitle=\"Overnight sitting\" description=\"No stress or worrying about your pet and home while you are traveling.\" link=\"/services-page/\" link_text=\"learn more\"][/trx_sc_price][/vc_column][/vc_row][vc_row][vc_column width=\"1/3\" icons_position=\"left\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\" css=\".vc_custom_1494159629237{margin-top: 1.5rem !important;}\"][trx_sc_button type=\"default\" align=\"center\" icon_position=\"left\" link=\"/appointments/\" title=\"make an appointment\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494159749317{margin-top: 0.9rem !important;}\"][/vc_column][vc_column width=\"1/3\" icons_position=\"left\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494159310838{margin-top: 0.7rem !important;}.vc_custom_1494159345357{margin-top: 1.85rem !important;}.vc_custom_1494159629237{margin-top: 1.5rem !important;}.vc_custom_1494159749317{margin-top: 0.9rem !important;}'
						)
				),
		'custom_299' => array(
				'name' => 'Dogs Tabs',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494224507273{margin-top: 0.8rem !important;}\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Professional Approach\" subtitle=\"To Every Pet\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\"][vc_tta_tabs alignment=\"center\" active_section=\"1\" no_fill_content_area=\"true\"][vc_tta_section title=\"Dog Care\" tab_id=\"1492380321008-2c177f70-25a4\"][trx_sc_promo type=\"blockquote\" title_style=\"default\" image_position=\"left\" size=\"normal\" full_height=\"\" text_align=\"left\" text_paddings=\"\" title=\"Daily Dog Care\" description=\"Hundreds of dog owners trust OnLeash to take care of their beloved furry family members. Give us a try, and you won’t be disappointed!\" image=\"300\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][trx_sc_services type=\"light\" featured=\"icon\" featured_position=\"top\" hide_excerpt=\"1\" no_margin=\"\" icons_animation=\"\" cat=\"23\" orderby=\"post_date\" order=\"desc\" title_style=\"default\"][/trx_sc_promo][/vc_tta_section][vc_tta_section title=\"Cat Care\" tab_id=\"1492380321013-e77b3c2e-3d9d\"][trx_sc_promo type=\"blockquote\" title_style=\"default\" image_position=\"left\" size=\"normal\" full_height=\"\" text_align=\"left\" text_paddings=\"\" title=\"Daily Dog Care\" description=\"Hundreds of dog owners trust OnLeash to take care of their beloved furry family members. Give us a try, and you won’t be disappointed!\" image=\"304\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][trx_sc_services type=\"light\" featured=\"icon\" featured_position=\"top\" hide_excerpt=\"1\" no_margin=\"\" icons_animation=\"\" cat=\"23\" orderby=\"post_date\" order=\"desc\" title_style=\"default\"][/trx_sc_promo][/vc_tta_section][vc_tta_section title=\"Small Animals\" tab_id=\"1492380410009-e6e9feb7-e66a\"][trx_sc_promo type=\"blockquote\" title_style=\"default\" image_position=\"left\" size=\"normal\" full_height=\"\" text_align=\"left\" text_paddings=\"\" title=\"Daily Dog Care\" description=\"Hundreds of dog owners trust OnLeash to take care of their beloved furry family members. Give us a try, and you won’t be disappointed!\" image=\"305\"][vc_empty_space alter_height=\"small\" hide_on_mobile=\"\"][trx_sc_services type=\"light\" featured=\"icon\" featured_position=\"top\" hide_excerpt=\"1\" no_margin=\"\" icons_animation=\"\" cat=\"23\" orderby=\"post_date\" order=\"desc\" title_style=\"default\"][/trx_sc_promo][/vc_tta_section][/vc_tta_tabs][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494139616310{margin-top: 1.5rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494224507273{margin-top: 0.8rem !important;}.vc_custom_1494139616310{margin-top: 1.5rem !important;}'
						)
				),
		'custom_319' => array(
				'name' => 'Dogs Team',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493887685403{margin-top: 0.8rem !important;}\"][trx_sc_team type=\"short\" orderby=\"post_date\" order=\"desc\" title_style=\"default\" title_tag=\"h2\" title_align=\"center\" count=\"2\" columns=\"2\" title=\"Meet Our Featured\" subtitle=\"Dog Walkers\" link=\"/about-us-style-1/\" link_text=\"view all team\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494225303514{margin-top: 0.95rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1493887685403{margin-top: 0.8rem !important;}.vc_custom_1494225303514{margin-top: 0.95rem !important;}'
						)
				),
		'custom_471' => array(
				'name' => 'Dogs Testimonials',
				'template' => '[vc_row][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493812919913{margin-top: 1.3rem !important;}\"][vc_single_image image=\"425\" img_size=\"full\" alignment=\"center\"][vc_empty_space hide_on_mobile=\"\" css=\".vc_custom_1493812955232{margin-top: 0.5rem !important;}\"][/vc_column][vc_column width=\"2/3\" icons_position=\"left\" offset=\"vc_col-sm-offset-2\"][trx_sc_testimonials type=\"default\" orderby=\"post_date\" order=\"desc\" slider=\"1\" slider_pagination=\"right\" slider_pagination_thumbs=\"\" title_style=\"default\" title_tag=\"h1\" title_align=\"center\" count=\"3\" columns=\"1\" title=\"What Clients Say\" subtitle=\"Customer Reviews\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493816523202{margin-top: 1.6rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1493812919913{margin-top: 1.3rem !important;}.vc_custom_1493812955232{margin-top: 0.5rem !important;}.vc_custom_1493816523202{margin-top: 1.6rem !important;}'
						)
				),
		'custom_313' => array(
				'name' => 'Dogs Testimonials Inverse',
				'template' => '[vc_row full_width=\"stretch_row\" css=\".vc_custom_1494230005301{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-review.jpg?id=331) !important;}\"][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493812919913{margin-top: 1.3rem !important;}\"][vc_single_image image=\"425\" img_size=\"full\" alignment=\"center\"][vc_empty_space hide_on_mobile=\"\" css=\".vc_custom_1493812955232{margin-top: 0.5rem !important;}\"][/vc_column][vc_column width=\"2/3\" icons_position=\"left\" offset=\"vc_col-sm-offset-2\"][trx_sc_testimonials type=\"default\" orderby=\"post_date\" order=\"desc\" slider=\"1\" slider_pagination=\"right\" slider_pagination_thumbs=\"\" title_style=\"inverse\" title_tag=\"h1\" title_align=\"center\" count=\"3\" columns=\"1\" title=\"What Clients Say\" subtitle=\"Customer Reviews\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493816523202{margin-top: 1.6rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494230005301{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-review.jpg?id=331) !important;}.vc_custom_1493812919913{margin-top: 1.3rem !important;}.vc_custom_1493812955232{margin-top: 0.5rem !important;}.vc_custom_1493816523202{margin-top: 1.6rem !important;}'
						)
				),
		'custom_306' => array(
				'name' => 'Dogs Title',
				'template' => '[vc_row full_width=\"stretch_row\" css=\".vc_custom_1492383390987{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-banner.jpg?id=307) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}\"][vc_column width=\"2/3\" icons_position=\"left\" offset=\"vc_col-sm-offset-2\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1493883935509{margin-top: 0.4rem !important;}\"][trx_sc_title title_style=\"inverse_with_icon\" title_tag=\"h1\" title_align=\"center\" color_style=\"dark\" title=\"Keeping Your Pets\" subtitle=\"Happy, Healthy and Safe!\" description=\"You’ll enjoy knowing our dedicated team will do whatever is needed to keep your pets happy, healthy and safe when you’re away from home.\" link=\"/contacts/\" link_text=\"register now\"][vc_empty_space alter_height=\"huge\" hide_on_mobile=\"\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\" css=\".vc_custom_1493886972388{margin-top: 1rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1492383390987{background-image: url(http://onleash.ancorathemes.com/wp-content/uploads/2017/04/bg-banner.jpg?id=307) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1493883935509{margin-top: 0.4rem !important;}.vc_custom_1493886972388{margin-top: 1rem !important;}'
						)
				),
		'custom_492' => array(
				'name' => 'Dogs Welcome',
				'template' => '[vc_row equal_height=\"yes\" content_placement=\"middle\"][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494248349078{margin-top: 1.4rem !important;}\"][/vc_column][vc_column width=\"7/12\" icons_position=\"left\"][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"1\"][trx_sc_title title_style=\"default\" title_tag=\"h2\" title_align=\"center\" title=\"Welcome to OnLeash\" subtitle=\"Premier Pet Services\" description=\"We provide a very warm and friendly environment for your pets. Our professional team made sure your pet always gets the highest level of care, because we treat all pets as if they were our own!

\"][vc_empty_space hide_on_mobile=\"\" css=\".vc_custom_1494248702382{margin-top: 2rem !important;}\"][vc_column_text]
<h4 style=\"text-align: center;\"><span style=\"color: #fbcd69;\">0 800 555 22 11</span></h4>
[/vc_column_text][vc_empty_space alter_height=\"medium\" hide_on_mobile=\"\"][/vc_column][vc_column width=\"5/12\" icons_position=\"left\"][contact-form-7 id=\"494\"][/vc_column][vc_column icons_position=\"left\"][vc_empty_space alter_height=\"large\" hide_on_mobile=\"\" css=\".vc_custom_1494248366446{margin-top: 1.4rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1494248349078{margin-top: 1.4rem !important;}.vc_custom_1494248702382{margin-top: 2rem !important;}.vc_custom_1494248366446{margin-top: 1.4rem !important;}'
						)
				)
		);
?>