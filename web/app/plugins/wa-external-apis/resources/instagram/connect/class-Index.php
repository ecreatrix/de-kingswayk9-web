<?php
/**
 * Connect to the Instagram API and return the results. It's possible
 * to build the url from a connected account (includes access token,
 * account id, account type), endpoint and parameters (hashtag, etc..)
 * as well as a full url such as from the pagination data from some Instagram API requests.
 *
 * Errors from either the Instagram API or from the HTTP request are detected
 * and can be handled.
 * @since 2.0
 */
namespace wa\APIs\Instagram\Connect;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Index::class ) ) {
	class Index {
		// Setup data for various API request
		public function __construct() {}

		public static function connect_all( $reset = false, $accessValues = false, $displayValues = false ) {
			if ( ! $accessValues ) {
				// AccessValues not given, try getting from saved settings
				$instagram = Plugin\Settings\Options::get_settings( Plugin\Instagram\Index::get_key() );

				if ( array_key_exists( 'access', $instagram ) ) {
					$accessValues = $instagram['access'];
				}
			}

			if ( ! $displayValues ) {
				$displayValues = [];
			}

			if ( $accessValues ) {
				// If access values are available, setup objects
				$messages = new Plugin\Instagram\Messages();

				$access = new Access( $reset, $accessValues );
				$access->connect();
				$connection['access'] = $access;

				$profile = new Profile( $reset, $access );
				$profile->connect();
				$connection['profile'] = $profile;

				$media = new Media( $reset, $access );
				$media->connect();
				$connection['media'] = $media;

				$display = new Display( $reset, $displayValues );
				$connection['display'] = $display;
				

				// Merge all messages together
				$messages->merge_objects( $access->get_messages(), $profile->get_messages(), $media->get_messages() );
			} else {
				//$messages->need_app_info();
			}

			return ['messages' => $messages, 'connection' => $connection];
		}

		public static function info_all( $settings ) {
			return [
				'access'  => $settings['access']->get_info(),
				'profile' => $settings['profile']->get_info(),
				'media'   => $settings['media']->get_info(),
				'display' => $settings['display']->get_info()
			];
		}
	}
}