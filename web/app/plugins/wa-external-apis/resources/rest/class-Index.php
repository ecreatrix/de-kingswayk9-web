<?php
namespace wa\APIs\Rest;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {
            $this->init_routes();
        }

        public static function get_route() {
            return Plugin\CODENAME . '/v1';
        }

        public function init_routes() {
            if ( class_exists( Routes::class ) ) {
                new Routes();
            }
        }
    }
}
