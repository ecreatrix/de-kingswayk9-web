<?php
// Add plugin-specific colors and fonts to the custom CSS
if (!function_exists('onleash_trx_addons_my_get_css')) {
    add_filter('onleash_filter_get_css', 'onleash_trx_addons_my_get_css', 10, 4);
    function onleash_trx_addons_my_get_css($css, $colors, $fonts, $scheme = '')
    {

        if (isset($css['colors']) && $colors) {
            $css['colors'] .= <<<CSS

.sc_services_chess .sc_services_item_content:before,
.sc_services_chess .sc_services_item {
    color: {$colors['text']};
    background-color: {$colors['bg_color']};
}
.sc_services_chess .sc_services_item:hover .sc_services_item_content:before {
    background-color: {$colors['text_dark']};
}
.sc_services_chess .sc_services_item:hover {
    color: {$colors['bg_color']};
}
.sc_item_title.sc_item_title_style_inverse,
.sc_item_title.sc_item_title_style_inverse_with_icon {
    color: {$colors['bg_color']};
}
.sc_item_subtitle.sc_item_title_style_inverse,
.sc_item_subtitle.sc_item_title_style_inverse_with_icon {
    color: {$colors['text_link']};
}
.sc_title_inverse_with_icon .sc_item_descr {
    color: {$colors['bg_color_08']};
}
.sc_testimonials_item {
    background-color: {$colors['alter_bg_color']};
}
.sc_testimonials_item_author_subtitle {
    color: {$colors['input_light']};
}
.sc_testimonials_item_author_title {
    color: {$colors['text_dark']};
}
.sc_testimonials_item_content {
    color: {$colors['text']};
}
.sc_testimonials .slider_swiper .slider_pagination_wrap .swiper-pagination-bullet,
.sc_testimonials .slider_swiper_outer .slider_pagination_wrap .swiper-pagination-bullet,
.sc_testimonials .swiper-pagination-custom .swiper-pagination-button {
    background-color: {$colors['text_link']};
}
.vc_row.vc_row-has-fill .sc_testimonials .slider_swiper .slider_pagination_wrap .swiper-pagination-bullet,
.vc_row.vc_row-has-fill .sc_testimonials .slider_swiper_outer .slider_pagination_wrap .swiper-pagination-bullet,
.vc_row.vc_row-has-fill .sc_testimonials .swiper-pagination-custom .swiper-pagination-button {
    background-color: {$colors['bg_color']};
}
.vc_row.vc_row-has-fill .sc_testimonials_item {
    background-color: {$colors['bg_color_007']};
}
.vc_row.vc_row-has-fill .sc_testimonials_item_author_title,
.vc_row.vc_row-has-fill .sc_testimonials_item_content {
    color: {$colors['bg_color']};
}
.vc_row.vc_row-has-fill .sc_testimonials_item_author_subtitle {
    color: {$colors['bg_color_04']};
}
.sc_team_default .sc_team_item {
    background-color: {$colors['bg_color']};
}
.sc_team_default .sc_team_item_subtitle  {
    color: {$colors['input_light']};
}
.sc_services_default .sc_services_item {
    background-color: {$colors['bg_color']};
}
.sc_services.sc_services_numbered .sc_services_columns [class*="trx_addons_column-"] + [class*="trx_addons_column-"] .sc_services_item:after,
.sc_services.sc_services_numbered .sc_services_columns [class*="trx_addons_column-"] .sc_services_item:before {
    border-color: {$colors['alter_bg_color']};
}
.sc_services_default .sc_services_item:hover .sc_services_item_icon {
    background-color: {$colors['bg_color_0']};
}
.sc_team_short .sc_team_item {
    background-color: {$colors['alter_bg_color']};
}
.sc_team_short .socials_wrap .social_item .social_icon {
    color: {$colors['text_link']};
    background-color: {$colors['bg_color']};
}
.sc_team_default_with_socials .socials_wrap .social_item .social_icon {
    color: {$colors['text_link']};
    background-color: {$colors['alter_bg_color']};
}
.sc_team_short .socials_wrap .social_item:hover .social_icon,
.sc_team_default_with_socials .socials_wrap .social_item:hover .social_icon {
    color: {$colors['bg_color']};
    background-color: {$colors['text_link']};
}
.sc_team_short .sc_team_item .post_featured:after {
    background-color: {$colors['alter_bg_color']};
}
.wpcf7-form:not(.on-white) input[type="text"],
.wpcf7-form:not(.on-white) input[type="number"],
.wpcf7-form:not(.on-white) input[type="email"],
.wpcf7-form:not(.on-white) input[type="tel"],
.wpcf7-form:not(.on-white) input[type="search"],
.wpcf7-form:not(.on-white) input[type="password"],
.wpcf7-form:not(.on-white) textarea,
.wpcf7-form:not(.on-white) textarea.wp-editor-area,
.wpcf7-form:not(.on-white) .select_container,
.wpcf7-form:not(.on-white) select,
.wpcf7-form:not(.on-white) .select_container:before,
.wpcf7-form:not(.on-white) .select_container select {
    background-color: {$colors['bg_color']}!important;
}
.sc_promo.sc_promo_blockquote {
    background-color: {$colors['alter_bg_color']};
}
.sc_promo.sc_promo_blockquote blockquote.trx_addons_blockquote_style_1 {
    color: {$colors['text']};
    background-color: {$colors['alter_bg_color']};
}
.sc_promo.sc_promo_blockquote blockquote > a, .sc_promo.sc_promo_blockquote blockquote > p > a, .sc_promo.sc_promo_blockquote blockquote > cite, .sc_promo.sc_promo_blockquote blockquote > p > cite {
    color: {$colors['text_dark']};
}
.sc_promo.sc_promo_blockquote .sc_services_light .sc_services_item_title a {
    color: {$colors['text_dark']};
}
.sc_promo.sc_promo_blockquote .sc_services_light .sc_services_item_title a:hover {
    color: {$colors['text_link']};
}
.sc_team_default_with_socials .sc_team_item_subtitle {
    color: {$colors['input_light']};
}
.vc_row-has-fill .sc_team_default_with_socials .socials_wrap .social_item .social_icon {
    background-color: {$colors['bg_color']};
}
.vc_row-has-fill .sc_team_default_with_socials .socials_wrap .social_item:hover .social_icon {
    background-color: {$colors['text_link']};
}
.sc_layouts_row_type_normal .search_wrap .search_submit,
.scheme_self.sc_layouts_row_type_normal .search_wrap .search_submit {
    color: {$colors['text_link2']};
}
.vc_row-has-fill ul[class*="trx_addons_list"][class*="_circled"]>li:before {
    color: {$colors['text_link']};
    background-color: {$colors['bg_color']};
}
.vc_row-has-fill ul[class*="trx_addons_list"][class*="_circled"]>li {
    color: {$colors['text']};
}
.vc_row-has-fill .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab>a {
    background-color: {$colors['bg_color']};
}
.vc_row-has-fill .sc_promo.sc_promo_blockquote {
    background-color: {$colors['bg_color']};
}

.vc_row-has-fill .sc_promo.sc_promo_blockquote blockquote.trx_addons_blockquote_style_1 {
    background-color: {$colors['bg_color']};
}
.sc_blogger_classic .sc_blogger_item .sc_blogger_item_featured_container .post_meta .post_meta_item a {
    color: {$colors['text_link']};
    background-color: {$colors['bg_color']};
}
.sc_blogger_item {
    background-color: {$colors['bg_color']};
}
.sc_blogger_item_title,
.sc_blogger_item_title a {
    color: {$colors['text_dark']};
}
.sc_blogger_item_title a:hover {
    color: {$colors['text_link']};
}
.sc_price_subtitle {
    color: {$colors['alter_dark']};
}
.sc_price_subtitle,
.sc_price_subtitle_container:before {
    background-color: {$colors['alter_bg_color']};
}
.vc_row-has-fill .sc_price_subtitle, .vc_row-has-fill .sc_price_subtitle_container:before {
    background-color: {$colors['bg_color']};
}
.trx_addons_popup input[type="text"],
.trx_addons_popup input[type="password"] {
    background-color: {$colors['bg_color']};
}
.sc_team_default .sc_team_item_socials .social_item .social_icon,
.team_member_page .team_member_socials .social_item .social_icon {
    color: {$colors['bg_color']};
    background-color: {$colors['text_link']};
}
.sc_team_default .sc_team_item_socials .social_item:hover .social_icon,
.team_member_page .team_member_socials .social_item:hover .social_icon {
    color: {$colors['bg_color']};
    background-color: {$colors['text_link2']};
}
.menu_mobile .search_mobile .search_field {
    background-color: {$colors['bg_color']};
}
.sc_button_subtitle {
color: {$colors['text_light']};

}
.color_style_dark.sc_button .sc_button_subtitle {
color: {$colors['bg_color']};
}
.footer_wrap .scheme_self.vc_row .socials_wrap .social_item .social_icon {
    color: {$colors['text_dark_071']};
    background-color: {$colors['text_dark_006']};
}
.footer_wrap .scheme_self.vc_row .socials_wrap .social_item:hover .social_icon {
    color: {$colors['text_light']};
    background-color: {$colors['text_link']};
}

CSS;
        }

        return $css;
    }
}
