<?php
namespace wa\APIs\Scripts;

use wa\APIs as Plugin;

// Exit if accessed directly.

if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Enqueues::class ) ) {
    class Enqueues {
        public function __construct() {
            // General scripts and styles
            add_action( 'wp_enqueue_scripts', [$this, 'frontend_enqueues'], 99 );

            // Dashboard assets
            add_action( 'admin_enqueue_scripts', [$this, 'admin_assets'] );

            // Gutenberg setup
            add_action( 'enqueue_block_editor_assets', [$this, 'blocks_assets'] );
        }

        public function admin_assets() {
            $this->dashboard_assets();
            $this->settings_assets();
        }

        public static function ajax_parameters() {
            $generic = [
                //'ajax_url' => admin_url( 'admin-ajax.php' ),
                'route_parent' => Plugin\Rest\Index::get_route(),
                'rest_url'     => get_rest_url() . Plugin\CODENAME . '/v1',
                'nonce'        => wp_create_nonce( Plugin\CODENAME . '_nonce' ),

                'admin'        => get_user_meta( get_current_user_id(), 'wa_api_admin', true )
            ];
            $instagram = Plugin\Instagram\Rest::ajax_parameters();

            return array_merge( $generic, $instagram );
        }

        public function blocks_assets() {
            $scripts = [
                [
                    'name'         => 'blocks-vendors',
                    'link'         => 'vendor.blocks',
                    'dependencies' => ['jquery']
                ],
                [
                    'name'         => 'blocks',
                    'dependencies' => ['jquery', 'wp-editor', 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-edit-post', 'wp-compose', 'underscore', 'wp-components', Plugin\SHORTNAME . '-blocks-vendors'],
                    'parameters'   => self::ajax_parameters(),
                    'action'       => 'localize'
                ]
            ];

            ScriptLoader::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name'         => 'blocks',
                    'dependencies' => ['wp-edit-blocks']
                ]
            ];

            ScriptLoader::add_assets( $styles, 'css' );
        }

        public static function dashboard_assets() {
            wp_enqueue_script(
                [
                    'hoverIntent',
                    'wp-color-picker'
                ]
            );

            $scripts = [
                [
                    'name'         => 'admin',
                    'dependencies' => ['jquery']
                ]
            ];

            self::fontawesome();
            ScriptLoader::add_assets( $scripts, 'js' );

            if ( ! wp_script_is( 'jquery-ui-draggable' ) ) {
                wp_enqueue_script(
                    [
                        'jquery',
                        'jquery-ui-core',
                        'jquery-ui-draggable'
                    ]
                );
            }

            $styles = [
                [
                    'name' => 'admin'
                ]
            ];

            wp_enqueue_style( 'wp-color-picker' );
            ScriptLoader::add_assets( $styles, 'css' );
        }

        public static function fontawesome() {
            wp_deregister_script( 'ec-fontawesome' );
            wp_deregister_script( 'wa-fontawesome' );

            $scripts = [
                [
                    'name'            => 'wa-fontawesome',
                    'link'            => 'https://kit.fontawesome.com/9e2a71fbfe.js',
                    'add_name_prefix' => false,
                    'extras'          => 'crossorigin="anonymous"'
                ]
            ];

            ScriptLoader::add_assets( $scripts, 'js' );
        }

        public function frontend_enqueues() {
            $this->project_assets();
        }

        public static function project_assets() {
            $scripts = [
                [
                    'name'         => 'main',
                    'dependencies' => ['jquery', 'wa-slick']
                ]
            ];

            self::fontawesome();
            self::slick();
            ScriptLoader::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'main'
                ]
            ];

            ScriptLoader::add_assets( $styles, 'css' );
        }

        public function settings_assets() {
            $screen = get_current_screen();

            if ( strpos( $screen->base, 'toplevel_page_' . Plugin\CODENAME ) === false && strpos( $screen->base, 'page_' . Plugin\CODENAME ) === false ) { // only load settings assets on settings page
                return;
            }

            $scripts = [
                [
                    'name'         => 'settings-vendors',
                    'link'         => 'vendor.settings',
                    'dependencies' => ['jquery']
                ],
                'settings' => [
                    'name'         => 'settings',
                    'dependencies' => ['jquery', 'wp-editor', 'wp-i18n', 'wp-element', 'wp-edit-post', 'wp-compose', 'underscore', 'wp-components', Plugin\SHORTNAME . '-settings-vendors', 'wp-api-request', 'wp-api-fetch'],
                    'parameters'   => self::ajax_parameters(),
                    'action'       => 'localize'
                ]
            ];

            ScriptLoader::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'settings'
                ]
            ];
            ScriptLoader::add_assets( $styles, 'css' );
        }

        public static function slick( ) {
            wp_deregister_script( 'ec-slick' );
            wp_deregister_script( 'wa-slick' );

            $scripts = [
                [
                    'name'            => 'wa-slick',
                    'link'            => 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
                    'add_name_prefix' => false,
                    'extras'          => 'data-search-pseudo-elements defer crossorigin="anonymous"'
                ]
            ];

            ScriptLoader::add_assets( $scripts, 'js' );
        }
    }
}
