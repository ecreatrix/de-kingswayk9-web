<?php
/**
 * The template to display menu in the footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */

// Footer menu
$onleash_menu_footer = onleash_get_nav_menu(array(
											'location' => 'menu_footer',
											'class' => 'sc_layouts_menu sc_layouts_menu_default'
											));
if (!empty($onleash_menu_footer)) {
	?>
	<div class="footer_menu_wrap">
		<div class="footer_menu_inner">
			<?php onleash_show_layout($onleash_menu_footer); ?>
		</div>
	</div>
	<?php
}
?>