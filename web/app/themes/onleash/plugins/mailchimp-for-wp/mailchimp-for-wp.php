<?php
/* Mail Chimp support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('onleash_mailchimp_theme_setup9')) {
	add_action( 'after_setup_theme', 'onleash_mailchimp_theme_setup9', 9 );
	function onleash_mailchimp_theme_setup9() {
		if (onleash_exists_mailchimp()) {
			add_action( 'wp_enqueue_scripts',							'onleash_mailchimp_frontend_scripts', 1100 );
			add_filter( 'onleash_filter_merge_styles',					'onleash_mailchimp_merge_styles');
		}
		if (is_admin()) {
			add_filter( 'onleash_filter_tgmpa_required_plugins',		'onleash_mailchimp_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'onleash_exists_mailchimp' ) ) {
	function onleash_exists_mailchimp() {
		return function_exists('__mc4wp_load_plugin') || defined('MC4WP_VERSION');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'onleash_mailchimp_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('onleash_filter_tgmpa_required_plugins',	'onleash_mailchimp_tgmpa_required_plugins');
	function onleash_mailchimp_tgmpa_required_plugins($list=array()) {
		if (in_array('mailchimp-for-wp', onleash_storage_get('required_plugins')))
			$list[] = array(
				'name' 		=> esc_html__('MailChimp for WP', 'onleash'),
				'slug' 		=> 'mailchimp-for-wp',
				'required' 	=> false
			);
		return $list;
	}
}



// Custom styles and scripts
//------------------------------------------------------------------------

// Enqueue custom styles
if ( !function_exists( 'onleash_mailchimp_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'onleash_mailchimp_frontend_scripts', 1100 );
	function onleash_mailchimp_frontend_scripts() {
		if (onleash_exists_mailchimp()) {
			if (onleash_is_on(onleash_get_theme_option('debug_mode')) && onleash_get_file_dir('plugins/mailchimp-for-wp/mailchimp-for-wp.css')!='')
				wp_enqueue_style( 'onleash-mailchimp-for-wp',  onleash_get_file_url('plugins/mailchimp-for-wp/mailchimp-for-wp.css'), array(), null );
		}
	}
}
	
// Merge custom styles
if ( !function_exists( 'onleash_mailchimp_merge_styles' ) ) {
	//Handler of the add_filter( 'onleash_filter_merge_styles', 'onleash_mailchimp_merge_styles');
	function onleash_mailchimp_merge_styles($list) {
		$list[] = 'plugins/mailchimp-for-wp/mailchimp-for-wp.css';
		return $list;
	}
}


// Add plugin-specific colors and fonts to the custom CSS
if (onleash_exists_mailchimp()) { require_once ONLEASH_THEME_DIR . 'plugins/mailchimp-for-wp/mailchimp-for-wp.styles.php'; }
?>