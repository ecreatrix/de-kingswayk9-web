<?php
/**
 * The template to display default site header
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_header_css   = $onleash_header_image   = '';
$onleash_header_video = onleash_get_header_video();
if (true || empty($onleash_header_video)) {
    $onleash_header_image = get_header_image();
    if (onleash_is_on(onleash_get_theme_option('header_image_override')) && apply_filters('onleash_filter_allow_override_header_image', true)) {
        if (is_category()) {
            if (($onleash_cat_img = onleash_get_category_image()) != '') {
                $onleash_header_image = $onleash_cat_img;
            }

        } else if (is_singular() || onleash_storage_isset('blog_archive')) {
            if (has_post_thumbnail()) {
                $onleash_header_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                if (is_array($onleash_header_image)) {
                    $onleash_header_image = $onleash_header_image[0];
                }

            } else {
                $onleash_header_image = '';
            }

        }
    }
}

?><header class="top_panel top_panel_default<?php
echo !empty($onleash_header_image) || !empty($onleash_header_video) ? ' with_bg_image' : ' without_bg_image';
if ('' != $onleash_header_video) {
    echo ' with_bg_video';
}

if ('' != $onleash_header_image) {
    echo ' ' . esc_attr(onleash_add_inline_css_class('background-image: url(' . esc_url($onleash_header_image) . ');'));
}

if (is_single() && has_post_thumbnail()) {
    echo ' with_featured_image';
}

if (onleash_is_on(onleash_get_theme_option('header_fullheight'))) {
    echo ' header_fullheight trx-stretch-height';
}

?> scheme_<?php echo esc_attr(onleash_is_inherit(onleash_get_theme_option('header_scheme'))
    ? onleash_get_theme_option('color_scheme')
    : onleash_get_theme_option('header_scheme'));
?>">
    <div class="vc_row sc_layouts_row sc_layouts_row_type_compact">
        <div class="content_wrap">
            <div class="columns_wrap">
                <div class="sc_layouts_column sc_layouts_column_align_left sc_layouts_column_icons_position_left column-1_4">
                    <div class="sc_layouts_item"><?php
get_template_part('templates/header-logo');
?></div>
                </div><div class="sc_layouts_column sc_layouts_column_align_right sc_layouts_column_icons_position_left column-3_4">
                    <?php if (onleash_is_on(onleash_get_theme_option('header_login'))) {?>
                        <div class="sc_layouts_item sc_layouts_hide_on_mobile">

                        </div>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
    <?php
// Main menu
if (onleash_get_theme_option("menu_style") == 'top') {
    ?>
    <div class="vc_row sc_layouts_row sc_layouts_row_type_normal sc_layouts_row_fixed">
        <div class="content_wrap">
            <div class="columns_wrap">
                <div class="sc_layouts_column sc_layouts_column_align_left sc_layouts_column_icons_position_left column-3_4">
                    <div class="sc_layouts_item">
                        <?php
// Main menu
    $onleash_menu_main = onleash_get_nav_menu([
        'location' => 'menu_main',
        'class'    => 'sc_layouts_menu sc_layouts_menu_default sc_layouts_hide_on_mobile',
    ]
    );
    if (empty($onleash_menu_main)) {
        $onleash_menu_main = onleash_get_nav_menu([
            'class' => 'sc_layouts_menu sc_layouts_menu_default sc_layouts_hide_on_mobile',
        ]
        );
    }
    onleash_show_layout($onleash_menu_main);
    ?>
                        <div class="sc_layouts_iconed_text sc_layouts_menu_mobile_button">
                            <a class="sc_layouts_item_link sc_layouts_iconed_text_link" href="#">
                                <span class="sc_layouts_item_icon sc_layouts_iconed_text_icon trx_addons_icon-menu"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

    <?php

// Background video
if (!empty($onleash_header_video)) {
    get_template_part('templates/header-video');
}

// Page title and breadcrumbs area
get_template_part('templates/header-title');

// Header widgets area
get_template_part('templates/header-widgets');

?></header>