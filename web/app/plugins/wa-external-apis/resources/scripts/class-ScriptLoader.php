<?php
// Exit if accessed directly.
namespace wa\APIs\Scripts;

use wa\APIs as APIs;

if (  ! defined( 'ABSPATH' ) ) {exit;}
if (  ! class_exists( ScriptLoader::class ) ) {
	class ScriptLoader {
		public function __construct() {}

		public static function add_assets( $assets, $ext, $debug = false ) {
			if (  ! is_array( $assets ) || empty( $assets ) ) {
				return;
			}
			$dev = apply_filters( 'wa_theme_get_dev', false );

			foreach ( $assets as $asset ) {
				if (  ! array_key_exists( 'link', $asset ) ) {
					$asset['link'] = $asset['name'];
				}
				if (  ! array_key_exists( 'script_object', $asset ) ) {
					$asset['script_object'] = 'rest';
				}
				$asset = self::merge_default( $asset );

				if ( $dev ) {
					// Add non-minimised for dev env
					$dev_asset = self::enqueue( $asset, $ext, false, $debug );

					if (  ! $dev_asset ) {
						// Add minimised version
						self::enqueue( $asset, $ext, true, $debug );
					}
				} else {
					// Add minimised version for prod env or non-minimised if mimimised not available
					$prod_asset = self::enqueue( $asset, $ext, true, $debug );

					if (  ! $prod_asset ) {
						// Add full version
						self::enqueue( $asset, $ext, false, $debug );
					}
				}
			}
		}

		public static function asset_version() {
			return APIs\Settings\Options::get_field( 'devMode' );
		}

		// Default WP enqueue/register as well as return for local usage
		public static function enqueue( $asset, $ext = 'css', $minimised = false, $debug = false ) {
			//var_dump( $asset );
			$info = self::get_file_info( $asset, $ext, $minimised );

			$version = self::get_file_time( $asset, $ext, $info );

			$link = self::get_file_path( $asset, $ext, $info, $debug );

			if (  ! $link ) {
				return false;
			}

			$name = $asset['name'];
			if ( $asset['add_name_prefix'] ) {
				$name = $info['shortname'] . '-' . $name;
			}

			$dependencies = $asset['dependencies'];

			if ( $debug ) {
				var_dump( $name );
				var_dump( $asset );
			}

			if ( 'css' === $ext || 'min.css' === $ext ) {
				// Used to add integrity or other attributes to script tag
				if ( $asset['extras'] ) {
					$extras = $asset['extras'];

					add_filter(
						'style_loader_tag',
						function ( $html, $handle ) use ( $name, $extras ) {
							if ( in_array( $handle, [$name], true ) ) {
								return preg_replace(
									'/^<style /',
									'<style ' . $extras . ' ',
									$html,
									1
								);
							} else {
								return $html;
							}
						},
						10,
						2
					);
				}

				if ( 'enqueue' === $asset['action'] ) {
					wp_enqueue_style( $name, $link, $dependencies, $version );

					return true;
				} else if ( 'register' === $asset['action'] ) {
					wp_register_style( $name, $link, $dependencies, $version );

					return true;
				}
			} else if ( 'js' === $ext || 'min.js' === $ext ) {
				if ( $asset['extras'] ) {
					$extras = $asset['extras'];

					add_filter(
						'script_loader_tag',
						function ( $html, $handle ) use ( $name, $extras ) {
							if ( in_array( $handle, [$name], true ) ) {
								return preg_replace(
									'/^<script /',
									'<script ' . $extras . ' ',
									$html,
									1
								);
							} else {
								return $html;
							}
						},
						10,
						2
					);
				}

				if ( 'enqueue' === $asset['action'] ) {
					wp_enqueue_script( $name, $link, $dependencies, $version, true );

					return true;
				} else if ( 'register' === $asset['action'] ) {
					wp_register_script( $name, $link, $dependencies, $version, true );

					return true;
				} else if ( 'localize' === $asset['action'] || 'ajax' === $asset['action'] ) {
					// AJAX script, add script and localize parameters for that script
					$script_object = $asset['script_object'];
					if ( $asset['add_name_prefix'] ) {
						$script_object = $info['codename'] . '_' . $script_object;
					}

					$parameters = [];
					if ( array_key_exists( 'parameters', $asset ) ) {
						$parameters = $asset['parameters'];
					}

					wp_enqueue_script( $name, $link, $dependencies, $version, true );

					wp_localize_script( $name, $script_object, $parameters );

					return true;
				}
			}

			return false;
		}

		public static function get_file_info( $asset, $ext, $minimised ) {
			$location = $asset['top'];
			$link     = $asset['link'];

			if ( '' !== $asset['name'] &&
				( 'localize' === $asset['action'] || 'ajax' === $asset['action'] )
			) {
				$link = $asset['name'];
			}

			$return = [];

			$return['type'] = 'local';
			if ( strpos( $link, 'http' ) !== false ) {
				// Live link
				$return['type'] = 'external';
			}

			if ( 'js' === $ext || 'min.js' === $ext ) {
				$location = 'assets/scripts/' . $location;
			} else if ( 'css' === $ext || 'min.css' === $ext ) {
				$location = 'assets/styles/' . $location;
			} else {
				return false;
			}

			if ( 'css' === $ext && ! $minimised ) {
				$link .= '.' . APIs\STYLE;
			}

			if ( $minimised && 'external' !== $return['type'] ) {
				$link .= '.min';
			}

			if ( $asset['add_ext'] && $link && strpos( $link, '.js' ) === false && strpos( $link, '.css' ) === false ) {
				// Add extension if none set

				$link .= '.' . $ext;
			}

			$return['link'] = $link;
			$return['path'] = APIs\PATH  . $location;

			$return['uri']     = APIs\URI  . $location;
			$return['version'] = APIs\VERSION;

			$return['shortname'] = APIs\SHORTNAME;
			$return['codename']  = APIs\CODENAME;

			return $return;
		}

		public static function get_file_path( $asset, $ext = 'js', $info = [], $debug = false ) {
			$uri   = $info['uri'];
			$path  = $info['path'];
			$link  = $info['link'];
			$found = '';

			if ( 'external' === $info['type'] ) {
				// Live link
				$found = 'live link: ' . $uri;
			} else {
				// Local asset
				if ( file_exists( $path . $link ) ) {
					$found = 'found: ' . $path . $link;

					$link = $uri . $link;
				} else {
					$found = 'file not found: ' . $path . $link;

					$link = false;
				}
			}

			if ( $debug ) {
				var_dump( $found );
			}

			return $link;
		}

		public static function get_file_time( $asset, $ext = 'js', $info = [] ) {
			$uri  = $info['uri'];
			$path = $info['path'];
			$link = $info['link'];

			// Get file version. Plugin/Theme version for external scripts and file edit time for internal scripts
			$version = $info['version'];

			if ( strpos( $link, 'http' ) === false && file_exists( $path . $link ) ) {
				$version = filemtime( $path . $link );
			}

			if ( 'external' === $info['type'] ) {
				// Live link
				$version = null;
			}

			return $version;
		}

		/* $assets = [
		'script_object'   => rest parameters to be included,
		'top'             => asset folder,
		'name'            => name used for registering,
		'link'            => link for in plugin/theme, url for outside,
		'dependencies'    => array of script names that should load first,
		'action'          => 'action' to enqueue, 'register' just to register, and false to return link
		'footer'          => true to add to footer, false for header (for scripts),
		'add_name_prefix' => add plugin/theme codename to asset registration,
		'add_ext'         => add .js/.css to filename,
		'extras'          => add bonus bits to script tag,
		],
		 */
		public static function merge_default( $asset ) {
			$default_asset = [
				'script_object'   => false,
				'top'             => '',
				'name'            => 'main',
				'link'            => 'main',
				'dependencies'    => '',
				'action'          => 'enqueue',
				'footer'          => true,
				'add_name_prefix' => true,
				'add_ext'         => true,
				'extras'          => false,
			];

			return array_merge( $default_asset, $asset );
		}
	}
}