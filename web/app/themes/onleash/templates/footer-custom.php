<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */

$onleash_footer_scheme =  onleash_is_inherit(onleash_get_theme_option('footer_scheme')) ? onleash_get_theme_option('color_scheme') : onleash_get_theme_option('footer_scheme');
$onleash_footer_id = str_replace('footer-custom-', '', onleash_get_theme_option("footer_style"));
$onleash_footer_meta = get_post_meta($onleash_footer_id, 'trx_addons_options', true);
?>
<footer class="footer_wrap footer_custom footer_custom_<?php echo esc_attr($onleash_footer_id); 
						?> footer_custom_<?php echo esc_attr(sanitize_title(get_the_title($onleash_footer_id))); 
						if (!empty($onleash_footer_meta['margin']) != '') 
							echo ' '.esc_attr(onleash_add_inline_css_class('margin-top: '.esc_attr(onleash_prepare_css_value($onleash_footer_meta['margin'])).';'));
						?> scheme_<?php echo esc_attr($onleash_footer_scheme); 
						?>">
	<?php
    // Custom footer's layout
    do_action('onleash_action_show_layout', $onleash_footer_id);
	?>
</footer><!-- /.footer_wrap -->
