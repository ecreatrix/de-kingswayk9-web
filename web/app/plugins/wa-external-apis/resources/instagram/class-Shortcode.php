<?php
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Shortcode::class ) ) {
	class Shortcode {
		public function __construct() {
			add_shortcode( 'instagram-feed', [$this, 'display_instagram'] );
		}

		public static function display_instagram( $atts ) {
			$atts = shortcode_atts(
				[
					'type' => 'carousel'
				], $atts, 'instagram-feed' );

			return Content::output( $atts['type'], false );
		}
	}
}
?>