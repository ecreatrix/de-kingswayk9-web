<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_blog_style = explode('_', onleash_get_theme_option('blog_style'));
$onleash_columns = empty($onleash_blog_style[1]) ? 2 : max(2, $onleash_blog_style[1]);
$onleash_expanded = !onleash_sidebar_present() && onleash_is_on(onleash_get_theme_option('expand_content'));
$onleash_post_format = get_post_format();
$onleash_post_format = empty($onleash_post_format) ? 'standard' : str_replace('post-format-', '', $onleash_post_format);
$onleash_animation = onleash_get_theme_option('blog_animation');

?><div class="<?php echo $onleash_blog_style[0] == 'classic' ? 'column' : 'masonry_item masonry_item'; ?>-1_<?php echo esc_attr($onleash_columns); ?>"><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_format_'.esc_attr($onleash_post_format)
					. ' post_layout_classic post_layout_classic_'.esc_attr($onleash_columns)
					. ' post_layout_'.esc_attr($onleash_blog_style[0]) 
					. ' post_layout_'.esc_attr($onleash_blog_style[0]).'_'.esc_attr($onleash_columns)
					); ?>
	<?php echo (!onleash_is_off($onleash_animation) ? ' data-animation="'.esc_attr(onleash_get_animation_classes($onleash_animation)).'"' : ''); ?>>
	<?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	// Featured image
	onleash_show_post_featured( array( 'thumb_size' => onleash_get_thumb_size($onleash_blog_style[0] == 'classic'
													? (strpos(onleash_get_theme_option('body_style'), 'full')!==false 
															? ( $onleash_columns > 2 ? 'big' : 'huge' )
															: (	$onleash_columns > 2
																? ($onleash_expanded ? 'med' : 'small')
																: ($onleash_expanded ? 'big' : 'med')
																)
														)
													: (strpos(onleash_get_theme_option('body_style'), 'full')!==false 
															? ( $onleash_columns > 2 ? 'masonry-big' : 'full' )
															: (	$onleash_columns <= 2 && $onleash_expanded ? 'masonry-big' : 'masonry')
														)
								) ) );

	if ( !in_array($onleash_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php 
			do_action('onleash_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h4 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );

			do_action('onleash_action_before_post_meta'); 

			// Post meta
            onleash_show_post_meta(array(
                    'categories' => false,
                    'date' => true,
                    'edit' => false,
                    'seo' => false,
                    'share' => false,
                    'counters' => 'comments'	//comments,likes,views - comma separated in any combination
                )
            );
			?>
		</div><!-- .entry-header -->
		<?php
	}		
	?>

	<div class="post_content entry-content">
		<div class="post_content_inner">
			<?php
			$onleash_show_learn_more = false; //!in_array($onleash_post_format, array('link', 'aside', 'status', 'quote'));
			if (has_excerpt()) {
				the_excerpt();
			} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
				the_content( '' );
			} else if (in_array($onleash_post_format, array('link', 'aside', 'status'))) {
				the_content();
			} else if ($onleash_post_format == 'quote') {
				if (($quote = onleash_get_tag(get_the_content(), '<blockquote>', '</blockquote>'))!='')
					onleash_show_layout(wpautop($quote));
				else
					the_excerpt();
			} else if (substr(get_the_content(), 0, 1)!='[') {
				the_excerpt();
			}
			?>
		</div>
		<?php
		// Post meta
		if (in_array($onleash_post_format, array('link', 'aside', 'status', 'quote'))) {
			onleash_show_post_meta(array(
				'share' => false,
				'counters' => 'comments'
				)
			);
		}
		// More button
		if ( $onleash_show_learn_more ) {
			?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'onleash'); ?></a></p><?php
		}
		?>
	</div><!-- .entry-content -->

</article></div>