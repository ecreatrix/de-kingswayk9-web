<?php
/**
 * The template to display the background video in the header
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.14
 */
$onleash_header_video = onleash_get_header_video();
$onleash_embed_video = '';
if (!empty($onleash_header_video) && !onleash_is_from_uploads($onleash_header_video)) {
	if (onleash_is_youtube_url($onleash_header_video) && preg_match('/[=\/]([^=\/]*)$/', $onleash_header_video, $matches) && !empty($matches[1])) {
		?><div id="background_video" data-youtube-code="<?php echo esc_attr($matches[1]); ?>"></div><?php
	} else {
		global $wp_embed;
		if (false && is_object($wp_embed)) {
			$onleash_embed_video = do_shortcode($wp_embed->run_shortcode( '[embed]' . trim($onleash_header_video) . '[/embed]' ));
			$onleash_embed_video = onleash_make_video_autoplay($onleash_embed_video);
		} else {
			$onleash_header_video = str_replace('/watch?v=', '/embed/', $onleash_header_video);
			$onleash_header_video = onleash_add_to_url($onleash_header_video, array(
				'feature' => 'oembed',
				'controls' => 0,
				'autoplay' => 1,
				'showinfo' => 0,
				'modestbranding' => 1,
				'wmode' => 'transparent',
				'enablejsapi' => 1,
				'origin' => home_url(),
				'widgetid' => 1
			));
			$onleash_embed_video = '<iframe src="' . esc_url($onleash_header_video) . '" width="1170" height="658" allowfullscreen="0" frameborder="0"></iframe>';
		}
		?><div id="background_video"><?php onleash_show_layout($onleash_embed_video); ?></div><?php
	}
}
?>