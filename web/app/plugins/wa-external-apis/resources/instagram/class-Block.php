<?php
/**
 * Instagram block.
 *
 * @package wain
 */
namespace wa\APIs\Instagram;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Block::class ) ) {
    class Block {
        public function __construct() {
            add_action( 'init', [$this, 'init'] );
            add_action( 'init', [$this, 'registerSettings'] );
        }

        public function block_attributes( $attributes ) {
            $attributes = array_merge( [
                'variant'            => 'masonry',
                'accessToken'        => get_option( Plugin\CODENAME . 'instagram_access_token' ),
                'count'              => 8,

                'equal'              => false,
                'crop'               => false,
                'verticalAlign'      => false,

                'columns'            => 4,
                'smColumns'          => '',
                'mdColumns'          => '',
                'lgColumns'          => '',
                'xlColumns'          => '',

                'showProfile'        => true,
                'showProfileAvatar'  => true,
                'profileAvatarSize'  => 70,
                'showProfileName'    => true,
                'showProfileBio'     => true,
                'showProfileWebsite' => true,
                'showProfileStats'   => true,
                'className'          => '',

                'effect'             => 'slide',
                'speed'              => 0.5,
                'autoplay'           => 4,
                'slidesPerView'      => 3,
                'centeredSlides'     => true,
                'loop'               => true,
                'freeScroll'         => false,

                'showArrows'         => true,
                'arrowPrevIcon'      => 'fas fa-angle-left',
                'arrowNextIcon'      => 'fas fa-angle-right',
                'showBullets'        => false,
                'dynamicBullets'     => false,

                'gap'                => 15
            ], $attributes );

            $attributes['showProfile'] = $attributes['showProfile'] && ( $attributes['showProfileAvatar'] || $attributes['showProfileName'] || $attributes['showProfileBio'] || $attributes['showProfileWebsite'] || $attributes['showProfileStats'] );

            return $attributes;
        }

        public function get_data_attributes( $attributes ) {
            $data = [];

            if ( 'swipe' === $attributes['variant'] ) {
                $data['data-effect'] = $attributes['effect'];
                $data['data-speed'] = $attributes['speed'];
                $data['data-autoplay'] = $attributes['autoplay'];
                $data['data-slides-per-view'] = $attributes['slidesPerView'];
                $data['data-centered-slides'] = $attributes['centeredSlides'] ? 'true' : 'false';
                $data['data-loop'] = $attributes['loop'] ? 'true' : 'false';
                $data['data-free-scroll'] = $attributes['freeScroll'] ? 'true' : 'false';
                $data['data-show-arrows'] = $attributes['showArrows'] ? 'true' : 'false';
                $data['data-show-bullets'] = $attributes['showBullets'] ? 'true' : 'false';
                $data['data-dynamic-bullets'] = $attributes['dynamicBullets'] ? 'true' : 'false';
                $data['data-gap'] = $attributes['gap'];
            }

            $combined = [];

            foreach ( $data as $key => $value ) {
                if ( $value && '' !== $value ) {
                    $combined[] = $key . '="' . $value . '";';
                }
            }
            $combined = implode( array_filter( $combined ), ' ' );

            if ( '' !== $combined ) {
                $combined = ' ' . $combined;
            }

            return $combined;
        }

        /**
         * Get instagram feed from REST
         *
         * @param  Array   $data - data to get feed.
         * @return array
         */
        public function get_feed( $data ) {
            $request = new \WP_REST_Request( 'GET', '/' . Plugin\BLOCK . '/v1/get_feed' );
            $request->set_query_params( $data );

            $response = rest_do_request( $request );

            $server = rest_get_server();

            $data = $server->response_to_data( $response, false );
            $data = isset( $data['response'] ) ? $data['response'] : false;

            return isset( $data['data'] ) ? $data['data'] : false;
        }

        /**
         * Get instagram profile from REST
         *
         * @param  Array   $data - data to get profile.
         * @return array
         */
        public function get_profile( $data ) {
            $request = new \WP_REST_Request( 'GET', '/' . Plugin\BLOCK . '/v1/get_profile' );
            $request->set_query_params( $data );

            $response = rest_do_request( $request );

            $server = rest_get_server();

            $data = $server->response_to_data( $response, false );
            $data = isset( $data['response'] ) ? $data['response'] : false;

            return isset( $data['data'] ) ? $data['data'] : false;
        }

        /**
         * Init.
         */
        public function init() {
            if ( ! function_exists( 'register_block_type' ) ) {
                return;
            }

            register_block_type( Plugin\BLOCK . '/instagram', [
                'render_callback' => [$this, 'render_block']
            ] );
        }

        public function registerSettings() {
            register_setting(
                Plugin\CODENAME . 'instagram_access_token',
                Plugin\CODENAME . 'instagram_access_token',
                [
                    'type'              => 'string',
                    'description'       => __( 'Instagram API Access Token for the Gutenberg block plugin.' ),
                    'sanitize_callback' => 'sanitize_text_field',
                    'show_in_rest'      => true,
                    'default'           => ''
                ]
            );
        }

        /**
         * Register gutenberg block output
         *
         * @param  array    $attributes - block attributes.
         * @return string
         */
        public function render_block( $attributes ) {
            ob_start();
            $attributes = $this->block_attributes( $attributes );

            $parentClass = [Plugin\BLOCK . '-instagram', Plugin\BLOCK . '-instagram-variant-' . $attributes['variant']];

            if ( 'swipe' === $attributes['variant'] ) {
                $parentClass[] = Plugin\BLOCK . '-carousel';
            }

            if ( $attributes['className'] ) {
                $parentClass[] = $attributes['className'];
            }
            $data = $this->get_data_attributes( $attributes );

            if ( $attributes['accessToken'] ) {
                $feed = $this->get_feed( [
                    'access_token' => $attributes['accessToken'],
                    'count'        => $attributes['count']
                ] );

                $profile = $this->get_profile( [
                    'access_token' => $attributes['accessToken']
                ] );

                if ( $feed || $profile ) {
                    echo '<div class="' . esc_attr( implode( $parentClass, ' ' ) ) . '"' . $data . '>';

                    if ( $profile && $attributes['showProfile'] ) {
                        $this->render_profile( $profile, $attributes );

                    }
                    if ( $feed ) {
                        $this->render_feed( $feed, $attributes );

                        if ( 'swipe' === $attributes['variant'] ) {
                            if ( $attributes['showArrows'] && $attributes['arrowPrevIcon'] ) {
                                echo '<div class="' . Plugin\BLOCK . '-carousel-arrow-prev-icon"><span class="' . $attributes['arrowPrevIcon'] . '"></span></div>';
                            }
                            if ( $attributes['showArrows'] && $attributes['arrowNextIcon'] ) {
                                echo '<div class="' . Plugin\BLOCK . '-carousel-arrow-next-icon"><span class="' . $attributes['arrowNextIcon'] . '"></span></div>';
                            }
                        }
                    }

                    echo '</div>';

                }
            }

            return ob_get_clean();
        }

        public function render_feed( $feed, $attributes ) {
            $feedClasses = [
                Plugin\BLOCK . '-instagram-items'
            ];

            $itemClasses = [Plugin\BLOCK . '-instagram-child'];

            $extras = [];

            // variant classname.
            if ( 'masonry' === $attributes['variant'] ) {
                $extras[] = $masonry = ' data-masonry=\'{ "itemSelector": ".grid-item" }\'';
                $feedClasses[] = 'grid';
                $itemClasses[] = 'grid-item';

                $feedClasses[] = $attributes['columns'] ? Plugin\BLOCK . '-instagram-columns-' . $attributes['columns'] : '';
                $feedClasses[] = $attributes['smColumns'] ? Plugin\BLOCK . '-instagram-sm-columns-' . $attributes['smColumns'] : '';
                $feedClasses[] = $attributes['mdColumns'] ? Plugin\BLOCK . '-instagram-md-columns-' . $attributes['mdColumns'] : '';
                $feedClasses[] = $attributes['lgColumns'] ? Plugin\BLOCK . '-instagram-lg-columns-' . $attributes['lgColumns'] : '';
                $feedClasses[] = $attributes['xlColumns'] ? Plugin\BLOCK . '-instagram-xl-columns-' . $attributes['xlColumns'] : '';
            } else if ( 'swipe' === $attributes['variant'] ) {
                $feedClasses[] = Plugin\BLOCK . '-carousel-items';
                $itemClasses[] = Plugin\BLOCK . '-carousel-slide';
            }
            if ( $attributes['verticalAlign'] ) {
                $itemClasses[] = 'align-' . $attributes['verticalAlign'];
            }

            if ( $attributes['crop'] ) {
                $feedClasses[] = 'is-cropped';
                $itemClasses[] = 'cropped-child';
            }
            if ( $attributes['equal'] ) {
                $feedClasses[] = 'is-square';
                $itemClasses[] = 'square-child';
            }

            $feedClasses = array_filter( $feedClasses );

            $extras = implode( $extras, ' ' );
            if ( '' !== $extras ) {
                $extras = ' ' . $extras;
            }

            echo '<div class="' . implode( $feedClasses, ' ' ) . '"' . $extras . '>';

            foreach ( $feed as $item ) {
                $caption = substr( esc_attr( $item['caption']['text'] ?: '' ), 0, 100 );

                $counts = '<div class="counts"><div class="likes"><span class="fa fa-heart" aria-hidden="true"></span><span class="count">' . $item['likes']['count'] . '</span></div><div class="comments"><span class="fas fa-comments" aria-hidden="true"></span><span class="count">' . $item['comments']['count'] . '</span></div></div>';
                $faInstagram = '<span class="fab fa-instagram" aria-hidden="true"></span>';

                $overlay = '<div class="overlay dark-overlay overlay-hover-show">' . $counts . $faInstagram . '<div class="caption">' . $caption . '</div></div>';

                $image = '<img src="' . esc_url( $item['images']['standard_resolution']['url'] ) . '" width="' . esc_attr( $item['images']['standard_resolution']['width'] ) . '" height="' . esc_attr( $item['images']['standard_resolution']['height'] ) . '" alt="' . $caption . '" >';

                echo '<div class="' . implode( $itemClasses, ' ' ) . '">';
                echo '<a href="' . esc_url( $item['link'] ) . '" target="_blank">';
                echo '<div class="wrapper">' . $image . $overlay . '</div>';
                echo '</a>';
                echo '</div>';

            }

            echo '</div>';

        }

        // Show profile content
        public function render_profile( $profile, $attributes ) {
            $url = 'https://www.instagram.com/' . esc_attr( $profile['username'] ) . '/';

            echo '<div class="' . Plugin\BLOCK . '-instagram-profile">';
            if ( $attributes['showProfileAvatar'] && isset( $profile['profile_picture'] ) ):
                echo '<div class="' . Plugin\BLOCK . '-instagram-profile-avatar">';
                echo '<a href="' . esc_url( $url ) . '" target="_blank"><img src="' . esc_url( $profile['profile_picture'] ) . '" alt="' . esc_attr( $profile['full_name'] ) . '" width="' . esc_attr( $attributes['profileAvatarSize'] ) . '" height="' . esc_attr( $attributes['profileAvatarSize'] ) . '" /></a>';
                echo '</div>';
            endif;
            echo '<div class="' . Plugin\BLOCK . '-instagram-profile-side">';
            if ( $attributes['showProfileName'] && isset( $profile['full_name'] ) ):
                echo '<div class="' . Plugin\BLOCK . '-instagram-profile-name">';
                echo '</div>';
            endif;
            if ( $attributes['showProfileStats'] && isset( $profile['counts'] ) ):
                echo '<div class="' . Plugin\BLOCK . '-instagram-profile-stats">';
                echo '<div><strong>' . esc_html( $profile['counts']['media'] ) . '</strong> <span>' . esc_html__( 'Posts', '@@text_domain' ) . '</span></div>';
                echo '<div><strong>' . esc_html( $profile['counts']['followed_by'] ) . '</strong> <span>' . esc_html__( 'Followers', '@@text_domain' ) . '</span></div>';
                echo '<div><strong>' . esc_html( $profile['counts']['follows'] ) . '</strong> <span>' . esc_html__( 'Following', '@@text_domain' ) . '</span></div>';
                echo '</div>';
            endif;
            if ( $attributes['showProfileBio'] && isset( $profile['bio'] ) ):
                echo '<div class="' . Plugin\BLOCK . '-instagram-profile-bio">';
                echo '<h2>' . esc_html( $profile['full_name'] ) . '</h2>';
                echo '<div>' . wp_kses_post( $profile['bio'] ) . '</div>';
                echo '</div>';
            endif;
            if ( $attributes['showProfileWebsite'] && isset( $profile['website'] ) ):
                echo '<div class="' . Plugin\BLOCK . '-instagram-profile-website"><a href="' . esc_url( $profile['website'] ) . '" target="_blank">' . esc_url( $profile['website'] ) . '</a></div>';
            endif;
            echo '</div>';
            echo '</div>';
        }
    }
}