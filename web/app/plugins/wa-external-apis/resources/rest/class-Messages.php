<?php
/**
 * Used to return messages as array object from $response or successfull one
 *
 * @since 2.0/5.0
 */
namespace wa\APIs\Rest;

use wa\APIs as APIs;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Messages::class ) ) {
	class Messages {
		public $messages;

		// Setup data for various API request
		public function __construct( $messages = [] ) {
			$this->messages = $messages;
		}

		public function add( $text = '', $type = '', $code = false ) {
			$message = [
				'text' => $text,
				'type' => $type,
				'code' => $code
			];

			if ( ! $code ) {
				$code = 'no_code';
			}

			if ( $type ) {
				$this->messages[$type][$code] = $message;
			} else {
				$this->messages[][$code] = $message;
			}
		}

		public function connection_error_unknown( $text = 'Unknown error', $type = 'warning', $code = 'unknown_error' ) {
			$this->add( $text, $type, $code );
		}

		public function create_blocks( $return_array = false ) {
			$blocks_array = [];
			$blocks_formatted = [];
			$notes = [];
			$codes = [];

			// Loop through message category and individual messages
			foreach ( $this->messages as $category_key => $category ) {
				$category_messages = '';
				//var_dump( $category );
				foreach ( $category as $message_key => $message ) {
					$note = $message['text'];
					$code = $message['code'];
					$class  = [];

					if ( $code && ! is_int( $code ) && 'error' === $category_key || 'warning' === $category_key ) {
						// Add error code for error/warning messages
						$note .= ', key=' . $code;
						$class[] = $code;
						$codes[$message_key] = $code;
					}

					$category_messages .= '<div class="' . implode( ' ', $class ) . '">' . $note . '.</div>';
				}

				$blocks_array['notes'][$category_key] = $category_messages;
				//var_dump( $blocks_array['notes'][$category_key] );

				$class  = ['alert'];
				if ( $category_key && ! is_int( $category_key ) ) {
					$class[] = 'alert-' . $category_key;
				}

				$notes[$message_key] = '<div class="' . implode( ' ', $class ) . '">' . $category_messages . '</div>';

				//$blocks_formatted[$category_key] = implode( '. ', $blocks_array[$category_key] );
			}
			//var_dump( $blocks_array );

			if ( $return_array ) {
				return $blocks_array;
			} else {
				// Loop through formatted message text and create divs
				return ['notes' => implode( $notes ), 'codes' => implode( ' ', $codes )];
			}
		}

		public function get_important_messages() {
			if ( array_key_exists( 'danger', $this->messages ) ) {
				return $this->messages['danger'];
			} else if ( array_key_exists( 'warning', $this->messages ) ) {
				return $this->messages['warning'];
			} else if ( array_key_exists( 'success', $this->messages ) ) {
				return $this->messages['success'];
			} else {
				return $this->messages;
			}
		}

		public function get_messages_code() {
			$important_messages = $this->get_important_messages();

			return trim( implode( ' ', array_keys( $important_messages ) ) );
		}

		public function get_messages_text() {
			$important_messages = $this->get_important_messages();
			//var_dump( $important_messages );
			$text = [];
			foreach ( $important_messages as $message ) {
				//var_dump( $message );
				$text[] = $message['text'];
			}
			//var_dump( $text );

			return trim( implode( ' ', $text ) );
		}

		// Merge multiple messages objects
		public function merge_objects() {
			$objects = func_get_args();
			$messages_merged = [];

			// the $objects array will contain all the arguments passed to the function
			foreach ( $objects as $object ) {
				$messages = $object->return_array();

				if ( empty( $messages_merged ) ) {
					$messages_merged = $messages;
				} else {
					$messages_merged = array_replace_recursive( $messages_merged, $messages );
				}
			}

			$this->messages = $messages_merged;
		}

		// Error rest.
		public static function response_error( $message, $code = '', $returned = [] ) {
			// Will cause script error and stop script immediately
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'error',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				401
			);
		}

		public static function response_return( $messages, $settings ) {
			$messages_array = $messages->return_array();
			$messages_blocks = $messages->create_blocks();

			//$settings['messages']['notes'] = $messages_blocks['notes'];
			//$settings['messages']['codes'] = $messages_blocks['codes'];
			//$settings['messages']['all'] = $messages_array;

			if ( array_key_exists( 'error', $messages_array ) ) {
				//$settings['messages']['type'] = 'error';

				return self::response_error( $messages_blocks['notes'], $messages_blocks['codes'], $settings );
			} else if ( array_key_exists( 'warning', $messages_array ) ) {
				//$settings['messages']['type'] = 'warning';

				return self::response_warning( $messages_blocks['notes'], $messages_blocks['codes'], $settings );
			} else {
				//$settings['messages']['type'] = 'success';

				return self::response_success( $messages_blocks['notes'], $messages_blocks['codes'], $settings );
			}
		}

		// Success rest.
		public static function response_success( $message, $code = '', $returned = [] ) {
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'success',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				200
			);
		}

		// Warning rest.
		public static function response_warning( $message, $code = '', $returned = [] ) {
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'warning',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				200
			);
		}

		// Returns all messages in array format
		public function return_array() {
			return $this->messages;
		}

		public function settings_change( $reset, $update_successful ) {
			if ( $reset ) {
				$this->settings_reset();
			} else if ( $update_successful ) {
				$this->settings_update_success();
			} else if ( ! $update_successful ) {
				$this->settings_update_unneeded();
			} else {
				$this->settings_update_failed();
			}
		}

		public function settings_reset( $text = 'Settings were reset', $type = 'success', $code = 'settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_failed( $text = 'Failed to update settings', $type = 'error', $code = 'no_settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_success( $text = 'Settings were updated', $type = 'success', $code = 'settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_unneeded( $text = 'No new data provided, settings do not need to be updated', $type = 'warning', $code = 'no_new_settings_data' ) {

			$this->add( $text, $type, $code );
		}
	}
}