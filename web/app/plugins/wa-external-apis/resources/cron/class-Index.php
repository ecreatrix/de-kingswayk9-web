<?php
/*
 * Time
 *
 * MINUTE_IN_SECONDS  = 60 (seconds)
 * HOUR_IN_SECONDS    = 60 * MINUTE_IN_SECONDS
 * DAY_IN_SECONDS     = 24 * HOUR_IN_SECONDS
 * WEEK_IN_SECONDS    = 7 * DAY_IN_SECONDS
 * MONTH_IN_SECONDS   = 30 * DAY_IN_SECONDS
 * YEAR_IN_SECONDS    = 365 * DAY_IN_SECONDS
 */
namespace wa\APIs\Cron;

use wa\APIs as Plugin;

// Exit if accessed directly.

if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {}

        public static function activate() {
            if ( class_exists( Plugin\Instagram\Cron::class ) ) {
                new Plugin\Instagram\Cron();
            }
        }

        public static function deactivate() {
            if ( class_exists( Plugin\Instagram\Cron::class ) ) {
                Plugin\Instagram\Cron::deactivate();
            }
        }
    }
}
