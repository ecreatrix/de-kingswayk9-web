<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_sidebar_position = onleash_get_theme_option('sidebar_position');
if (onleash_sidebar_present()) {
	ob_start();
	$onleash_sidebar_name = onleash_get_theme_option('sidebar_widgets');
	onleash_storage_set('current_sidebar', 'sidebar');
	if ( is_active_sidebar($onleash_sidebar_name) ) {
		dynamic_sidebar($onleash_sidebar_name);
	}
	$onleash_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($onleash_out)) {
		?>
		<div class="sidebar <?php echo esc_attr($onleash_sidebar_position); ?> widget_area<?php if (!onleash_is_inherit(onleash_get_theme_option('sidebar_scheme'))) echo ' scheme_'.esc_attr(onleash_get_theme_option('sidebar_scheme')); ?>" role="complementary">
			<div class="sidebar_inner">
				<?php
				do_action( 'onleash_action_before_sidebar' );
				onleash_show_layout(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $onleash_out));
				do_action( 'onleash_action_after_sidebar' );
				?>
			</div><!-- /.sidebar_inner -->
		</div><!-- /.sidebar -->
		<?php
	}
}
?>