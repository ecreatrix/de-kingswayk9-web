<?php
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Rest::class ) ) {
    class Rest {
        public function __construct() {}

        public static function ajax_parameters() {
            $settings = Plugin\Settings\Options::get_settings( Index::get_key() );
            $instagram = [
                'access'  => false,
                'profile' => false,
                'media'   => false,
                'display' => false
            ];

            if ( ! $settings || empty( $settings ) ) {
                // Set default values if settings empty
                $access = new Connect\Access();
                $access->connect( true );
                $instagram['access'] = $access->get_info();
            } else if ( array_key_exists( 'access', $settings ) ) {
                $instagram = Connect\Index::info_all( $settings );
            } else if ( ! array_key_exists( 'access', $settings ) ) {
                return;
            }

            return [
                'instagram' => $instagram
            ];
        }

        public static function get_request( \WP_REST_Request $request ) {
            $settings = Plugin\Settings\Options::get_settings( Index::get_key() );

            return Plugin\Rest\Messages::response_success( json_encode( $settings ) );
        }

        // Set Instagram settings value from Settings page and API access
        public static function set_request( \WP_REST_Request $request ) {
            $accessValues = $request->get_param( 'access' );
            $displayValues = $request->get_param( 'display' );
            $reset = $request->get_param( 'reset' );

            if ( $reset ) {
                //$accessValues = [];
            }

            $connect = Connect\Index::connect_all( $reset, $accessValues, $displayValues );

            $messages = $connect['messages'];
            $settings = $connect['connection'];

            $update_successful = Plugin\Settings\Options::update_settings( $settings, Index::get_key() );

            if ( array_key_exists( 'access', $settings ) ) {
                $return = Connect\Index::info_all( $settings );
                $messages->settings_change( false, $update_successful );

                return $messages->response_return( $messages, $return );
            }
        }
    }
}
