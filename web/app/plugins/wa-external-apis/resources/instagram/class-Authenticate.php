<?php
/**
 * Connect to the Instagram API and return the results. It's possible
 * to build the url from a connected account (includes access token,
 * account id, account type), endpoint and parameters (hashtag, etc..)
 * as well as a full url such as from the pagination data from some Instagram API requests.
 *
 * Errors from either the Instagram API or from the HTTP request are detected
 * and can be handled.
 *
 * @since 2.0
 */
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Authenticate::class ) ) {
	class Authenticate {
		const API_URL = 'https://graph.instagram.com/';

		const LONG_ACCESS_URL = 'https://graph.instagram.com/access_token';

		const OAUTH_URL = 'https://api.instagram.com/oauth/authorize';

		const SHORT_ACCESS_URL = 'https://api.instagram.com/oauth/access_token';

		const TOKEN_REFRESH_URL = 'https://graph.instagram.com/refresh_access_token';

		public static $connectTimeout = 20000;

		// Look for new information every 50 days (Instagram tokens last 60days)
		public static $long_term_expiry = DAY_IN_SECONDS * 50;

		public $messages;

		// Setup data for various API request
		public function __construct() {
			$this->messages = new Plugin\Instagram\Messages();
		}

		public static function create_auth_url( $app_id, $secret, $redirect_url ) {
			$page = Plugin\Settings\Pages::get_admin_page();

			return self::OAUTH_URL . '?app_id=' . $app_id . '&redirect_uri=' . $redirect_url . '&response_type=code&scope=user_profile,user_media&state=' . $page;
		}

		public static function makeCall( $access, $function, $params = null, $method = 'GET' ) {
			$long_term_access_token = $access->get_long_term_access_token();
			$connection_stage = $access->get_connection_stage();

			$paramString = null;

			if ( isset( $params ) && is_array( $params ) ) {
				$paramString = '&' . http_build_query( $params );
			}

			$authMethod = '?access_token=' . $long_term_access_token;

			$apiCall = self::API_URL . $function . $authMethod . (  ( 'GET' === $method ) ? $paramString : null );

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $apiCall );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Accept: application/json'] );
			curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT_MS, self::$connectTimeout );
			curl_setopt( $ch, CURLOPT_TIMEOUT_MS, self::$long_term_expiry );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch, CURLOPT_HEADER, true );

			$jsonData = curl_exec( $ch );

			if ( ! $jsonData ) {
				$this->messages->curl_error_make_call( $ch );
			}

			list( $headerContent, $jsonData ) = explode( "\r\n\r\n", $jsonData, 2 );

			curl_close( $ch );

			return json_decode( $jsonData );
		}

		// Make cUrl request and return results
		public static function makeOAuthCall( $params, $expiration, $url_type = false, $method = 'POST' ) {
			$paramString = null;

			if ( isset( $params ) && is_array( $params ) ) {
				$paramString = '?' . http_build_query( $params );
			}
			$url = self::LONG_ACCESS_URL;
			if ( 'refresh' === $url_type ) {
				$url = self::TOKEN_REFRESH_URL;
			} else if ( 'short' === $url_type ) {
				$url = self::SHORT_ACCESS_URL;
			}

			$apiCall = $url . (  ( 'GET' === $method ) ? $paramString : null );

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $apiCall );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Accept: application/json'] );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch, CURLOPT_TIMEOUT_MS, $expiration );

			if ( 'POST' === $method ) {
				curl_setopt( $ch, CURLOPT_POST, count( $params ) );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $params ) );
			}

			$jsonData = curl_exec( $ch );

			if ( ! $jsonData ) {
				$this->messages->curl_error_make_oauth( $ch );

				return;
			}

			curl_close( $ch );

			return json_decode( $jsonData );
		}
	}
}