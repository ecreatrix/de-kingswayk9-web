<?php
/**
 * The Footer: widgets area, logo, footer menu and socials
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

						// Widgets area inside page content
						onleash_create_widgets_area('widgets_below_content');
						?>				
					</div><!-- </.content> -->

					<?php
					// Show main sidebar
					get_sidebar();

					// Widgets area below page content
					onleash_create_widgets_area('widgets_below_page');

					$onleash_body_style = onleash_get_theme_option('body_style');
					if ($onleash_body_style != 'fullscreen') {
						?></div><!-- </.content_wrap> --><?php
					}
					?>
			</div><!-- </.page_content_wrap> -->

			<?php
			// Footer
			$onleash_footer_style = onleash_get_theme_option("footer_style");
			if (strpos($onleash_footer_style, 'footer-custom-')===0) $onleash_footer_style = 'footer-custom';
			get_template_part( "templates/{$onleash_footer_style}");
			?>

		</div><!-- /.page_wrap -->

	</div><!-- /.body_wrap -->

	<?php if (onleash_is_on(onleash_get_theme_option('debug_mode')) && onleash_get_file_dir('images/makeup.jpg')!='') { ?>
		<img src="<?php echo esc_url(onleash_get_file_url('images/makeup.jpg')); ?>" id="makeup">
	<?php } ?>

	<?php wp_footer(); ?>

</body>
</html>