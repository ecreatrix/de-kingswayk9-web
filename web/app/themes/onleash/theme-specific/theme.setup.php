<?php
/**
 * Setup theme-specific fonts and colors
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.22
 */

// Theme init priorities:
// 1 - register filters to add/remove lists items in the Theme Options
// 2 - create Theme Options
// 3 - add/remove Theme Options elements
// 5 - load Theme Options
// 9 - register other filters (for installer, etc.)
//10 - standard Theme init procedures (not ordered)
if ( !function_exists('onleash_customizer_theme_setup1') ) {
	add_action( 'after_setup_theme', 'onleash_customizer_theme_setup1', 1 );
	function onleash_customizer_theme_setup1() {
		
		// -----------------------------------------------------------------
		// -- Theme fonts (Google and/or custom fonts)
		// -----------------------------------------------------------------
		
		// Fonts to load when theme start
		// It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
		// Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
		// For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'
		onleash_storage_set('load_fonts', array(
			// Google font
			array(
				'name'   => 'Montserrat',
				'family' => 'sans-serif',
                'styles' => '300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic'
				),
            array(
                'name'   => 'Pacifico',
                'family' => 'cursive'
            )
		));
		
		// Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
		onleash_storage_set('load_fonts_subset', 'latin,latin-ext');
		
		// Settings of the main tags
		onleash_storage_set('theme_fonts', array(
			'p' => array(
				'title'				=> esc_html__('Main text', 'onleash'),
				'description'		=> esc_html__('Font settings of the main text of the site', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1rem',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.55',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.15px',
				'margin-top'		=> '0em',
				'margin-bottom'		=> '1.5em'
				),
			'h1' => array(
				'title'				=> esc_html__('Heading 1', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '4.2em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.15',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-1.6px',
				'margin-top'		=> '1.715em',
				'margin-bottom'		=> '0.575em'
				),
			'h2' => array(
				'title'				=> esc_html__('Heading 2', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '3.2em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.15',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-1.3px',
				'margin-top'		=> '2.26em',
				'margin-bottom'		=> '0.685em'
				),
			'h3' => array(
				'title'				=> esc_html__('Heading 3', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '2.4em',
				'font-weight'		=> '600',
				'font-style'		=> 'normal',
				'line-height'		=> '1.25',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.8px',
				'margin-top'		=> '3em',
				'margin-bottom'		=> '0.925em'
				),
			'h4' => array(
				'title'				=> esc_html__('Heading 4', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '2em',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.7px',
				'margin-top'		=> '3.6em',
				'margin-bottom'		=> '0.825em'
				),
			'h5' => array(
				'title'				=> esc_html__('Heading 5', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1.6em',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.25',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.5px',
				'margin-top'		=> '4.7em',
				'margin-bottom'		=> '0.65em'
				),
			'h6' => array(
				'title'				=> esc_html__('Heading 6', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1.333em',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '-0.5px',
				'margin-top'		=> '5.55em',
				'margin-bottom'		=> '0.95em'
				),
			'logo' => array(
				'title'				=> esc_html__('Logo text', 'onleash'),
				'description'		=> esc_html__('Font settings of the text case of the logo', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1.867em',
				'font-weight'		=> '600',
				'font-style'		=> 'normal',
				'line-height'		=> '1',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0'
				),
			'button' => array(
				'title'				=> esc_html__('Buttons', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '0.933em',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> 'normal',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0px'
				),
			'input' => array(
				'title'				=> esc_html__('Input fields', 'onleash'),
				'description'		=> esc_html__('Font settings of the input fields, dropdowns and textareas', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '0.933em',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> 'normal',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0px'
				),
			'info' => array(
				'title'				=> esc_html__('Post meta', 'onleash'),
				'description'		=> esc_html__('Font settings of the post meta: date, counters, share, etc.', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '13px',
				'font-weight'		=> '500',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> '0px',
				'margin-top'		=> '0.4em',
				'margin-bottom'		=> ''
				),
			'menu' => array(
				'title'				=> esc_html__('Main menu', 'onleash'),
				'description'		=> esc_html__('Font settings of the main menu items', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '600',
				'font-style'		=> 'normal',
				'line-height'		=> 'normal',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0px'
				),
			'submenu' => array(
				'title'				=> esc_html__('Dropdown menu', 'onleash'),
				'description'		=> esc_html__('Font settings of the dropdown menu items', 'onleash'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '14px',
				'font-weight'		=> '600',
				'font-style'		=> 'normal',
				'line-height'		=> 'normal',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0px'
				)
		));
		
		
		// -----------------------------------------------------------------
		// -- Theme colors for customizer
		// -- Attention! Inner scheme must be last in the array below
		// -----------------------------------------------------------------
		onleash_storage_set('scheme_color_groups', array(
			'main'	=> array(
							'title'			=> __('Main', 'onleash'),
							'description'	=> __('Colors of the main content area', 'onleash')
							),
			'alter'	=> array(
							'title'			=> __('Alter', 'onleash'),
							'description'	=> __('Colors of the alternative blocks (sidebars, etc.)', 'onleash')
							),
			'extra'	=> array(
							'title'			=> __('Extra', 'onleash'),
							'description'	=> __('Colors of the extra blocks (dropdowns, price blocks, table headers, etc.)', 'onleash')
							),
			'inverse' => array(
							'title'			=> __('Inverse', 'onleash'),
							'description'	=> __('Colors of the inverse blocks - when link color used as background of the block (dropdowns, blockquotes, etc.)', 'onleash')
							),
			'input'	=> array(
							'title'			=> __('Input', 'onleash'),
							'description'	=> __('Colors of the form fields (text field, textarea, select, etc.)', 'onleash')
							),
			)
		);
		onleash_storage_set('scheme_color_names', array(
			'bg_color'	=> array(
							'title'			=> __('Background color', 'onleash'),
							'description'	=> __('Background color of this block in the normal state', 'onleash')
							),
			'bg_hover'	=> array(
							'title'			=> __('Background hover', 'onleash'),
							'description'	=> __('Background color of this block in the hovered state', 'onleash')
							),
			'bd_color'	=> array(
							'title'			=> __('Border color', 'onleash'),
							'description'	=> __('Border color of this block in the normal state', 'onleash')
							),
			'bd_hover'	=>  array(
							'title'			=> __('Border hover', 'onleash'),
							'description'	=> __('Border color of this block in the hovered state', 'onleash')
							),
			'text'		=> array(
							'title'			=> __('Text', 'onleash'),
							'description'	=> __('Color of the plain text inside this block', 'onleash')
							),
			'text_dark'	=> array(
							'title'			=> __('Text dark', 'onleash'),
							'description'	=> __('Color of the dark text (bold, header, etc.) inside this block', 'onleash')
							),
			'text_light'=> array(
							'title'			=> __('Text light', 'onleash'),
							'description'	=> __('Color of the light text (post meta, etc.) inside this block', 'onleash')
							),
			'text_link'	=> array(
							'title'			=> __('Link', 'onleash'),
							'description'	=> __('Color of the links inside this block', 'onleash')
							),
			'text_hover'=> array(
							'title'			=> __('Link hover', 'onleash'),
							'description'	=> __('Color of the hovered state of links inside this block', 'onleash')
							),
			'text_link2'=> array(
							'title'			=> __('Link 2', 'onleash'),
							'description'	=> __('Color of the accented texts (areas) inside this block', 'onleash')
							),
			'text_hover2'=> array(
							'title'			=> __('Link 2 hover', 'onleash'),
							'description'	=> __('Color of the hovered state of accented texts (areas) inside this block', 'onleash')
							),
			'text_link3'=> array(
							'title'			=> __('Link 3', 'onleash'),
							'description'	=> __('Color of the other accented texts (buttons) inside this block', 'onleash')
							),
			'text_hover3'=> array(
							'title'			=> __('Link 3 hover', 'onleash'),
							'description'	=> __('Color of the hovered state of other accented texts (buttons) inside this block', 'onleash')
							)
			)
		);
		onleash_storage_set('schemes', array(
		
			// Color scheme: 'default'
			'default' => array(
				'title'	 => esc_html__('Default', 'onleash'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'			=> '#ffffff',
					'bd_color'			=> '#e5e5e5',
		
					// Text and links colors
					'text'				=> '#7e7e7e', //+
					'text_light'		=> '#9c9c9c', //+
					'text_dark'			=> '#494848', //+
					'text_link'			=> '#a7da77', //+
					'text_hover'		=> '#85b358', //+
					'text_link2'		=> '#fbcd69', //+
					'text_hover2'		=> '#8be77c',
					'text_link3'		=> '#ddb837',
					'text_hover3'		=> '#eec432',
		
					// Alternative blocks (sidebar, tabs, alternative blocks, etc.)
					'alter_bg_color'	=> '#f5f4f0', //+
					'alter_bg_hover'	=> '#e6e8eb',
					'alter_bd_color'	=> '#ece9e4', //+
					'alter_bd_hover'	=> '#d7d7d6', //+
					'alter_text'		=> '#505050', //+
					'alter_light'		=> '#b7b7b7', //+
					'alter_dark'		=> '#595959', //+
					'alter_link'		=> '#fe7259',
					'alter_hover'		=> '#72cfd5',
					'alter_link2'		=> '#80d572',
					'alter_hover2'		=> '#8be77c',
					'alter_link3'		=> '#ddb837',
					'alter_hover3'		=> '#eec432',
		
					// Extra blocks (submenu, tabs, color blocks, etc.)
					'extra_bg_color'	=> '#303030', //+
					'extra_bg_hover'	=> '#28272e',
					'extra_bd_color'	=> '#313131',
					'extra_bd_hover'	=> '#3d3d3d',
					'extra_text'		=> '#bfbfbf',
					'extra_light'		=> '#afafaf',
					'extra_dark'		=> '#ffffff',
					'extra_link'		=> '#72cfd5',
					'extra_hover'		=> '#fe7259',
					'extra_link2'		=> '#80d572',
					'extra_hover2'		=> '#8be77c',
					'extra_link3'		=> '#ddb837',
					'extra_hover3'		=> '#eec432',
		
					// Input fields (form's fields and textarea)
					'input_bg_color'	=> '#f5f4f0', //+
					'input_bg_hover'	=> '#f5f4f0', //+
					'input_bd_color'	=> '#f5f4f0', //+
					'input_bd_hover'	=> '#a7da77', //+
					'input_text'		=> '#b7b7b7', //+
					'input_light'		=> '#ababab', //+
					'input_dark'		=> '#1d1d1d', //+
					
					// Inverse blocks (text and links on the 'text_link' background)
					'inverse_bd_color'	=> '#67bcc1',
					'inverse_bd_hover'	=> '#5aa4a9',
					'inverse_text'		=> '#1d1d1d',
					'inverse_light'		=> '#333333',
					'inverse_dark'		=> '#000000',
					'inverse_link'		=> '#ffffff',
					'inverse_hover'		=> '#1d1d1d'
				)
			),
		
			// Color scheme: 'dark'
			'dark' => array(
				'title'  => esc_html__('Dark', 'onleash'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'			=> '#0e0d12',
					'bd_color'			=> '#1c1b1f',
		
					// Text and links colors
					'text'				=> '#b7b7b7', //+
					'text_light'		=> '#303030', //+
					'text_dark'			=> '#ffffff', //+
					'text_link'			=> '#a7da77', //+
					'text_hover'		=> '#ffaa5f',
					'text_link2'		=> '#80d572',
					'text_hover2'		=> '#8be77c',
					'text_link3'		=> '#ddb837',
					'text_hover3'		=> '#eec432',

					// Alternative blocks (sidebar, tabs, alternative blocks, etc.)
					'alter_bg_color'	=> '#1e1d22',
					'alter_bg_hover'	=> '#28272e',
					'alter_bd_color'	=> '#313131',
					'alter_bd_hover'	=> '#3d3d3d',
					'alter_text'		=> '#a6a6a6',
					'alter_light'		=> '#5f5f5f',
					'alter_dark'		=> '#ffffff',
					'alter_link'		=> '#ffaa5f',
					'alter_hover'		=> '#fe7259',
					'alter_link2'		=> '#80d572',
					'alter_hover2'		=> '#8be77c',
					'alter_link3'		=> '#ddb837',
					'alter_hover3'		=> '#eec432',

					// Extra blocks (submenu, tabs, color blocks, etc.)
					'extra_bg_color'	=> '#1e1d22',
					'extra_bg_hover'	=> '#28272e',
					'extra_bd_color'	=> '#313131',
					'extra_bd_hover'	=> '#3d3d3d',
					'extra_text'		=> '#a6a6a6',
					'extra_light'		=> '#5f5f5f',
					'extra_dark'		=> '#ffffff',
					'extra_link'		=> '#ffaa5f',
					'extra_hover'		=> '#fe7259',
					'extra_link2'		=> '#80d572',
					'extra_hover2'		=> '#8be77c',
					'extra_link3'		=> '#ddb837',
					'extra_hover3'		=> '#eec432',

					// Input fields (form's fields and textarea)
					'input_bg_color'	=> '#2e2d32',
					'input_bg_hover'	=> '#2e2d32',
					'input_bd_color'	=> '#2e2d32',
					'input_bd_hover'	=> '#353535',
					'input_text'		=> '#b7b7b7',
					'input_light'		=> '#5f5f5f',
					'input_dark'		=> '#ffffff',
					
					// Inverse blocks (text and links on the 'text_link' background)
					'inverse_bd_color'	=> '#e36650',
					'inverse_bd_hover'	=> '#cb5b47',
					'inverse_text'		=> '#1d1d1d',
					'inverse_light'		=> '#5f5f5f',
					'inverse_dark'		=> '#000000',
					'inverse_link'		=> '#ffffff',
					'inverse_hover'		=> '#1d1d1d'
				)
			)
		
		));
	}
}

			
// Additional (calculated) theme-specific colors
// Attention! Don't forget setup custom colors also in the theme.customizer.color-scheme.js
if (!function_exists('onleash_customizer_add_theme_colors')) {
	function onleash_customizer_add_theme_colors($colors) {
		if (substr($colors['text'], 0, 1) == '#') {
			$colors['text_071']  = onleash_hex2rgba( $colors['text'], 0.71 );
			$colors['bg_color_0']  = onleash_hex2rgba( $colors['bg_color'], 0 );
			$colors['bg_color_007']  = onleash_hex2rgba( $colors['bg_color'], 0.07 );
			$colors['bg_color_02']  = onleash_hex2rgba( $colors['bg_color'], 0.2 );
			$colors['bg_color_03']  = onleash_hex2rgba( $colors['bg_color'], 0.3 );
			$colors['bg_color_04']  = onleash_hex2rgba( $colors['bg_color'], 0.4 );
			$colors['bg_color_07']  = onleash_hex2rgba( $colors['bg_color'], 0.7 );
			$colors['bg_color_08']  = onleash_hex2rgba( $colors['bg_color'], 0.8 );
			$colors['bg_color_09']  = onleash_hex2rgba( $colors['bg_color'], 0.9 );
			$colors['alter_bg_color_07']  = onleash_hex2rgba( $colors['alter_bg_color'], 0.7 );
			$colors['alter_bg_color_04']  = onleash_hex2rgba( $colors['alter_bg_color'], 0.4 );
			$colors['alter_bg_color_02']  = onleash_hex2rgba( $colors['alter_bg_color'], 0.2 );
			$colors['alter_bd_color_02']  = onleash_hex2rgba( $colors['alter_bd_color'], 0.2 );
			$colors['extra_bg_color_07']  = onleash_hex2rgba( $colors['extra_bg_color'], 0.7 );
			$colors['text_dark_006']  = onleash_hex2rgba( $colors['text_dark'], 0.06 );
			$colors['text_dark_023']  = onleash_hex2rgba( $colors['text_dark'], 0.23 );
			$colors['text_dark_05']  = onleash_hex2rgba( $colors['text_dark'], 0.5 );
			$colors['text_dark_071']  = onleash_hex2rgba( $colors['text_dark'], 0.71 );
			$colors['text_dark_07']  = onleash_hex2rgba( $colors['text_dark'], 0.7 );
			$colors['text_link_02']  = onleash_hex2rgba( $colors['text_link'], 0.2 );
			$colors['text_link_07']  = onleash_hex2rgba( $colors['text_link'], 0.7 );
			$colors['text_link_blend'] = onleash_hsb2hex(onleash_hex2hsb( $colors['text_link'], 2, -5, 5 ));
			$colors['alter_link_blend'] = onleash_hsb2hex(onleash_hex2hsb( $colors['alter_link'], 2, -5, 5 ));
		} else {
			$colors['text_071'] = '{{ data.text_071 }}';
			$colors['bg_color_0'] = '{{ data.bg_color_0 }}';
			$colors['bg_color_007'] = '{{ data.bg_color_007 }}';
			$colors['bg_color_02'] = '{{ data.bg_color_02 }}';
			$colors['bg_color_03'] = '{{ data.bg_color_03 }}';
			$colors['bg_color_04'] = '{{ data.bg_color_04 }}';
			$colors['bg_color_07'] = '{{ data.bg_color_07 }}';
			$colors['bg_color_08'] = '{{ data.bg_color_08 }}';
			$colors['bg_color_09'] = '{{ data.bg_color_09 }}';
			$colors['alter_bg_color_07'] = '{{ data.alter_bg_color_07 }}';
			$colors['alter_bg_color_04'] = '{{ data.alter_bg_color_04 }}';
			$colors['alter_bg_color_02'] = '{{ data.alter_bg_color_02 }}';
			$colors['alter_bd_color_02'] = '{{ data.alter_bd_color_02 }}';
			$colors['extra_bg_color_07'] = '{{ data.extra_bg_color_07 }}';
			$colors['text_dark_006'] = '{{ data.text_dark_006 }}';
			$colors['text_dark_023'] = '{{ data.text_dark_023 }}';
			$colors['text_dark_05'] = '{{ data.text_dark_05 }}';
			$colors['text_dark_07'] = '{{ data.text_dark_07 }}';
			$colors['text_link_02'] = '{{ data.text_link_02 }}';
			$colors['text_link_07'] = '{{ data.text_link_07 }}';
			$colors['text_link_blend'] = '{{ data.text_link_blend }}';
			$colors['alter_link_blend'] = '{{ data.alter_link_blend }}';
		}
		return $colors;
	}
}


			
// Additional theme-specific fonts rules
// Attention! Don't forget setup fonts rules also in the theme.customizer.color-scheme.js
if (!function_exists('onleash_customizer_add_theme_fonts')) {
	function onleash_customizer_add_theme_fonts($fonts) {
		$rez = array();	
		foreach ($fonts as $tag => $font) {
			//$rez[$tag] = $font;
			if (substr($font['font-family'], 0, 2) != '{{') {
				$rez[$tag.'_font-family'] 		= !empty($font['font-family']) && !onleash_is_inherit($font['font-family'])
														? 'font-family:' . trim($font['font-family']) . ';' 
														: '';
				$rez[$tag.'_font-size'] 		= !empty($font['font-size']) && !onleash_is_inherit($font['font-size'])
														? 'font-size:' . onleash_prepare_css_value($font['font-size']) . ";"
														: '';
				$rez[$tag.'_line-height'] 		= !empty($font['line-height']) && !onleash_is_inherit($font['line-height'])
														? 'line-height:' . trim($font['line-height']) . ";"
														: '';
				$rez[$tag.'_font-weight'] 		= !empty($font['font-weight']) && !onleash_is_inherit($font['font-weight'])
														? 'font-weight:' . trim($font['font-weight']) . ";"
														: '';
				$rez[$tag.'_font-style'] 		= !empty($font['font-style']) && !onleash_is_inherit($font['font-style'])
														? 'font-style:' . trim($font['font-style']) . ";"
														: '';
				$rez[$tag.'_text-decoration'] 	= !empty($font['text-decoration']) && !onleash_is_inherit($font['text-decoration'])
														? 'text-decoration:' . trim($font['text-decoration']) . ";"
														: '';
				$rez[$tag.'_text-transform'] 	= !empty($font['text-transform']) && !onleash_is_inherit($font['text-transform'])
														? 'text-transform:' . trim($font['text-transform']) . ";"
														: '';
				$rez[$tag.'_letter-spacing'] 	= !empty($font['letter-spacing']) && !onleash_is_inherit($font['letter-spacing'])
														? 'letter-spacing:' . trim($font['letter-spacing']) . ";"
														: '';
				$rez[$tag.'_margin-top'] 		= !empty($font['margin-top']) && !onleash_is_inherit($font['margin-top'])
														? 'margin-top:' . onleash_prepare_css_value($font['margin-top']) . ";"
														: '';
				$rez[$tag.'_margin-bottom'] 	= !empty($font['margin-bottom']) && !onleash_is_inherit($font['margin-bottom'])
														? 'margin-bottom:' . onleash_prepare_css_value($font['margin-bottom']) . ";"
														: '';
			} else {
				$rez[$tag.'_font-family']		= '{{ data["'.$tag.'_font-family"] }}';
				$rez[$tag.'_font-size']			= '{{ data["'.$tag.'_font-size"] }}';
				$rez[$tag.'_line-height']		= '{{ data["'.$tag.'_line-height"] }}';
				$rez[$tag.'_font-weight']		= '{{ data["'.$tag.'_font-weight"] }}';
				$rez[$tag.'_font-style']		= '{{ data["'.$tag.'_font-style"] }}';
				$rez[$tag.'_text-decoration']	= '{{ data["'.$tag.'_text-decoration"] }}';
				$rez[$tag.'_text-transform']	= '{{ data["'.$tag.'_text-transform"] }}';
				$rez[$tag.'_letter-spacing']	= '{{ data["'.$tag.'_letter-spacing"] }}';
				$rez[$tag.'_margin-top']		= '{{ data["'.$tag.'_margin-top"] }}';
				$rez[$tag.'_margin-bottom']		= '{{ data["'.$tag.'_margin-bottom"] }}';
			}
		}
		return $rez;
	}
}


//-------------------------------------------------------
//-- Thumb sizes
//-------------------------------------------------------

if ( !function_exists('onleash_customizer_theme_setup') ) {
	add_action( 'after_setup_theme', 'onleash_customizer_theme_setup' );
	function onleash_customizer_theme_setup() {

		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(370, 0, false);
		
		// Add thumb sizes
		// ATTENTION! If you change list below - check filter's names in the 'trx_addons_filter_get_thumb_size' hook
		$thumb_sizes = apply_filters('onleash_filter_add_thumb_sizes', array(
			'onleash-thumb-huge'		=> array(1170, 658, true),
			'onleash-thumb-big' 		=> array( 760, 410, true),  //+
			'onleash-thumb-med' 		=> array( 740, 364, true),  //+
			'onleash-thumb-tiny' 		=> array( 300, 300, true),  //+
			'onleash-thumb-blogger'		=> array( 740, 464, true),  //+
			'onleash-thumb-team' 		=> array( 730, 820, true),  //+
			'onleash-thumb-team-short'	=> array( 524, 732, true),  //+
			'onleash-thumb-team-default-with-socials'	=> array( 740, 820, true),  //+
			'onleash-thumb-masonry-big' => array( 760,   0, false),		// Only downscale, not crop
			'onleash-thumb-masonry'		=> array( 370,   0, false),		// Only downscale, not crop
			)
		);
		$mult = onleash_get_theme_option('retina_ready', 1);
		if ($mult > 1) $GLOBALS['content_width'] = apply_filters( 'onleash_filter_content_width', 1170*$mult);
		foreach ($thumb_sizes as $k=>$v) {
			// Add Original dimensions
			add_image_size( $k, $v[0], $v[1], $v[2]);
			// Add Retina dimensions
			if ($mult > 1) add_image_size( $k.'-@retina', $v[0]*$mult, $v[1]*$mult, $v[2]);
		}

	}
}

if ( !function_exists('onleash_customizer_image_sizes') ) {
	add_filter( 'image_size_names_choose', 'onleash_customizer_image_sizes' );
	function onleash_customizer_image_sizes( $sizes ) {
		$thumb_sizes = apply_filters('onleash_filter_add_thumb_sizes', array(
			'onleash-thumb-huge'		=> esc_html__( 'Fullsize image', 'onleash' ),
			'onleash-thumb-big'			=> esc_html__( 'Large image', 'onleash' ),
			'onleash-thumb-med'			=> esc_html__( 'Medium image', 'onleash' ),
			'onleash-thumb-tiny'		=> esc_html__( 'Small square avatar', 'onleash' ),
			'onleash-thumb-masonry-big'	=> esc_html__( 'Masonry Large (scaled)', 'onleash' ),
			'onleash-thumb-masonry'		=> esc_html__( 'Masonry (scaled)', 'onleash' ),
			)
		);
		$mult = onleash_get_theme_option('retina_ready', 1);
		foreach($thumb_sizes as $k=>$v) {
			$sizes[$k] = $v;
			if ($mult > 1) $sizes[$k.'-@retina'] = $v.' '.esc_html__('@2x', 'onleash' );
		}
		return $sizes;
	}
}

// Remove some thumb-sizes from the ThemeREX Addons list
if ( !function_exists( 'onleash_customizer_trx_addons_add_thumb_sizes' ) ) {
	add_filter( 'trx_addons_filter_add_thumb_sizes', 'onleash_customizer_trx_addons_add_thumb_sizes');
	function onleash_customizer_trx_addons_add_thumb_sizes($list=array()) {
		if (is_array($list)) {
			foreach ($list as $k=>$v) {
				if (in_array($k, array(
								'trx_addons-thumb-huge',
								'trx_addons-thumb-big',
								'trx_addons-thumb-medium',
								'trx_addons-thumb-tiny',
								'trx_addons-thumb-masonry-big',
								'trx_addons-thumb-masonry',
								)
							)
						) unset($list[$k]);
			}
		}
		return $list;
	}
}

// and replace removed styles with theme-specific thumb size
if ( !function_exists( 'onleash_customizer_trx_addons_get_thumb_size' ) ) {
	add_filter( 'trx_addons_filter_get_thumb_size', 'onleash_customizer_trx_addons_get_thumb_size');
	function onleash_customizer_trx_addons_get_thumb_size($thumb_size='') {
		return str_replace(array(
							'trx_addons-thumb-huge',
							'trx_addons-thumb-huge-@retina',
							'trx_addons-thumb-big',
							'trx_addons-thumb-big-@retina',
							'trx_addons-thumb-medium',
							'trx_addons-thumb-medium-@retina',
							'trx_addons-thumb-tiny',
							'trx_addons-thumb-tiny-@retina',
                            'trx_addons-thumb-blogger',
							'trx_addons-thumb-blogger-@retina',
                            'trx_addons-thumb-team',
							'trx_addons-thumb-team-@retina',
                            'trx_addons-thumb-team-short',
							'trx_addons-thumb-team-short-@retina',
                            'trx_addons-thumb-team-default-with-socials',
							'trx_addons-thumb-team-default-with-socials-@retina',
							'trx_addons-thumb-masonry-big',
							'trx_addons-thumb-masonry-big-@retina',
							'trx_addons-thumb-masonry',
							'trx_addons-thumb-masonry-@retina',
							),
							array(
							'onleash-thumb-huge',
							'onleash-thumb-huge-@retina',
							'onleash-thumb-big',
							'onleash-thumb-big-@retina',
							'onleash-thumb-med',
							'onleash-thumb-med-@retina',
							'onleash-thumb-tiny',
							'onleash-thumb-tiny-@retina',
                            'onleash-thumb-blogger',
							'onleash-thumb-blogger-@retina',
                            'onleash-thumb-team',
							'onleash-thumb-team-@retina',
                            'onleash-thumb-team-short',
							'onleash-thumb-team-short-@retina',
                            'onleash-thumb-team-default-with-socials',
							'onleash-thumb-team-default-with-socials-@retina',
							'onleash-thumb-masonry-big',
							'onleash-thumb-masonry-big-@retina',
							'onleash-thumb-masonry',
							'onleash-thumb-masonry-@retina',
							),
							$thumb_size);
	}
}
?>