<?php
/**
 * The template to display the page title and breadcrumbs
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

// Page (category, tag, archive, author) title

if ( onleash_need_page_title() ) {
    onleash_sc_layouts_showed('title', true);
	?>
	<div class="top_panel_title sc_layouts_row sc_layouts_row_type_normal">
		<div class="content_wrap">
			<div class="sc_layouts_column sc_layouts_column_align_center">
				<div class="sc_layouts_item">
					<div class="sc_layouts_title sc_align_center">
						<?php
						// Blog/Post title
						?><div class="sc_layouts_title_title"><?php
							$onleash_blog_title = onleash_get_blog_title();
							$onleash_blog_title_text = $onleash_blog_title_class = $onleash_blog_title_link = $onleash_blog_title_link_text = '';
							if (is_array($onleash_blog_title)) {
								$onleash_blog_title_text = $onleash_blog_title['text'];
								$onleash_blog_title_class = !empty($onleash_blog_title['class']) ? ' '.$onleash_blog_title['class'] : '';
								$onleash_blog_title_link = !empty($onleash_blog_title['link']) ? $onleash_blog_title['link'] : '';
								$onleash_blog_title_link_text = !empty($onleash_blog_title['link_text']) ? $onleash_blog_title['link_text'] : '';
							} else
								$onleash_blog_title_text = $onleash_blog_title;
							?>
							<h1 class="sc_layouts_title_caption<?php echo esc_attr($onleash_blog_title_class); ?>"><?php
								$onleash_top_icon = onleash_get_category_icon();
								if (!empty($onleash_top_icon)) {
									$onleash_attr = onleash_getimagesize($onleash_top_icon);
									?><img src="<?php echo esc_url($onleash_top_icon); ?>" alt="" <?php if (!empty($onleash_attr[3])) onleash_show_layout($onleash_attr[3]);?>><?php
								}
								echo wp_kses_data($onleash_blog_title_text);
							?></h1>
							<?php
							if (!empty($onleash_blog_title_link) && !empty($onleash_blog_title_link_text)) {
								?><a href="<?php echo esc_url($onleash_blog_title_link); ?>" class="theme_button theme_button_small sc_layouts_title_link"><?php echo esc_html($onleash_blog_title_link_text); ?></a><?php
							}
							
							// Category/Tag description
							if ( is_category() || is_tag() || is_tax() ) 
								the_archive_description( '<div class="sc_layouts_title_description">', '</div>' );
		
						?></div><?php
	
						// Breadcrumbs
						?><div class="sc_layouts_title_breadcrumbs"><?php
							do_action( 'onleash_action_breadcrumbs');
						?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>