<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.06
 */

$onleash_header_css = $onleash_header_image = '';
$onleash_header_video = onleash_get_header_video();
if (true || empty($onleash_header_video)) {
	$onleash_header_image = get_header_image();
	if (onleash_is_on(onleash_get_theme_option('header_image_override')) && apply_filters('onleash_filter_allow_override_header_image', true)) {
		if (is_category()) {
			if (($onleash_cat_img = onleash_get_category_image()) != '')
				$onleash_header_image = $onleash_cat_img;
		} else if (is_singular() || onleash_storage_isset('blog_archive')) {
			if (has_post_thumbnail()) {
				$onleash_header_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				if (is_array($onleash_header_image)) $onleash_header_image = $onleash_header_image[0];
			} else
				$onleash_header_image = '';
		}
	}
}

$onleash_header_id = str_replace('header-custom-', '', onleash_get_theme_option("header_style"));
$onleash_header_meta = get_post_meta($onleash_header_id, 'trx_addons_options', true);

?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr($onleash_header_id); 
						?> top_panel_custom_<?php echo esc_attr(sanitize_title(get_the_title($onleash_header_id)));
						echo !empty($onleash_header_image) || !empty($onleash_header_video) 
							? ' with_bg_image' 
							: ' without_bg_image';
						if ($onleash_header_video!='') 
							echo ' with_bg_video';
						if ($onleash_header_image!='') 
							echo ' '.esc_attr(onleash_add_inline_css_class('background-image: url('.esc_url($onleash_header_image).');'));
						if (!empty($onleash_header_meta['margin']) != '') 
							echo ' '.esc_attr(onleash_add_inline_css_class('margin-bottom: '.esc_attr(onleash_prepare_css_value($onleash_header_meta['margin'])).';'));
						if (is_single() && has_post_thumbnail()) 
							echo ' with_featured_image';
						if (onleash_is_on(onleash_get_theme_option('header_fullheight'))) 
							echo ' header_fullheight trx-stretch-height';
						?> scheme_<?php echo esc_attr(onleash_is_inherit(onleash_get_theme_option('header_scheme')) 
														? onleash_get_theme_option('color_scheme') 
														: onleash_get_theme_option('header_scheme'));
						?>"><?php

	// Background video
	if (!empty($onleash_header_video)) {
		get_template_part( 'templates/header-video' );
	}
		
	// Custom header's layout
	do_action('onleash_action_show_layout', $onleash_header_id);

	// Header widgets area
	get_template_part( 'templates/header-widgets' );
		
?></header>