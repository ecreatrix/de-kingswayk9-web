<?php
/**
 * Connect to the Instagram API and return profile info
 *
 * long access token required
 *
 * info =
 *  *** caption, id, media_type, media_url, permalink, thumbnail_url, timestamp, username, children{id, media_type, media_url, permalink, thumbnail_url, timestamp, username}
 *
 * @since 2.0
 */
namespace wa\APIs\Instagram\Connect;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Media::class ) ) {
	class Media extends ConnectionParent {
		public $access;

		// Look for new information every 2 days
		public $expiry = DAY_IN_SECONDS * 2;

		public $key = 'media';

		public $mediaFields = 'caption, id, media_type, media_url, permalink, thumbnail_url, timestamp, username, children{id, media_type, media_url, permalink, thumbnail_url, timestamp, username}';

		//public $mediaChildrenFields = 'id, media_type, media_url, permalink, thumbnail_url, timestamp, username';

		// Setup data for various API request
		public function __construct( $reset, $access ) {
			parent::__construct( $reset, 'media', [] );
			$this->access = $access;

			//$this->connect( $access, $args );
		}

		public function connect( $args = [] ) {
			if ( ! parent::call_ready( $this->access ) ) {
				return;
			}

			$params = [
				'fields' => $this->mediaFields
			];

			if ( array_key_exists( 'limit', $args ) && $args['limit'] > 0 ) {
				$params['limit'] = $args['limit'];
			}
			if ( array_key_exists( 'before', $args ) && isset( $args['before'] ) ) {
				$params['before'] = $args['before'];
			}
			if ( array_key_exists( 'after', $args ) && isset( $args['after'] ) ) {
				$params['after'] = $args['after'];
			}

			$media = Plugin\Instagram\Authenticate::makeCall( $this->access, 'me/media', $params );

			if ( $media && is_object( $media ) ) {
				$this->info = [
					'data'          => $media->data,
					'paging'        => $media->paging,
					'expirationRaw' => $this->expiry + time()
				];
				//$this->info = $media;

				$this->messages->add( 'Media information received from Instagram', 'success', 'media_connected' );
			}
		}

		public function pagination( $obj ) {
			if ( is_object( $obj ) && ! null === $obj->paging ) {
				if ( ! isset( $obj->paging->next ) ) {
					return;
				}

				$apiCall = explode( '?', $obj->paging->next );

				if ( count( $apiCall ) < 2 ) {
					return;
				}

				$function = str_replace( self::API_URL, '', $apiCall[0] );
				parse_str( $apiCall[1], $params );

				parent::makeCall( $function, $params );

				$this->messages->add( 'Media item\'s next page received.', 'success', 'media_pagination_success' );

				return;
			}

			$this->messages->add( 'Error: pagination() | This method doesn\'t support pagination.', 'warning', 'media_pagination_error' );
		}
	}
}