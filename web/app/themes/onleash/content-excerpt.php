<?php
/**
 * The default template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_post_format = get_post_format();
$onleash_post_format = empty($onleash_post_format) ? 'standard' : str_replace('post-format-', '', $onleash_post_format);
$onleash_animation = onleash_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_excerpt post_format_'.esc_attr($onleash_post_format) ); ?>
	<?php echo (!onleash_is_off($onleash_animation) ? ' data-animation="'.esc_attr(onleash_get_animation_classes($onleash_animation)).'"' : ''); ?>
	><?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

    $dt = get_the_date('d-M');
    if (!empty($dt)) {
        $dt = explode('-', $dt);
        $date = '<span class="sc_excerpt_item_date"><span class="sc_excerpt_item_day">'.$dt[0].'</span>
		            <span class="sc_excerpt_item_month">'.$dt[1].'</span>
	            </span>';
    } else {
        $date ='';
    }

    // Title and post meta
    if (get_the_title() != '') {
        ?>
        <div class="post_header entry-header">
            <?php
            do_action('onleash_action_before_post_title');

            // Post title
            if (!has_post_thumbnail() && $onleash_post_format == 'standard') {
                onleash_show_layout($date);
            }
            the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
            if (!has_post_thumbnail() && $onleash_post_format == 'standard') {
                onleash_show_post_meta(array(
                        'categories' => true,
                        'author' => true,
                        'date' => false,
                        'edit' => false,
                        'seo' => false,
                        'share' => false,
                        'counters' => ''	//comments,likes,views - comma separated in any combination
                    )
                );
            }
            ?>
        </div><!-- .post_header --><?php
    }

	// Featured image
	onleash_show_post_featured(array( 'thumb_size' => onleash_get_thumb_size( strpos(onleash_get_theme_option('body_style'), 'full')!==false ? 'full' : 'big' ),'post_info' => $onleash_post_format == 'standard' ? $date : '', ));

    do_action('onleash_action_before_post_meta');

    // Post meta
    if (!(!has_post_thumbnail() && $onleash_post_format == 'standard')) {
        onleash_show_post_meta(array(
                'categories' => true,
                'author' => true,
                'date' => false,
                'edit' => false,
                'seo' => false,
                'share' => false,
                'counters' => ''    //comments,likes,views - comma separated in any combination
            )
        );
    }

	// Post content
	?><div class="post_content entry-content"><?php
		if (onleash_get_theme_option('blog_content') == 'fullpost') {
			// Post content area
			?><div class="post_content_inner"><?php
				the_content( '' );
			?></div><?php
			// Inner pages
			wp_link_pages( array(
				'before'      => '<div class="page_links"><span class="page_links_title">' . esc_html__( 'Pages:', 'onleash' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'onleash' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

		} else {

			$onleash_show_learn_more = !in_array($onleash_post_format, array('link', 'aside', 'status', 'quote'));

			// Post content area
			?><div class="post_content_inner"><?php
				if (has_excerpt()) {
					the_excerpt();
				} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
					the_content( '' );
				} else if (in_array($onleash_post_format, array('link', 'aside', 'status'))) {
					the_content();
				} else if ($onleash_post_format == 'quote') {
					if (($quote = onleash_get_tag(get_the_content(), '<blockquote>', '</blockquote>'))!='')
						onleash_show_layout(wpautop($quote));
					else
						the_excerpt();
				} else if (substr(get_the_content(), 0, 1)!='[') {
					the_excerpt();
				}
			?></div><?php

            if ( is_sticky() && !is_paged() ) {
                onleash_show_post_meta(array(
                        'categories' => true,
                        'author' => true,
                        'date' => false,
                        'edit' => false,
                        'seo' => false,
                        'share' => false,
                        'counters' => ''    //comments,likes,views - comma separated in any combination
                    )
                );
            }

			// More button
			if ( $onleash_show_learn_more ) {
				?><p><a class="more-link" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read more', 'onleash'); ?></a></p><?php
			}

		}
	?></div><!-- .entry-content -->
</article>