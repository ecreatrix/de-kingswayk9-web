<?php
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Index::class ) ) {
	class Index {
		public static $key = 'instagram';

		public function __construct() {
			// Cron job, will only work when access is setup
			//add_action( Plugin\CODENAME . '_instagram_connect', [$this, 'connect'] );

			$this->init_shortcode();
		}

		public static function get_key() {
			return self::$key;
		}

		// Setup CRON job to automatically reconnect API if info available
		/*public function connect() {
		$settings = Plugin\Settings\Options::get_settings( self::get_key() );

		$access = new Settings\Access( $submitted_access_info, $page );
		$access->connect();
		$settings['access'] = $access->get_info();

		$update_successful = Plugin\Settings\Options::update_settings( self::get_key(), $settings );
		}*/

		public function init_shortcode() {
			if ( class_exists( Shortcode::class ) ) {
				new Shortcode();
			}
		}
	}
}
?>