<?php
namespace wa\APIs\Settings;

use wa\APIs as APIs;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Rest::class ) ) {
    class Rest {
        public function __construct() {}

        public static function get_settings_request( \WP_REST_Request $request ) {
            $settings = Options::get_settings( Options::get_options_key() );

            return APIs\Rest\Messages::response_success( json_encode( $settings ) );
        }

        public static function update_settings_request( \WP_REST_Request $request ) {
            $new_settings = $request->get_param( 'values' );
            $reset = $request->get_param( 'reset' );

            $update_successful = Options::update_settings( $new_settings );

            $messages = new APIs\Rest\Messages();
            $messages->settings_change( $reset, $update_successful );

            // No further messages manipulations needed, return message

            return $messages->response_return( $messages, $new_settings );
        }
    }
}
