<?php
namespace wa\APIs\Settings;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {
            $this->init_classes();
        }

        public function init_classes() {
            if ( class_exists( Options::class ) ) {
                new Options();
            }
            if ( class_exists( Pages::class ) ) {
                new Pages();
            }

            if ( class_exists( Plugin\Instagram\Index::class ) ) {
                new Plugin\Instagram\Index();
            }
        }
    }
}
