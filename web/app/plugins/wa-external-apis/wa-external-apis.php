<?php
/**
 * @wordpress-plugin
 * Plugin Name: WA APIs connection
 * Description: Connect to various APIs
 * Version:     2020.11.17
 * Author:      Alyx Calder
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 * Plugin Type: Piklist
 * @package   wa-APIs
 *
 * @author    Alyx Calder <info@ecreatrix.com>
 * @copyright 2020 Alyx Calder
 * @license   GPL-2.0+
 */
namespace wa\APIs;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {exit;}

if ( ! class_exists( Plugin::class ) ) {
      defined( __NAMESPACE__ . '\VERSION' ) || define( __NAMESPACE__ . '\VERSION', '2' );

      defined( __NAMESPACE__ . '\URI' ) || define( __NAMESPACE__ . '\URI', plugin_dir_url( __FILE__ ) );
      defined( __NAMESPACE__ . '\PATH' ) || define( __NAMESPACE__ . '\PATH', plugin_dir_path( __FILE__ ) );

      defined( __NAMESPACE__ . '\NAME' ) || define( __NAMESPACE__ . '\NAME', 'External APIs' );
      defined( __NAMESPACE__ . '\SHORTNAME' ) || define( __NAMESPACE__ . '\SHORTNAME', 'wa-apis' );
      defined( __NAMESPACE__ . '\CODENAME' ) || define( __NAMESPACE__ . '\CODENAME', 'waapis' );

      defined( __NAMESPACE__ . '\BLOCK' ) || define( __NAMESPACE__ . '\BLOCK', 'waapis' );

      // Used by Instagram API
      // Upload folder name for local image files for posts
      defined( __NAMESPACE__ . '\IG_UPLOADS_NAME' ) || define( __NAMESPACE__ . '\IG_UPLOADS_NAME', CODENAME . '_instagram-feed-images' );

      // Name of the database table that contains instagram posts
      defined( __NAMESPACE__ . '\IG_POSTS_TYPE' ) || define( __NAMESPACE__ . '\IG_POSTS_TYPE', 'instagram_posts' );

      // Name of the database table that contains feed ids and the ids of posts
      defined( __NAMESPACE__ . '\IG_FEEDS_POSTS' ) || define( __NAMESPACE__ . '\IG_FEEDS_POSTS', 'instagram_feeds_posts' );
      defined( __NAMESPACE__ . '\REFRESH_THRESHOLD_OFFSET' ) || define( __NAMESPACE__ . '\REFRESH_THRESHOLD_OFFSET', 40 * 86400 );
      defined( __NAMESPACE__ . '\MINIMUM_INTERVAL' ) || define( __NAMESPACE__ . '\MINIMUM_INTERVAL', 600 );

      defined( __NAMESPACE__ . '\BACKUP_PREFIX' ) || define( __NAMESPACE__ . '\BACKUP_PREFIX', '!' );
      defined( __NAMESPACE__ . '\TAGGED_PREFIX' ) || define( __NAMESPACE__ . '\TAGGED_PREFIX', '^' );
      defined( __NAMESPACE__ . '\FPL_PREFIX' ) || define( __NAMESPACE__ . '\FPL_PREFIX', '$' );
      defined( __NAMESPACE__ . '\USE_BACKUP_PREFIX' ) || define( __NAMESPACE__ . '\USE_BACKUP_PREFIX', '&' );
      defined( __NAMESPACE__ . '\CRON_UPDATE_CACHE_TIME' ) || define( __NAMESPACE__ . '\CRON_UPDATE_CACHE_TIME', 60 * 60 * 24 * 60 );
      defined( __NAMESPACE__ . '\MAX_RECORDS' ) || define( __NAMESPACE__ . '\MAX_RECORDS', 750 );

      defined( __NAMESPACE__ . '\STYLE' ) || define( __NAMESPACE__ . '\STYLE', 'external-apis' );

      $loader = plugin_dir_path( __FILE__ ) . 'resources/includes/loader.php';
      if ( $loader ) {
            require_once $loader;

            spl_autoload_register( function ( $class ) {
                  \wa\loader( $class, plugin_dir_path( __FILE__ ) . 'resources/', 'wa\\APIs\\' );
            } );
      }

      class Plugin {
            // Holds the main instance.
            private static $__instance = null;

            public function __construct() {
                  $this->init_resources();

                  add_action( 'init', [$this, 'plugin_piklist'] );
                  register_activation_hook( __FILE__,  [$this, 'activate' ] );
                  register_deactivation_hook( __FILE__, [$this, 'deactivate'] );

                  //$update_successful = Settings\Options::update_settings( [], Instagram\Index::get_key() );
                  /*$default = [
                        'access' => [
                              //'id' => '1223475488003561'
                              'secret' => 'da5666bdf9240b4615669e3ffbdd575b'
                              //'redirectUrl' => 'https://withalyx.ca/apis/instagram.php',
                              //'authCode'    => 'AQBcZe6gN_xkegKCelaLEswK50t0H_rrgObAOVHA4xVohrlLMupv6cNX0-N-3ioAHghVOLYxSuQC0NXQNM0K_pMKccKe0nHK7hVWZNLssR6oAnC1Q-n55T-LvF2jyTQ8ishMbWJPoCa-YHo5srIHsefUt08HX0uHuVwMSjFhVnOYwV3w4way0fFG-VFL5X0Z6H0_vhrCqiB5zG3jJuJ9l3b_KjkAWpVlku9IZzmslBxPoQ'
                              //'shortTermToken' => 'IGQVJYV1AybmNmRUZA0M0pETHpvVFlLSGVMMkhDYnlEeElEUktzQ3lZAVGVkX3c0ck5MMThjMnV4RzI4bmJLc3lJZAUU4NTlFOUJxT1RDSzh6dWdSSzNPUl9PbndmQVE2RW9sRFpNQlJaS0xrajFKMWNuTnRoUE9jWTV1TFRZA'
                        ]
                  ];

                  $connect = Instagram\Connect\Index::connect_all( false, $default['access'] );

                  $messages = $connect['messages'];
                  $settings = $connect['connection'];*/
                  //$update_successful = Settings\Options::update_settings( $settings, Instagram\Index::get_key() );

                  //var_dump( $connect );
                  /*$return = [
                  'access'  => $settings['access']->get_info(),
                  'display' => $settings['display']->get_info()
                  ];

                  var_dump( $return );*/
                  //$messages->settings_change( false, $update_successful );
                  //var_dump( $settings );
                  //$instagram = Settings\Options::get_settings( Instagram\Index::get_key() );
                  //var_dump( $instagram );

                  //$access = new Instagram\Settings\Access( false, $default['access'] );
                  //$access->connect();
                  //$settings['access'] = $access->get_info();

                  //$profile = new Instagram\Settings\Profile( false, $access );
                  //$profile->connect();
                  //$settings['profile'] = $profile->get_info();

                  //$media = new Instagram\Settings\Media( false, $access );
                  //$media->connect();
                  //$settings['media'] = $media->get_info();

                  //var_dump( Instagram\Rest::ajax_parameters() );
                  //Cron\Index::deactivate();
                  Cron\Index::activate();
                  //(new Instagram\Cron())->refresh_access();
                  
                  //$settings = Settings\Options::get_settings( Instagram\Index::get_key() );
                  //var_dump($settings['access']->get_info());
            }

            public function a_debug() {
                  //$carousel = new Instagram\Content\Carousel();
                  //$carousel->content();

                  $transients = new Instagram\Transients();

                  $access = new Instagram\Settings\Access();
                  $access->connect();
            }

            // Stop cron events when plugin is deactivated
            public function activate() {
                  Cron\Index::activate();
            }
            public function deactivate() {
                  Cron\Index::deactivate();
            }

            public function init_resources() {
                  //$this->init_blocks();
                  $this->init_scripts();

                  add_action( 'plugins_loaded', [$this, 'init_settings'] );
            }

            public function init_scripts() {
                  if ( class_exists( Rest\Index::class ) ) {
                        new Rest\Index();
                  }

                  if ( class_exists( Scripts\Index::class ) ) {
                        new Scripts\Index();
                  }
            }

            public function init_settings() {
                  if ( class_exists( Settings\Index::class ) ) {
                        new Settings\Index();
                  }
            }

            // Get/create the plugin instance.
            public static function instance() {
                  if ( empty( self::$__instance ) ) {
                        self::$__instance = new Plugin();
                  }

                  return self::$__instance;
            }

            // register and load the widget
            public function load_widget() {
                  if ( class_exists( Content\Widget::class ) ) {
                        //new Content\Widget();
                        //register_widget( Content\Widget::class );
                  }
            }

            public function plugin_piklist() {
                  if ( is_admin() ) {
                        //include_once 'resources/plugins/class-PiklistChecker.php';
                        if ( class_exists( Plugins\PiklistChecker::class ) ) {
                              //new Plugins\PiklistChecker();
                        }
                  }
            }
      }

      Plugin::instance();
}
