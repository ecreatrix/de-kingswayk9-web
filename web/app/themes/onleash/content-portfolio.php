<?php
/**
 * The Portfolio template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_blog_style = explode('_', onleash_get_theme_option('blog_style'));
$onleash_columns = empty($onleash_blog_style[1]) ? 2 : max(2, $onleash_blog_style[1]);
$onleash_post_format = get_post_format();
$onleash_post_format = empty($onleash_post_format) ? 'standard' : str_replace('post-format-', '', $onleash_post_format);
$onleash_animation = onleash_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_portfolio_'.esc_attr($onleash_columns).' post_format_'.esc_attr($onleash_post_format).(is_sticky() && !is_paged() ? ' sticky' : '') ); ?>
	<?php echo (!onleash_is_off($onleash_animation) ? ' data-animation="'.esc_attr(onleash_get_animation_classes($onleash_animation)).'"' : ''); ?>>
	<?php

	// Sticky label
	if ( is_sticky() && !is_paged() ) {
		?><span class="post_label label_sticky"></span><?php
	}

	$onleash_image_hover = onleash_get_theme_option('image_hover');
	// Featured image
	onleash_show_post_featured(array(
		'thumb_size' => onleash_get_thumb_size(strpos(onleash_get_theme_option('body_style'), 'full')!==false || $onleash_columns < 3 ? 'masonry-big' : 'masonry'),
		'show_no_image' => true,
		'class' => $onleash_image_hover == 'dots' ? 'hover_with_info' : '',
		'post_info' => $onleash_image_hover == 'dots' ? '<div class="post_info">'.esc_html(get_the_title()).'</div>' : ''
	));
	?>
</article>