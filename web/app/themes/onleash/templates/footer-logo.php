<?php
/**
 * The template to display the site logo in the footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */

// Logo
if (onleash_is_on(onleash_get_theme_option('logo_in_footer'))) {
	$onleash_logo_image = '';
	if (onleash_get_retina_multiplier(2) > 1)
		$onleash_logo_image = onleash_get_theme_option( 'logo_footer_retina' );
	if (empty($onleash_logo_image)) 
		$onleash_logo_image = onleash_get_theme_option( 'logo_footer' );
	$onleash_logo_text   = get_bloginfo( 'name' );
	if (!empty($onleash_logo_image) || !empty($onleash_logo_text)) {
		?>
		<div class="footer_logo_wrap">
			<div class="footer_logo_inner">
				<?php
				if (!empty($onleash_logo_image)) {
					$onleash_attr = onleash_getimagesize($onleash_logo_image);
					echo '<a href="'.esc_url(home_url('/')).'"><img src="'.esc_url($onleash_logo_image).'" class="logo_footer_image" alt=""'.(!empty($onleash_attr[3]) ? sprintf(' %s', $onleash_attr[3]) : '').'></a>' ;
				} else if (!empty($onleash_logo_text)) {
					echo '<h1 class="logo_footer_text"><a href="'.esc_url(home_url('/')).'">' . esc_html($onleash_logo_text) . '</a></h1>';
				}
				?>
			</div>
		</div>
		<?php
	}
}
?>