<?php
namespace wa\APIs\Instagram\Connect;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Display::class ) ) {
	class Display extends ConnectionParent {
		public function __construct( $reset = false, $info = [] ) {
			parent::__construct( $reset, 'display', $info );
		}

		public function defaults() {
			$values = [
				'showComments'     => false,

				'layoutType'       => 'carousel',

				'carouselLoop'     => true,
				'carouselArrows'   => true,
				'carouselDots'     => false,
				'carouselAutoplay' => false,
				'carouselInterval' => 3000,

				'rowsDesktop'      => 1,
				'rowsTablet'       => 1,
				'rowsMobile'       => 1,

				'colsDesktop'      => 5,
				'colsTablet'       => 2,
				'colsMobile'       => 1
			];

			return $values;
		}
	}
}