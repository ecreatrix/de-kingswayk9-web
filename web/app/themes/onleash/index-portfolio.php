<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

onleash_storage_set('blog_archive', true);

// Load scripts for both 'Gallery' and 'Portfolio' layouts!
wp_enqueue_script( 'imagesloaded' );
wp_enqueue_script( 'masonry' );
wp_enqueue_script( 'classie', onleash_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
wp_enqueue_script( 'onleash-gallery-script', onleash_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$onleash_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$onleash_sticky_out = onleash_get_theme_option('sticky_style')=='columns' 
							&& is_array($onleash_stickies) && count($onleash_stickies) > 0 && get_query_var( 'paged' ) < 1;
	
	// Show filters
	$onleash_cat = onleash_get_theme_option('parent_cat');
	$onleash_post_type = onleash_get_theme_option('post_type');
	$onleash_taxonomy = onleash_get_post_type_taxonomy($onleash_post_type);
	$onleash_show_filters = onleash_get_theme_option('show_filters');
	$onleash_tabs = array();
	if (!onleash_is_off($onleash_show_filters)) {
		$onleash_args = array(
			'type'			=> $onleash_post_type,
			'child_of'		=> $onleash_cat,
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 0,
			'exclude'		=> '',
			'include'		=> '',
			'number'		=> '',
			'taxonomy'		=> $onleash_taxonomy,
			'pad_counts'	=> false
		);
		$onleash_portfolio_list = get_terms($onleash_args);
		if (is_array($onleash_portfolio_list) && count($onleash_portfolio_list) > 0) {
			$onleash_tabs[$onleash_cat] = esc_html__('All', 'onleash');
			foreach ($onleash_portfolio_list as $onleash_term) {
				if (isset($onleash_term->term_id)) $onleash_tabs[$onleash_term->term_id] = $onleash_term->name;
			}
		}
	}
	if (count($onleash_tabs) > 0) {
		$onleash_portfolio_filters_ajax = true;
		$onleash_portfolio_filters_active = $onleash_cat;
		$onleash_portfolio_filters_id = 'portfolio_filters';
		if (!is_customize_preview())
			wp_enqueue_script('jquery-ui-tabs', false, array('jquery', 'jquery-ui-core'), null, true);
		?>
		<div class="portfolio_filters onleash_tabs onleash_tabs_ajax">
			<ul class="portfolio_titles onleash_tabs_titles">
				<?php
				foreach ($onleash_tabs as $onleash_id=>$onleash_title) {
					?><li><a href="<?php echo esc_url(onleash_get_hash_link(sprintf('#%s_%s_content', $onleash_portfolio_filters_id, $onleash_id))); ?>" data-tab="<?php echo esc_attr($onleash_id); ?>"><?php echo esc_html($onleash_title); ?></a></li><?php
				}
				?>
			</ul>
			<?php
			$onleash_ppp = onleash_get_theme_option('posts_per_page');
			if (onleash_is_inherit($onleash_ppp)) $onleash_ppp = '';
			foreach ($onleash_tabs as $onleash_id=>$onleash_title) {
				$onleash_portfolio_need_content = $onleash_id==$onleash_portfolio_filters_active || !$onleash_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr(sprintf('%s_%s_content', $onleash_portfolio_filters_id, $onleash_id)); ?>"
					class="portfolio_content onleash_tabs_content"
					data-blog-template="<?php echo esc_attr(onleash_storage_get('blog_template')); ?>"
					data-blog-style="<?php echo esc_attr(onleash_get_theme_option('blog_style')); ?>"
					data-posts-per-page="<?php echo esc_attr($onleash_ppp); ?>"
					data-post-type="<?php echo esc_attr($onleash_post_type); ?>"
					data-taxonomy="<?php echo esc_attr($onleash_taxonomy); ?>"
					data-cat="<?php echo esc_attr($onleash_id); ?>"
					data-parent-cat="<?php echo esc_attr($onleash_cat); ?>"
					data-need-content="<?php echo (false===$onleash_portfolio_need_content ? 'true' : 'false'); ?>"
				>
					<?php
					if ($onleash_portfolio_need_content) 
						onleash_show_portfolio_posts(array(
							'cat' => $onleash_id,
							'parent_cat' => $onleash_cat,
							'taxonomy' => $onleash_taxonomy,
							'post_type' => $onleash_post_type,
							'page' => 1,
							'sticky' => $onleash_sticky_out
							)
						);
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		onleash_show_portfolio_posts(array(
			'cat' => $onleash_cat,
			'parent_cat' => $onleash_cat,
			'taxonomy' => $onleash_taxonomy,
			'post_type' => $onleash_post_type,
			'page' => 1,
			'sticky' => $onleash_sticky_out
			)
		);
	}

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>