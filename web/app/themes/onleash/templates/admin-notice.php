<?php
/**
 * The template to display Admin notices
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.1
 */
?>
<div class="update-nag" id="onleash_admin_notice">
	<h3 class="onleash_notice_title"><?php echo sprintf(esc_html__('Welcome to %s', 'onleash'), wp_get_theme()->name); ?></h3>
	<?php
	if (!onleash_exists_trx_addons()) {
		?><p><?php echo wp_kses_data(__('<b>Attention!</b> Plugin "ThemeREX Addons is required! Please, install and activate it!', 'onleash')); ?></p><?php
	}
	?><p><?php
		if (onleash_get_value_gp('page')!='tgmpa-install-plugins') {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=tgmpa-install-plugins'); ?>" class="button-primary"><i class="dashicons dashicons-admin-plugins"></i> <?php esc_html_e('Install plugins', 'onleash'); ?></a>
			<?php
		}
		if (function_exists('onleash_exists_trx_addons') && onleash_exists_trx_addons()) {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=trx_importer'); ?>" class="button-primary"><i class="dashicons dashicons-download"></i> <?php esc_html_e('One Click Demo Data', 'onleash'); ?></a>
			<?php
		}
		?>
        <a href="<?php echo esc_url(admin_url().'customize.php'); ?>" class="button-primary"><i class="dashicons dashicons-admin-appearance"></i> <?php esc_html_e('Theme Customizer', 'onleash'); ?></a>
        <a href="#" class="button onleash_hide_notice"><i class="dashicons dashicons-dismiss"></i> <?php esc_html_e('Hide Notice', 'onleash'); ?></a>
	</p>
</div>