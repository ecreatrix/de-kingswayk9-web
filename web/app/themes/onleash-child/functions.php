<?php
/**
 * Child-Theme functions and definitions
 */

// Load frontend scripts
if ( !function_exists( 'onleash_wp_scripts' ) ) {
    //Handler of the add_action('wp_enqueue_scripts', 'onleash_wp_scripts', 1000);
    function onleash_wp_scripts() {
        
        // Enqueue styles
        //------------------------
        
        // Links to selected fonts
        $links = onleash_theme_fonts_links();
        if (count($links) > 0) {
            foreach ($links as $slug => $link) {
                wp_enqueue_style( sprintf('onleash-font-%s', $slug), $link );
            }
        }
        
        // Fontello styles must be loaded before main stylesheet
        // This style NEED the theme prefix, because style 'fontello' in some plugin contain different set of characters
        // and can't be used instead this style!
        wp_enqueue_style( 'onleash-fontello',  onleash_get_file_url('css/fontello/css/fontello-embedded.css') );

        // Load main stylesheet
        $main_stylesheet = get_template_directory_uri() . '/style.css';
        wp_enqueue_style( 'onleash-main', $main_stylesheet, array(), filemtime(get_template_directory() . '/style.css') );

        // Load child stylesheet (if different) after the main stylesheet and fontello icons (important!)
        $child_stylesheet = get_stylesheet_directory_uri() . '/style.css';
        if ($child_stylesheet != $main_stylesheet) {
            wp_enqueue_style( 'onleash-child', $child_stylesheet, array('onleash-main'), filemtime(get_stylesheet_directory() . '/style.css') );
        }

        // Add custom bg image for the body_style == 'boxed'
        if ( onleash_get_theme_option('body_style') == 'boxed' && ($bg_image = onleash_get_theme_option('boxed_bg_image')) != '' )
            wp_add_inline_style( 'onleash-main', '.body_style_boxed { background-image:url('.esc_url($bg_image).') }' );

        // Merged styles
        if ( onleash_is_off(onleash_get_theme_option('debug_mode')) )
            wp_enqueue_style( 'onleash-styles', onleash_get_file_url('css/__styles.css') );

        // Custom colors
        if ( !is_customize_preview() && !isset($_GET['color_scheme']) && onleash_is_off(onleash_get_theme_option('debug_mode')) )
            wp_enqueue_style( 'onleash-colors', onleash_get_file_url('css/__colors.css') );
        else
            wp_add_inline_style( 'onleash-main', onleash_customizer_get_css() );

        // Add post nav background
        onleash_add_bg_in_post_nav();

        // Disable loading JQuery UI CSS
        wp_deregister_style('jquery_ui');
        wp_deregister_style('date-picker-css');


        // Enqueue scripts  
        //------------------------
        
        // Modernizr will load in head before other scripts and styles
        if ( in_array(substr(onleash_get_theme_option('blog_style'), 0, 7), array('gallery', 'portfol', 'masonry')) )
            wp_enqueue_script( 'modernizr', onleash_get_file_url('js/theme.gallery/modernizr.min.js'), array(), null, false );

        // Superfish Menu
        // Attention! To prevent duplicate this script in the plugin and in the menu, don't merge it!
        wp_enqueue_script( 'superfish', onleash_get_file_url('js/superfish.js'), array('jquery'), null, true );
        
        // Merged scripts
        if ( onleash_is_off(onleash_get_theme_option('debug_mode')) )
            wp_enqueue_script( 'onleash-init', onleash_get_file_url('js/__scripts.js'), array('jquery'), null, true );
        else {
            // Skip link focus
            wp_enqueue_script( 'skip-link-focus-fix', onleash_get_file_url('js/skip-link-focus-fix.js'), null, true );
            // Background video
            $header_video = onleash_get_header_video();
            if (!empty($header_video) && !onleash_is_inherit($header_video)) {
                if (onleash_is_youtube_url($header_video))
                    wp_enqueue_script( 'tubular', onleash_get_file_url('js/jquery.tubular.js'), array('jquery'), null, true );
                else
                    wp_enqueue_script( 'bideo', onleash_get_file_url('js/bideo.js'), array(), null, true );
            }
            // Theme scripts
            wp_enqueue_script( 'onleash-utils', onleash_get_file_url('js/_utils.js'), array('jquery'), null, true );
            wp_enqueue_script( 'onleash-init', onleash_get_file_url('js/_init.js'), array('jquery'), null, true );  
        }
        
        // Comments
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

        // Media elements library   
        if (onleash_get_theme_setting('use_mediaelements')) {
            wp_enqueue_style ( 'mediaelement' );
            wp_enqueue_style ( 'wp-mediaelement' );
            wp_enqueue_script( 'mediaelement' );
            wp_enqueue_script( 'wp-mediaelement' );
        }
    }
}

if (!function_exists('onleash_wp_scripts')) {
    //Handler of the add_action('wp_enqueue_scripts', 'onleash_wp_scripts', 1000);
    function onleash_wp_scripts()
    {

        // Enqueue styles
        //------------------------

        // Links to selected fonts
        $links = onleash_theme_fonts_links();
        if (count($links) > 0) {
            foreach ($links as $slug => $link) {
                wp_enqueue_style(sprintf('onleash-font-%s', $slug), $link);
            }
        }

        // Fontello styles must be loaded before main stylesheet
        // This style NEED the theme prefix, because style 'fontello' in some plugin contain different set of characters
        // and can't be used instead this style!
        wp_enqueue_style('onleash-fontello', onleash_get_file_url('css/fontello/css/fontello-embedded.css'));

        // Load main stylesheet
        $main_stylesheet = get_template_directory_uri() . '/style.css';
        wp_enqueue_style('onleash-main', $main_stylesheet, [], filemtime(get_template_directory() . '/style.css'));

        // Load child stylesheet (if different) after the main stylesheet and fontello icons (important!)
        $child_stylesheet = get_stylesheet_directory_uri() . '/style.css';
        if ($child_stylesheet != $main_stylesheet) {
            wp_enqueue_style('onleash-child', $child_stylesheet, ['onleash-main'], filemtime(get_stylesheet_directory() . '/style.css'));
        }

        // Add custom bg image for the body_style == 'boxed'
        if (onleash_get_theme_option('body_style') == 'boxed' && ($bg_image = onleash_get_theme_option('boxed_bg_image')) != '') {
            wp_add_inline_style('onleash-main', '.body_style_boxed { background-image:url(' . esc_url($bg_image) . ') }');
        }

        // Merged styles
        if (onleash_is_off(onleash_get_theme_option('debug_mode'))) {
            wp_enqueue_style('onleash-styles', onleash_get_file_url('css/__styles.css'));
        }

        // Custom colors
        if (!is_customize_preview() && !isset($_GET['color_scheme']) && onleash_is_off(onleash_get_theme_option('debug_mode'))) {
            wp_enqueue_style('onleash-colors', onleash_get_file_url('css/__colors.css'));
        } else {
            wp_add_inline_style('onleash-main', onleash_customizer_get_css());
        }

        // Add post nav background
        onleash_add_bg_in_post_nav();

        // Disable loading JQuery UI CSS
        wp_deregister_style('jquery_ui');
        wp_deregister_style('date-picker-css');

        // Enqueue scripts
        //------------------------

        // Modernizr will load in head before other scripts and styles
        if (in_array(substr(onleash_get_theme_option('blog_style'), 0, 7), ['gallery', 'portfol', 'masonry'])) {
            wp_enqueue_script('modernizr', onleash_get_file_url('js/theme.gallery/modernizr.min.js'), [], null, false);
        }

        // Superfish Menu
        // Attention! To prevent duplicate this script in the plugin and in the menu, don't merge it!
        wp_enqueue_script('superfish', onleash_get_file_url('js/superfish.js'), ['jquery'], null, true);

        // Merged scripts
        if (onleash_is_off(onleash_get_theme_option('debug_mode'))) {
            wp_enqueue_script('onleash-init', onleash_get_file_url('js/__scripts.js'), ['jquery'], null, true);
        } else {
            // Skip link focus
            wp_enqueue_script('skip-link-focus-fix', onleash_get_file_url('js/skip-link-focus-fix.js'), null, true);
            // Background video
            $header_video = onleash_get_header_video();
            if (!empty($header_video) && !onleash_is_inherit($header_video)) {
                if (onleash_is_youtube_url($header_video)) {
                    wp_enqueue_script('tubular', onleash_get_file_url('js/jquery.tubular.js'), ['jquery'], null, true);
                } else {
                    wp_enqueue_script('bideo', onleash_get_file_url('js/bideo.js'), [], null, true);
                }

            }
            // Theme scripts
            wp_enqueue_script('onleash-utils', onleash_get_file_url('js/_utils.js'), ['jquery'], null, true);
            wp_enqueue_script('onleash-init', onleash_get_file_url('js/_init.js'), ['jquery'], null, true);
        }

        // Comments
        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }

        // Media elements library
        if (onleash_get_theme_setting('use_mediaelements')) {
            wp_enqueue_style('mediaelement');
            wp_enqueue_style('wp-mediaelement');
            wp_enqueue_script('mediaelement');
            wp_enqueue_script('wp-mediaelement');
        }
    }
}
