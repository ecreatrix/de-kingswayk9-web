<?php
namespace wa\APIs\Instagram\Content;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Carousel::class ) ) {
	class Carousel extends Plugin\Instagram\Content {
		public function __construct() {
			parent::__construct();
		}

		public function content( $echo = true ) {
			if ( ! $this->output ) {
				return '';
			}

			$output = $this->output;

			if ( ! $output ) {
				return '';
			}

			$id = ['instafeed-' . time()];

			$class = [Plugin\SHORTNAME . '-instafeed', Plugin\SHORTNAME . '-jumbotron'];

			$content = '<div id="' . implode( ' ', $id ) . '" class="' . implode( ' ', $class ) . '">';

			$content .= '<div class="slick-add-' . Plugin\CODENAME . '" data-slick-atts="{' . $this->slick_atts() . '}">';

			$content .= implode( $output );

			$content .= '</div>';

			$content .= '</div>';

			if ( $echo ) {
				echo $content;
			}

			return $content;
		}

		public function slick_atts() {
			if ( ! $this->display ) {
				return '';
			}

			$settings = $this->display;

			$slick_atts = [
				'focusOnSelect'      => false,
				'centerMode'         => false,

				'slidesToShow'       => $settings['colsDesktop'],
				'slidesToShowTablet' => $settings['colsTablet'],
				'slidesToShowMobile' => $settings['colsMobile'],

				'slidesToScroll'     => 1,
				'cssEase'            => ' &quot;cubic-bezier(0.455, 0.03, 0.515, 0.955)&quot;',

				'infinite'           => $settings['carouselLoop'],
				'autoplay'           => $settings['carouselAutoplay'],
				'arrows'             => $settings['carouselArrows'],
				'dots'               => $settings['carouselDots'],
				'autoplaySpeed'      => $settings['carouselInterval']
			];

			$data_slick = [];
			foreach ( $slick_atts as $key => $value ) {
				if ( is_bool( $value ) ) {
					$value = var_export( $value, true );
				}
				if ( 'on' === $value ) {
					$value = 'true';
				}

				if ( 'off' === $value || empty( $value ) ) {
					$value = 'false';
				}

				$data_slick[] = '&quot;' . $key . '&quot;: ' . $value;
			}

			return implode( ', ', $data_slick );
		}
	}
}
?>