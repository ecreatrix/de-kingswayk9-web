<?php
namespace wa\APIs\Rest;
use wa\APIs as APIs;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( WP::class ) ) {
    class WP {
        public function __construct() {}

        public function edit_theme_permission() {
            if ( ! current_user_can( 'edit_theme_options' ) ) {
                return Messages::response_error( __( 'User doesn\'t have permissions to change options.', 'wa' ), 'user_dont_have_permission' );
            }

            return true;
        }

        public function manage_options_permission() {
            if ( ! current_user_can( 'manage_options' ) ) {
                return Messages::response_error( __( 'User doesn\'t have permissions to change options.', 'wa' ), 'user_dont_have_permission' );
            }

            return true;
        }
    }
}
