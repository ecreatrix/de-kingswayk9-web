<?php
namespace wa\APIs\Scripts;

use wa\APIs as Plugin;

// Exit if accessed directly.

if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {
            $this->init_enqueues();
        }

        public function init_enqueues() {
            if ( class_exists( Enqueues::class ) ) {
                new Enqueues();
            }
            if ( class_exists( Ajax::class ) ) {
                new Ajax();
            }
        }
    }
}
