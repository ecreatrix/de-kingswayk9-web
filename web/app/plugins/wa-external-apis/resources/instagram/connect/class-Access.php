<?php
/**
 * Connect to the Instagram API and return profile info
 *
 * info => [
 *  *** app => [
 *  ***  *** id - from Instagram's app creation
 *  ***  *** secret - from Instagram's app creation
 *  ***  *** redirectUrl - from Instagram's app creation
 *  *** ]
 *  *** auth => [
 *  ***  *** url - combination of app info, used to get auth code
 *  ***  *** code - received from Instagram, used to get shortTerm
 *  *** ]
 *  *** shortTerm => [ connected, token, response ] - connected states whether successful connection has been made and the rest is received from Instagram, used to get long access token
 *  *** longTerm => [ connected, token, tokenType, expiration_raw, expiration_formatted, response ] - connected states whether successful connection has been made and the rest is received from Instagram
 *
 *  *** connection_stage =
 *  *** *** infoneeded - if 3 app info fields NOT set
 *  *** *** authCode - if 3 app info fields set
 *  *** *** shortTerm - if API has connected and received an auth code
 *  *** *** longTerm - if API has connected and received a short access token
 *  *** *** complete - if all tokens received
 *  ]
 * @since 2.0
 */
namespace wa\APIs\Instagram\Connect;
use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Access::class ) ) {
	class Access extends ConnectionParent {
		public $key = 'access';

		// Setup data for various API request
		public function __construct( $reset = false, $info = [] ) {
			parent::__construct( $reset, 'access', $info );

			$this->update_connection_stage();
			$this->auth_link();
		}

		public function auth_link() {
			$link = Plugin\Instagram\Authenticate::create_auth_url( $this->info['id'], $this->info['secret'], $this->info['redirectUrl'] );
			//$this->info['authLink'] = $link;

			$this->info['authButtonConnect'] = '<a class="connect-instagram wp-core-ui wp-ui-primary" href="' . $link . '"><i class="instagram-icon"></i><i className="instagram-name"></i> <i className="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Connect an Instagram Account</a>';
			$this->info['authButtonInfoNeeded'] = '<div class="disabled">Set API information above to access Auth link</div>';
		}

		// Connect to API for short term and long term access in order if needed or refresh existing long term
		public function connect() {
			$this->update_connection_stage();
			$connection_stage = $this->info['connectionStage'];

			//wp_clear_scheduled_hook( Plugin\CODENAME . 'Instagram_connect' );

			if ( 'shortTerm' === $connection_stage ) {
				// If short access needed use Auth Code to get it then get long token from short
				$this->connect_short_access_token();
				$this->connect_long_access_token();
			} else if ( 'longTerm' === $connection_stage ) {
				// If long access needed, connect to get token using short access
				$this->connect_long_access_token();
			} else if ( 'complete' === $connection_stage ) {
				// If long access available, refresh it
				$this->connect_long_access_token( true );

				// Schedule reconnection daily if it's not already scheduled
				//wp_schedule_event( time(), 'daily', Plugin\CODENAME . 'Instagram_connect' );
			} else {
				// Send message that basic information not given yet
				$this->messages->access_message( $connection_stage );
			}

			//var_dump( $this->messages->return_array() );
			$this->update_connection_stage();
		}

		// TODO setup transient to run every 50 days as long lived tokens expire in 60 days
		// Query instagram for access token, used on settings page save and once every 50 days
		// Uses Short Access Token and Secret code to receive Long Access Token, an Expiration and a Token Type from Instagram
		public function connect_long_access_token( $refresh = false ) {
			$stage = $this->info['connectionStage'];
			$status = $this->check_expiry( $this->info['expirationRaw'] );

			if ( 'valid' === $status ) {
				//$this->messages->add( 'Access information in settings is not expired so there is no need to query Instagram', 'success', 'accessSaved_available' );

				//return;
			}

			if ( 'longTerm' !== $stage && 'complete' !== $stage ) {
				// Requirements not met, set message and stop function
				$this->update_connection_stage();

				$this->messages->access_message( $this->info['connectionStage'] );

				return false;
			}

			$url_type = 'long';
			if ( $refresh && 'valid' === $status ) {
				// Refresh Long Access Token
				$args['grant_type'] = 'ig_refresh_token';
				$args['access_token'] = $this->info['longTermToken'];

				$url_type = 'refresh';
			} else {
				// Get Long Access Token

				$args['grant_type'] = 'ig_exchange_token';
				$args['client_secret'] = $this->info['secret'];
				$args['access_token'] = $this->info['shortTermToken'];
			}

			$response = Plugin\Instagram\Authenticate::makeOAuthCall( $args, $this->get_expiration_raw(), $url_type, 'GET' );

			if ( is_object( $response ) && property_exists( $response, 'access_token' ) ) {
				// Connected, update access info
				$expiration_raw = time() + $response->expires_in;
				$expiration_formatted = date( 'Y-m-d H:i:s', $expiration_raw );

				$this->info['longTermToken'] = $response->access_token;
				$this->info['longTermType'] = $response->token_type;
				$this->info['expirationRaw'] = $expiration_raw;
				$this->info['expirationFormatted'] = $expiration_formatted;

				$this->info['longTermResponse'] = json_encode( $response );
				$this->info['longTermConnected'] = true;

				$type = 'success';
				$code = 'oauthSuccess';

				if ( $refresh ) {
					$text = 'Long access token refreshed with a new expiration date of: ' . $expiration_formatted;
				} else {
					$text = 'Long access token received from Instagram with an expiration date of: ' . $expiration_formatted;
				}

				$this->messages->connection_success( $text, $type, $code );
			} else {
				// oAuth or unknown error occured
				$this->info['longTermToken'] = '';
				$this->info['longTermType'] = '';
				$this->info['expirationRaw'] = '';
				$this->info['expirationFormatted'] = '';

				$this->info['longTermResponse'] = [];
				$this->info['longTermConnected'] = false;

				$this->messages->connection_error( $response );
			}

			//var_dump($this->info);
			$this->update_connection_stage();
		}

		// Uses Auth code, redirect url, Api ID and Api Secret to receive Short Access Token and User ID from Instagram
		public function connect_short_access_token() {
			$connection_stage = $this->info['connectionStage'];

			$args['client_id'] = $this->info['id'];
			$args['client_secret'] = $this->info['secret'];
			$args['grant_type'] = 'authorization_code';
			$args['redirect_uri'] = $this->info['redirectUrl'];
			$args['code'] = $this->info['authCode'];

			$response = Plugin\Instagram\Authenticate::makeOAuthCall( $args, $this->get_expiration_raw(), 'short' );

			if ( is_object( $response ) && property_exists( $response, 'access_token' ) ) {
				// Connected, update access info
				$this->info['shortTermToken'] = $response->access_token;

				$this->info['shortTermResponse'] = json_encode( $response );
				$this->info['shortTermConnected'] = true;

				$text = 'Short access token received from Instagram';
				$type = 'success';
				$code = 'oauthSuccess';

				$this->messages->connection_success( $text, $type, $code );
			} else {
				// oAuth or unknown error occured
				$this->info['shortTermToken'] = '';
				$this->info['shortTermConnected'] = false;

				$this->messages->connection_error( $response );
			}

			$this->update_connection_stage();

		}

		// $info will be false if data should be reset
		public function defaults() {
			// Use default values if none given
			$default = [
				'id'                  => '',
				'secret'              => '',
				'redirectUrl'         => '',

				//'authLink'            => '',
				//'authButton'          => [],
				'authCode'            => '',
				'authConnected'       => '',

				'shortTermToken'      => '',
				'shortTermConnected'  => '',

				'longTermToken'       => '',
				'longTermConnected'   => '',
				'longTermResponse'    => '',

				'expirationFormatted' => '',
				'expirationRaw'       => '',

				'connectionStage'     => 'infoNeeded'
			];

			return $default;
		}

		public function get_connection_stage() {
			return $this->info['connectionStage'];
		}

		public function get_expiration_raw() {
			return $this->info['expirationRaw'];
		}

		public function get_long_term_access_token() {
			return $this->info['longTermToken'];
		}

		// Set connection stage based on saved access values
		public function update_connection_stage() {
			if ( $this->info['authCode'] && '' !== $this->info['authCode'] && ! $this->info['shortTermConnected'] ) {
				// Set stage to 'shortTerm' if minimum required to access short term token
				//var_dump( 'short' );
				$this->info['authConnected'] = true;
				$this->info['connectionStage'] = 'shortTerm';
			} else if ( ! $this->info['authConnected']
				&& $this->info['id'] && '' !== $this->info['id']
				&& $this->info['secret'] && '' !== $this->info['secret']
				&& $this->info['redirectUrl'] && '' !== $this->info['redirectUrl']
			) {
				// Set stage to 'auth' if minimum required to access auth code by connecting to Instagram API from Instagram button push

				$this->info['connectionStage'] = 'auth';
			} else if ( $this->info['shortTermConnected'] && ! $this->info['longTermConnected']
			) {
				// Set stage to 'longTerm' if minimum required to access long term token

				//var_dump( 'long' );
				$this->info['shortTermConnected'] = true;
				$this->info['connectionStage'] = 'longTerm';
			} else if ( $this->info['longTermToken'] && '' !== $this->info['longTermToken'] ) {
				// Set stage to 'complete' if all tokens set

				//var_dump( 'complete' );
				$this->info['longTermConnected'] = true;
				$this->info['connectionStage'] = 'complete';
			} else {
				// App info not set, start at the beginning
				$this->info['connectionStage'] = 'infoNeeded';
			}
		}
	}
}