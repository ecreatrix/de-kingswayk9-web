<?php
/**
 * The template to display the socials in the footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */


// Socials
if ( onleash_is_on(onleash_get_theme_option('socials_in_footer')) && ($onleash_output = onleash_get_socials_links()) != '') {
	?>
	<div class="footer_socials_wrap socials_wrap">
		<div class="footer_socials_inner">
			<?php onleash_show_layout($onleash_output); ?>
		</div>
	</div>
	<?php
}
?>