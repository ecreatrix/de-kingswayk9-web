<?php
/**
 * Connect to the Instagram API and return profile info
 *
 * long access token required
 *
 * info =
 *  *** account_type, id, media_count, username
 *
 * @since 2.0
 */
namespace wa\APIs\Instagram\Connect;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Profile::class ) ) {
	class Profile extends ConnectionParent {
		public $access;

		public $expiry = DAY_IN_SECONDS * 30;

		public $key = 'profile';

		// Look for new information every 30 days
		public $user_fields = 'account_type,id,media_count,username';

		public function __construct( $reset, $access ) {
			parent::__construct( $reset, 'profile', [] );
			$this->access = $access;

			//$this->connect( $access );
		}

		// Get new content if none available or if it's expired
		public function connect() {
			if ( ! parent::call_ready( $this->access ) ) {
				return;
			}

			$profile = Plugin\Instagram\Authenticate::makeCall( $this->access, 'me',
				['fields' => $this->user_fields]
			);

			if ( $profile && is_object( $profile ) ) {
				$this->info = [
					'accountType'   => $profile->account_type,
					'id'            => $profile->id,
					'mediaCount'    => $profile->media_count,
					'username'      => $profile->username,
					'expirationRaw' => $this->expiry + time()
				];

				$this->messages->add( 'Profile information received from Instagram', 'success', 'profile_connected' );
			}
		}
	}
}