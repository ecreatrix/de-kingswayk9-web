<?php
/*
Title: Advanced
 */

piklist( 'field', [
	'type'        => 'checkbox',
	'field'       => 'wa_api_admin',
	'label'       => __( 'Admin settings' ),
	'description' => __( 'Do not enable unless you know how to use this as it will break the site' ),
	//'scope'       => 'user_meta',
	'choices'     => [
		true => 'Show'
	]
	//'value'       => false
] );
