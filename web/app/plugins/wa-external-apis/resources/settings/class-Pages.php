<?php
namespace wa\APIs\Settings;

use wa\APIs as APIs;
use wa\Theme\Settings\Index as ThemeSettings;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Pages::class ) ) {
    class Pages {
        public function __construct() {
            add_action( 'admin_menu', [$this, 'register_menu'], 99 );
            //add_action( 'after_setup_theme', [$this, 'test'], 99 );
        }

        // Add admin page to theme menu
        public static function admin_page_info( $info = [] ) {
            $new_info = [
                'menu_slug'        => APIs\CODENAME,
                'menu_position'    => 80, // Used if theme doesn't have existing menu filter
                'submenu_position' => 20,
                'page_title'       => esc_html__( APIs\NAME, APIs\CODENAME ),
                'menu_title'       => esc_html__( APIs\NAME, APIs\CODENAME ),
                'capability'       => 'manage_options',
                'content_id'       => APIs\SHORTNAME
            ];

            return array_merge( $info, $new_info );
        }

        public function content() {
            echo '<div class="' . APIs\SHORTNAME . '-admin-page wa-admin-page"></div>';

            return '';
        }

        public static function get_admin_page() {
            $admin_page_info = self::admin_page_info();
            //var_dump( $admin_page_info['menu_slug'] . ' ' );

            return admin_url( 'admin.php?page=' . $admin_page_info['menu_slug'] );
        }

        public function register_menu() {
            $filter_slug = 'wa_theme_get_menu_slug';
            $filter_title = 'wa_theme_get_menu_title';
            $admin_page_info = self::admin_page_info();
            $function = [$this, 'content'];

            if ( has_filter( $filter_slug ) && has_filter( $filter_slug ) ) {
                // Theme has admin menu filter so add this page as submenu
                $menu_slug = apply_filters( $filter_slug, '' );
                $menu_title = apply_filters( $filter_title, '' );
                //$parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function = '', $position
                add_submenu_page( $menu_slug, $admin_page_info['page_title'], $admin_page_info['menu_title'], $admin_page_info['capability'], $admin_page_info['menu_slug'], $function, $admin_page_info['submenu_position'] );
            } else {
                add_menu_page( $admin_page_info['page_title'], $admin_page_info['menu_title'], $admin_page_info['capability'], $admin_page_info['menu_slug'], $function, '', $admin_page_info['menu_position'] );
            }
        }
    }
}