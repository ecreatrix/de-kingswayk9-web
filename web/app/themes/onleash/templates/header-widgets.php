<?php
/**
 * The template to display the widgets area in the header
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

// Header sidebar
$onleash_header_name = onleash_get_theme_option('header_widgets');
$onleash_header_present = !onleash_is_off($onleash_header_name) && is_active_sidebar($onleash_header_name);
if ($onleash_header_present) { 
	onleash_storage_set('current_sidebar', 'header');
	$onleash_header_wide = onleash_get_theme_option('header_wide');
	ob_start();
	if ( is_active_sidebar($onleash_header_name) ) {
		dynamic_sidebar($onleash_header_name);
	}
	$onleash_widgets_output = ob_get_contents();
	ob_end_clean();
	if (!empty($onleash_widgets_output)) {
		$onleash_widgets_output = preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $onleash_widgets_output);
		$onleash_need_columns = strpos($onleash_widgets_output, 'columns_wrap')===false;
		if ($onleash_need_columns) {
			$onleash_columns = max(0, (int) onleash_get_theme_option('header_columns'));
			if ($onleash_columns == 0) $onleash_columns = min(6, max(1, substr_count($onleash_widgets_output, '<aside ')));
			if ($onleash_columns > 1)
				$onleash_widgets_output = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($onleash_columns).' widget ', $onleash_widgets_output);
			else
				$onleash_need_columns = false;
		}
		?>
		<div class="header_widgets_wrap widget_area<?php echo !empty($onleash_header_wide) ? ' header_fullwidth' : ' header_boxed'; ?>">
			<div class="header_widgets_inner widget_area_inner">
				<?php 
				if (!$onleash_header_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($onleash_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'onleash_action_before_sidebar' );
				onleash_show_layout($onleash_widgets_output);
				do_action( 'onleash_action_after_sidebar' );
				if ($onleash_need_columns) {
					?></div>	<!-- /.columns_wrap --><?php
				}
				if (!$onleash_header_wide) {
					?></div>	<!-- /.content_wrap --><?php
				}
				?>
			</div>	<!-- /.header_widgets_inner -->
		</div>	<!-- /.header_widgets_wrap -->
		<?php
	}
}
?>