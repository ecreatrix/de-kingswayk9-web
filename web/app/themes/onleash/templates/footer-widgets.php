<?php
/**
 * The template to display the widgets area in the footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */

// Footer sidebar
$onleash_footer_name = onleash_get_theme_option('footer_widgets');
$onleash_footer_present = !onleash_is_off($onleash_footer_name) && is_active_sidebar($onleash_footer_name);
if ($onleash_footer_present) { 
	onleash_storage_set('current_sidebar', 'footer');
	$onleash_footer_wide = onleash_get_theme_option('footer_wide');
	ob_start();
	if ( is_active_sidebar($onleash_footer_name) ) {
		dynamic_sidebar($onleash_footer_name);
	}
	$onleash_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($onleash_out)) {
		$onleash_out = preg_replace("/<\\/aside>[\r\n\s]*<aside/", "</aside><aside", $onleash_out);
		$onleash_need_columns = true;	//or check: strpos($onleash_out, 'columns_wrap')===false;
		if ($onleash_need_columns) {
			$onleash_columns = max(0, (int) onleash_get_theme_option('footer_columns'));
			if ($onleash_columns == 0) $onleash_columns = min(4, max(1, substr_count($onleash_out, '<aside ')));
			if ($onleash_columns > 1)
				$onleash_out = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($onleash_columns).' widget ', $onleash_out);
			else
				$onleash_need_columns = false;
		}
		?>
		<div class="footer_widgets_wrap widget_area<?php echo !empty($onleash_footer_wide) ? ' footer_fullwidth' : ''; ?> sc_layouts_row  sc_layouts_row_type_normal">
			<div class="footer_widgets_inner widget_area_inner">
				<?php 
				if (!$onleash_footer_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($onleash_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'onleash_action_before_sidebar' );
				onleash_show_layout($onleash_out);
				do_action( 'onleash_action_after_sidebar' );
				if ($onleash_need_columns) {
					?></div><!-- /.columns_wrap --><?php
				}
				if (!$onleash_footer_wide) {
					?></div><!-- /.content_wrap --><?php
				}
				?>
			</div><!-- /.footer_widgets_inner -->
		</div><!-- /.footer_widgets_wrap -->
		<?php
	}
}
?>