<?php
/**
 * The template to display the logo or the site name and the slogan in the Header
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_args = get_query_var('onleash_logo_args');

// Site logo
$onleash_logo_image  = onleash_get_logo_image(isset($onleash_args['type']) ? $onleash_args['type'] : '');
$onleash_logo_text   = onleash_is_on(onleash_get_theme_option('logo_text')) ? get_bloginfo( 'name' ) : '';
if (!empty($onleash_logo_image) || !empty($onleash_logo_text)) {
	?><a class="sc_layouts_logo" href="<?php echo is_front_page() ? '#' : esc_url(home_url('/')); ?>"><?php
		if (!empty($onleash_logo_image)) {
			$onleash_attr = onleash_getimagesize($onleash_logo_image);
			echo '<img src="'.esc_url($onleash_logo_image).'" alt=""'.(!empty($onleash_attr[3]) ? sprintf(' %s', $onleash_attr[3]) : '').'>' ;
		} else {
			onleash_show_layout(onleash_prepare_macros($onleash_logo_text), '<span class="logo_text">', '</span>');
		}
	?></a><?php
}
?>