<?php
namespace wa\APIs\Rest;

use wa\APIs as Plugin;

// Debug: go to www.[site]/wp-json/waapi/v1/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Routes::class ) ) {
    class Routes extends \WP_REST_Controller {
        public function __construct() {
            add_action( 'rest_api_init', [$this, 'register_routes'] );
        }

        public function register_routes() {
            $namespace = Index::get_route();
            $wp = new WP();

            $this->register_routes_settings( $namespace, $wp );
            $this->register_routes_instagram( $namespace, $wp );
        }

        public function register_routes_instagram( $namespace, $wp ) {
            $instagram = new Plugin\Instagram\Rest();

            register_rest_route( $namespace, '/instagram/', [
                'methods'             => \WP_REST_Server::EDITABLE,
                'callback'            => [$instagram, 'set_request'],
                'permission_callback' => [$wp, 'manage_options_permission']
            ] );

            register_rest_route( $namespace, '/instagram/', [
                'methods'             => \WP_REST_Server::READABLE,
                'callback'            => [$instagram, 'get_request'],
                'permission_callback' => [$wp, 'manage_options_permission']
            ] );
        }

        public function register_routes_settings( $namespace, $wp ) {
            register_rest_route( $namespace, '/settings/', [
                'methods'             => \WP_REST_Server::EDITABLE,
                'callback'            => [$wp, 'update_settings_request'],
                'args'                => [],
                'permission_callback' => [$wp, 'manage_options_permission']
            ] );

            register_rest_route( $namespace, '/settings/', [
                'methods'             => \WP_REST_Server::READABLE,
                'callback'            => [$wp, 'get_settings_request'],
                'permission_callback' => [$wp, 'manage_options_permission']
            ] );
        }
    }
}
