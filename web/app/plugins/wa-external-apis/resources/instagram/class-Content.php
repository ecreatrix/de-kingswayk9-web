<?php
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( Content::class ) ) {
	class Content {
		public $display = false;

		public $media = false;

		public $output = false;

		public function __construct() {
			$this->setup_defaults();
		}

		public function content() {
			return implode( '<br/>', $this->output );
		}

		public function get_info( $key, $instagram ) {
			$value = [];

			if ( array_key_exists( $key, $instagram ) ) {
				$value = $instagram[$key]->get_info();
			}

			return $value;
		}

		public static function output( $type, $echo ) {
			if ( 'carousel' === $type ) {
				$carousel = new Content\Carousel();

				return $carousel->content( $echo );
			}
		}

		public function process_media() {
			if ( ! $this->media || ! $this->media['data'] ) {
				//TODO add alert for admin users
				return '';
			}

			$posts = $this->media['data'];

			// If media content has been saved, use it to get all images
			if ( ! $posts ) {
				return '';
			}

			$processed_data = [];
			$output = [];

			$i = 0;

			foreach ( $posts as $post ) {
				$i++;
				$id = $post->id;
				$href = $post->permalink;
				$type = $post->media_type;
				$caption = property_exists( $post, 'caption' ) ? $post->caption : '';
				$timestamp = date( 'F j, Y', strtotime( $post->timestamp ) );

				$src = false;
				if ( 'IMAGE' === $type ) {
					$src = $post->media_url;

				} else if ( 'VIDEO' === $type ) {
					$src = $post->thumbnail_url;
				}

				//For testing
				//$src .= '-x';

				$block = '';
				if ( $src ) {
					$img = '<img class="image id-' . $id . '" src="' . $src . '" />';
					//$icon = '<i class="fab fa fa-instagram" aria-hidden="true"></i>';
					$icon = '';
					$caption_block = '';

					if ( $caption && $this->display && $this->display['showComments'] ) {
						$caption_block = '<span class="caption">' . $caption . '</span>';
					}

					$content = '<div class="content">' . $icon . $caption_block . '</div>';
					$style = ' style="background-image: url(' . $src . ')"';

					$link = '<div class="slide"' . $style . '><a class="overlay-container instagram-link" href="' . $href . '" target="_blank" aria-hidden="false" tabindex="-1">' . $content . '</a></div>';

					$block = [
						'img'  => $img,
						'link' => $link
					];

					$output[] = $link;
				}

				$processed_data[] = [
					'id'        => $id,
					'caption'   => $caption,
					'src'       => $src,
					'href'      => $href,
					'timestamp' => $timestamp,
					'block'     => $block
				];

				if ( $i > 12 ) {
					// Stop after 12 images
					break;
				}
			}

			$this->media['processed'] = $processed_data;
			$this->output = $output;
		}

		public function setup_defaults() {
			$instagram = Plugin\Settings\Options::get_settings( Index::get_key() );

			if ( ! is_array( $instagram ) ) {
				return;
			}

			$connect = ( new Connect\Index() )->connect_all();

			// Get saved API media content if available
			$media = $this->get_info( 'media', $instagram );
			$profile = $this->get_info( 'profile', $instagram );
			$display = $this->get_info( 'display', $instagram );

			if ( $media
				&& array_key_exists( 'data', $media )
				&& count( $media['data'] ) > 0
			) {
				$this->media['data'] = $media['data'];
			}

			if ( $media
				&& array_key_exists( 'paging', $media )
				&& $media['paging']
			) {
				$this->media['paging'] = $media['paging'];
			}

			if ( $profile
				&& count( $profile ) > 0
			) {
				$this->profile = $profile;
			}

			// Get saved display settings
			if ( $display ) {
				$this->display = $display;
			}

			$this->process_media();
		}
	}
}