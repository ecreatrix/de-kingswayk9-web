<?php
/**
 * The template to display posts in widgets and/or in the search results
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */

$onleash_post_id    = get_the_ID();
$onleash_post_date  = onleash_get_date();
$onleash_post_title = get_the_title();
$onleash_post_link  = get_permalink();
$onleash_post_author_id   = get_the_author_meta('ID');
$onleash_post_author_name = get_the_author_meta('display_name');
$onleash_post_author_url  = get_author_posts_url($onleash_post_author_id, '');

$onleash_args = get_query_var('onleash_args_widgets_posts');
$onleash_show_date = isset($onleash_args['show_date']) ? (int) $onleash_args['show_date'] : 1;
$onleash_show_image = isset($onleash_args['show_image']) ? (int) $onleash_args['show_image'] : 1;
$onleash_show_author = isset($onleash_args['show_author']) ? (int) $onleash_args['show_author'] : 1;
$onleash_show_counters = isset($onleash_args['show_counters']) ? (int) $onleash_args['show_counters'] : 1;
$onleash_show_categories = isset($onleash_args['show_categories']) ? (int) $onleash_args['show_categories'] : 1;

$onleash_output = onleash_storage_get('onleash_output_widgets_posts');

$onleash_post_counters_output = '';
if ( $onleash_show_counters ) {
	$onleash_post_counters_output = '<span class="post_info_item post_info_counters">'
								. onleash_get_post_counters('comments')
							. '</span>';
}


$onleash_output .= '<article class="post_item with_thumb">';

if ($onleash_show_image) {
	$onleash_post_thumb = get_the_post_thumbnail($onleash_post_id, onleash_get_thumb_size('tiny'), array(
		'alt' => get_the_title()
	));
	if ($onleash_post_thumb) $onleash_output .= '<div class="post_thumb">' . ($onleash_post_link ? '<a href="' . esc_url($onleash_post_link) . '">' : '') . ($onleash_post_thumb) . ($onleash_post_link ? '</a>' : '') . '</div>';
}

$onleash_output .= '<div class="post_content">'
			. ($onleash_show_categories 
					? '<div class="post_categories">'
						. onleash_get_post_categories()
						. $onleash_post_counters_output
						. '</div>' 
					: '')
			. '<h6 class="post_title">' . ($onleash_post_link ? '<a href="' . esc_url($onleash_post_link) . '">' : '') . ($onleash_post_title) . ($onleash_post_link ? '</a>' : '') . '</h6>'
			. apply_filters('onleash_filter_get_post_info', 
								'<div class="post_info">'
									. ($onleash_show_date 
										? '<span class="post_info_item post_info_posted">'
											. ($onleash_post_link ? '<a href="' . esc_url($onleash_post_link) . '" class="post_info_date">' : '') 
											. esc_html($onleash_post_date) 
											. ($onleash_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. ($onleash_show_author 
										? '<span class="post_info_item post_info_posted_by">' 
											. esc_html__('by', 'onleash') . ' ' 
											. ($onleash_post_link ? '<a href="' . esc_url($onleash_post_author_url) . '" class="post_info_author">' : '') 
											. esc_html($onleash_post_author_name) 
											. ($onleash_post_link ? '</a>' : '') 
											. '</span>'
										: '')
									. (!$onleash_show_categories && $onleash_post_counters_output
										? $onleash_post_counters_output
										: '')
								. '</div>')
		. '</div>'
	. '</article>';
onleash_storage_set('onleash_output_widgets_posts', $onleash_output);
?>