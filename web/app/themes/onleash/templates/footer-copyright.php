<?php
/**
 * The template to display the copyright info in the footer
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0.10
 */

// Copyright area
$onleash_footer_scheme =  onleash_is_inherit(onleash_get_theme_option('footer_scheme')) ? onleash_get_theme_option('color_scheme') : onleash_get_theme_option('footer_scheme');
$onleash_copyright_scheme = onleash_is_inherit(onleash_get_theme_option('copyright_scheme')) ? $onleash_footer_scheme : onleash_get_theme_option('copyright_scheme');
?> 
<div class="footer_copyright_wrap scheme_<?php echo esc_attr($onleash_copyright_scheme); ?>">
	<div class="footer_copyright_inner">
		<div class="content_wrap">
			<div class="copyright_text"><?php
				// Replace {{...}} and [[...]] on the <i>...</i> and <b>...</b>
				$onleash_copyright = onleash_prepare_macros(onleash_get_theme_option('copyright'));
				if (!empty($onleash_copyright)) {
					// Replace {date_format} on the current date in the specified format
					if (preg_match("/(\\{[\\w\\d\\\\\\-\\:]*\\})/", $onleash_copyright, $onleash_matches)) {
						$onleash_copyright = str_replace($onleash_matches[1], date(str_replace(array('{', '}'), '', $onleash_matches[1])), $onleash_copyright);
					}
					// Display copyright
					echo wp_kses_data(nl2br($onleash_copyright));
				}
			?></div>
		</div>
	</div>
</div>
