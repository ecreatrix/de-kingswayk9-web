<?php
/**
 * The template to display the main menu
 *
 * @package WordPress
 * @subpackage ONLEASH
 * @since ONLEASH 1.0
 */
?>
<div class="top_panel_navi sc_layouts_row sc_layouts_row_type_compact sc_layouts_row_fixed sc_layouts_row_delimiter
			scheme_<?php echo esc_attr(onleash_is_inherit(onleash_get_theme_option('menu_scheme')) 
												? (onleash_is_inherit(onleash_get_theme_option('header_scheme')) 
													? onleash_get_theme_option('color_scheme') 
													: onleash_get_theme_option('header_scheme')) 
												: onleash_get_theme_option('menu_scheme')); ?>">
	<div class="content_wrap">
		<div class="columns_wrap">
			<div class="sc_layouts_column sc_layouts_column_align_left sc_layouts_column_icons_position_left column-1_4">
				<?php
                // Logo
                ?><div class="sc_layouts_item"><?php
                    get_template_part( 'templates/header-logo' );
                    ?></div>
			</div><?php
			
			// Attention! Don't place any spaces between columns!
			?><div class="sc_layouts_column sc_layouts_column_align_right sc_layouts_column_icons_position_left column-3_4">
				<div class="sc_layouts_item">
					<?php
					// Main menu
					$onleash_menu_main = onleash_get_nav_menu(array(
						'location' => 'menu_main', 
						'class' => 'sc_layouts_menu sc_layouts_menu_default sc_layouts_hide_on_mobile'
						)
					);
					if (empty($onleash_menu_main)) {
						$onleash_menu_main = onleash_get_nav_menu(array(
							'class' => 'sc_layouts_menu sc_layouts_menu_default sc_layouts_hide_on_mobile'
							)
						);
					}
					onleash_show_layout($onleash_menu_main);
					?>
			</div>
		</div><!-- /.sc_layouts_row -->
	</div><!-- /.content_wrap -->
</div><!-- /.top_panel_navi -->