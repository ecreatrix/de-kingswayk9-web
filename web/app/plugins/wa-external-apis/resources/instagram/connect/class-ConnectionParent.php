<?php
/**
 * Connect to the Instagram API and return the results. It's possible
 * to build the url from a connected account (includes access token,
 * account id, account type), endpoint and parameters (hashtag, etc..)
 * as well as a full url such as from the pagination data from some Instagram API requests.
 *
 * Errors from either the Instagram API or from the HTTP request are detected
 * and can be handled.
 *
 * @since 2.0
 */
namespace wa\APIs\Instagram\Connect;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( ConnectionParent::class ) ) {
	class ConnectionParent {
		public $info = [];

		public $key = false;

		public $messages;

		// Setup data for various API request
		public function __construct( $reset = false, $key = false, $info = [] ) {
			$this->messages = new Plugin\Instagram\Messages();
			$this->key = $key;

			if ( $key ) {
				$this->setup_info( $reset, $key, $info );
			}
		}

		public function call_ready( $access ) {
			if ( ! is_object( $access ) || empty( $access ) ) {
				//$this->messages->need_app_info();

				return;
			}

			$status = $this->check_expiry( $access->get_expiration_raw() );

			if ( 'valid' !== $status ) {
				$this->messages->expired_access();

				return;
			}

			$long_term_access_token = $access->get_long_term_access_token();
			$connection_stage = $access->get_connection_stage();

			if ( ! $long_term_access_token ) {
				// Long access token not set, no calls can be made
				$this->messages->access_message( $connection_stage );

				return false;
			}

			$status = $this->check_expiry( $this->info['expirationRaw'] );

			if ( 'valid' === $status ) {
				if ( 'media' === $this->key ) {
					$this->messages->media_not_expired();
				} else if ( 'profile' === $this->key ) {
					$this->messages->profile_not_expired();
				}

				return false;
			}

			return true;
		}

		public static function check_expiry( $expiration = false ) {
			$now = time();

			if ( ! $expiration ) {
				// Return expired if timestamp not available
				return 'expired';
			}

			if ( $expiration > $now ) {
				return 'valid';
			} else {
				return 'expired';
			}
		}

		public function defaults() {
			return [
				'expirationRaw' => time()
			];
		}

		// Returns all information received from Instagram
		public function get_info() {
			return $this->info;
		}

		// Returns messages
		public function get_messages() {
			return $this->messages;
		}

		public function setup_info( $reset, $key, $info = [] ) {
			$default = $this->defaults();

				if(is_object( $info )) {
					$info = $info->get_info();
				}

				if(!is_array( $info )) {
					$info = [];
				}

			// Setup with currently saved info if available if reset not requested
			$current_settings = Plugin\Settings\Options::get_settings( Plugin\Instagram\Index::get_key() );

			if ( $info && array_key_exists( $key, $current_settings ) ) {
				$info = array_merge( $current_settings[$key]->get_info(), $info );
			}

			if ( $reset ) {
				$info = [];
			}

			// Add submitted info if reset not requested
			$this->info = array_merge( $default, $info );
		}
	}
}