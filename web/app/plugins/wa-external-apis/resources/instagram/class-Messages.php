<?php
/**
 * Used to return message as array object from $response or successfull one
 *
 * @since 2.0/5.0
 */
namespace wa\APIs\Instagram;

use wa\APIs as Plugin;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Messages::class ) ) {
	class Messages extends Plugin\Rest\Messages {
		// Add appropriate message based on connection stage
		public function access_message( $connection_stage ) {
			if ( 'infoNeeded' === $connection_stage || 'authCode' === $connection_stage ) {
			} else if ( 'shortTerm' === $connection_stage ) {
				$this->need_auth_code_needed();
			} else if ( 'longTerm' === $connection_stage ) {
				$this->need_short_term_token();
			} //else if ( 'complete' === $connection_stage ) {
			  //$this->need_long_term_token();
			  //}
		}

		public function connection_error( $response ) {
			//var_dump( $response );
			if ( is_object( $response ) && property_exists( $response, 'error' ) || property_exists( $response, 'error_message' ) || property_exists( $response, 'error_type' ) ) {
				// oAuth error occcured
				$this->connection_error_instagram( $response );
			} else {
				// Random error occured

				$this->connection_error_unknown();
			}

			return $this->messages;
		}

		// oAuth error occcured
		public function connection_error_instagram( $response ) {
			if ( is_object( $response ) && property_exists( $response, 'error' ) ) {
				$text = 'Instagram error: ' . $response->error->type
				. ', message: '
				. $response->error->message;
			} else {
				$text = 'Instagram error: ' . $response->error_type
				. ', code: ' . $response->code
				. ', message: '
				. $response->error_message;
			}

			$type = 'warning';
			$code = 'oauth_error';

			$this->add( $text, $type, $code );
		}

		public function connection_success( $response ) {
			$text = 'Instagram connection successful, token received';
			$type = 'success';
			$code = 'oauth_success';

			$this->add( $text, $type, $code );
		}

		public function curl_error_make_call( $ch ) {
			$error = curl_error( $ch );

			$this->add( 'Error: makeCall() - cURL error: ' . $error, 'warning', 'curl_error_make_call' );
		}

		public function curl_error_make_oauth( $ch ) {
			$error = curl_error( $ch );

			$this->add( 'Error: makeOAuthCall() - cURL error: ' . $error, 'warning', 'curl_error_make_oauth' );
		}

		public function expired_access() {
			$this->add( 'App access information in settings is expired. Please renew via settings page.', 'warning', 'access_unavailable' );
		}

		public function media_not_expired() {
			$this->add( 'Media information in settings is not expired so there is no need to query Instagram', 'success', 'media_saved_available' );
		}

		public function need_app_info() {
			$this->add( 'App ID, Secret and Redirect URL needed to get authentication code', 'warning', 'basic_info' );
		}

		public function need_auth_code_needed() {
			$this->add( 'Authorization code needed, please connect via link below', 'warning', 'token_auth_code' );
		}

		public function need_long_term_token() {
			$this->add( 'Long term token needed, please connect via link below first', 'warning', 'token_long_term' );
		}

		public function need_short_term_token() {
			$this->add( 'Short term token needed, please connect via link below first', 'warning', 'token_short_term' );
		}

		public function profile_not_expired() {
			$this->add( 'Profile information in settings is not expired so there is no need to query Instagram', 'success', 'profile_saved_available' );
		}

		public function settings_reset( $text = 'Instagram settings were reset', $type = 'success', $code = 'settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_failed( $text = 'Failed to update Instagram settings', $type = 'error', $code = 'no_settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_success( $text = 'Instagram settings were updated', $type = 'success', $code = 'settings_update' ) {
			$this->add( $text, $type, $code );
		}

		public function settings_update_unneeded( $text = 'No new data provided, Instagram settings do not need to be updated', $type = 'warning', $code = 'no_new_settings_data' ) {
			$this->add( $text, $type, $code );
		}
	}
}